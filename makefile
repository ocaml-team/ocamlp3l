#######################################################################
#                                                                     #
#                          OCamlP3l                                   #
#                                                                     #
# (C) 2004-2007                                                       #
#             Roberto Di Cosmo (dicosmo@dicosmo.org)                  #
#             Zheng Li (zli@lip6.fr)                                  #
#             Pierre Weis (Pierre.Weis@inria.fr)                      #
#             Francois Clement (Francois.Clement@inria.fr)            #
#                                                                     #
# Based on original Ocaml P3L System                                  #
# (C) 1997 by                                                         #
#             Roberto Di Cosmo (dicosmo@ens.fr)                       #
#             Marco Danelutto (marcod@di.unipi.it)                    #
#             Xavier Leroy  (Xavier.Leroy@inria.fr)                   #
#             Susanna Pelagatti (susanna@di.unipi.it)                 #
#                                                                     #
# This program is free software; you can redistribute it and/or       #
# modify it under the terms of the GNU Library General Public License #
# as published by the Free Software Foundation; either version 2      #
# of the License, or (at your option) any later version.              #
#                                                                     #
# This program is distributed in the hope that it will be useful,     #
# but WITHOUT ANY WARRANTY; without even the implied warranty of      #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       #
# GNU Library General Public License for more details.                #
#                                                                     #
#######################################################################

# $Id: makefile,v 1.33 2007/01/24 11:54:33 fclement Exp $
#
#  You should not modify anything in this file!

MAKEFILES_DIR = .

include $(MAKEFILES_DIR)/makefile.application

CAMLSOURCES = command_options.ml version.ml basedefs.ml commlib.ml streams.ml p3lstream.ml \
          seqp3l.ml parp3l.ml grafp3l.ml \
          template.ml nodecode.ml
CAMLNISOURCES = server_process.ml server_thread.ml
CAMLIOSOURCES = server.mli

SOURCES = $(CAMLSOURCES) $(CAMLSOURCES:.ml=.mli) \
          $(CAMLNISOURCES) $(CAMLIOSOURCES) Makefile

MODELS = process thread
MODELSDIRS = vprocess vthread

.PHONY: all configure install uninstall clean clean-all depend bannerfile banner

all:: configure

configure: $(MODELSDIRS)

$(MODELSDIRS):
	$(MKDIR) $(MODELSDIRS)
	for i in $(MODELSDIRS); do \
	  (cd Source; $(CP) $(SOURCES) ../$$i); \
	done
	for i in $(MODELSDIRS); do \
	  $(TOUCH) $$i/.depend; \
	  sleep 1; \
	  $(TOUCH) $$i/.depend-rebuild; \
	done; \
	$(MAKE) depend

all install uninstall clean depend::
	for i in $(MODELS); do \
	  (cd v$$i; \
	   $(MAKE) $@ PACKAGELIBDIR=$(PACKAGELIBDIR)/v$$i MODEL=$$i); \
	done
	cd Tools; $(MAKE) $@

clean-all:: clean

banner: bannerfile
	for i in `ls *akefile* INSTALL README VERSION TODO`; do \
	  $(BANNER) --banner-file $(BANNERFILE) $$i; \
	done
	(cd Source; \
	for i in Makefile $(SOURCES); do \
	  $(BANNER) --banner-file $(BANNERFILE) $$i; \
	done)
	(cd Tools; \
	for i in `ls Makefile *.ml*`; do \
	  $(BANNER) --banner-file $(BANNERFILE) $$i; \
	done)

bannerfile: banner.in
	$(HTMLC) -i banner.in -o banner
	$(CP) banner Source
	$(CP) banner Tools

unconfigure: clean-all
	$(RM) $(MODELSDIRS)

include $(MAKEFILES_DIR)/makefile.common
