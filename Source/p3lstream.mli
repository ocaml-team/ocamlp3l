(***********************************************************************)
(*                                                                     *)
(*                          OCamlP3l                                   *)
(*                                                                     *)
(* (C) 2004-2007                                                       *)
(*             Roberto Di Cosmo (dicosmo@dicosmo.org)                  *)
(*             Zheng Li (zli@lip6.fr)                                  *)
(*             Pierre Weis (Pierre.Weis@inria.fr)                      *)
(*             Francois Clement (Francois.Clement@inria.fr)            *)
(*                                                                     *)
(* Based on original Ocaml P3L System                                  *)
(* (C) 1997 by                                                         *)
(*             Roberto Di Cosmo (dicosmo@ens.fr)                       *)
(*             Marco Danelutto (marcod@di.unipi.it)                    *)
(*             Xavier Leroy  (Xavier.Leroy@inria.fr)                   *)
(*             Susanna Pelagatti (susanna@di.unipi.it)                 *)
(*                                                                     *)
(* This program is free software; you can redistribute it and/or       *)
(* modify it under the terms of the GNU Library General Public License *)
(* as published by the Free Software Foundation; either version 2      *)
(* of the License, or (at your option) any later version.              *)
(*                                                                     *)
(* This program is distributed in the hope that it will be useful,     *)
(* but WITHOUT ANY WARRANTY; without even the implied warranty of      *)
(* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       *)
(* GNU Library General Public License for more details.                *)
(*                                                                     *)
(***********************************************************************)

(* $Id: p3lstream.mli,v 1.3 2007/01/23 15:50:04 weis Exp $ *)

type 'a pac = tag list * 'a lazy_t
and tag = int * int
type sign = Eos
exception End_of_stream
type 'a pkg = Pac of 'a pac | Sign of sign
type 'a s = { mutable buf : 'a pkg list; mutable gen : unit -> 'a pkg; }
val from_fun : (unit -> 'a pkg) -> 'a s
val from_list : 'a pkg list -> 'a s
val singleton : 'a pkg -> 'a s
val pkg_next : 'a s -> 'a pkg
val pkg_iter : ('a pkg -> unit) -> 'a s -> unit
val buff_pkg : 'a pkg -> 'a s -> unit
val buff_list : 'a pkg list -> 'a s -> unit
type 'a t = 'a s
type color = int
val of_fun : (unit -> 'a) -> 'a t
val of_list : 'a list -> 'a t
val of_array : 'a array -> 'a t
val next : 'a t -> 'a
val iter : ('a -> unit) -> 'a t -> unit
val warning : string
val to_fun : 'a t -> unit -> 'a
val to_list : 'a t -> 'a list
val to_array : 'a t -> 'a array
