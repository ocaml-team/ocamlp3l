(***********************************************************************)
(*                                                                     *)
(*                          OCamlP3l                                   *)
(*                                                                     *)
(* (C) 2004-2007                                                       *)
(*             Roberto Di Cosmo (dicosmo@dicosmo.org)                  *)
(*             Zheng Li (zli@lip6.fr)                                  *)
(*             Pierre Weis (Pierre.Weis@inria.fr)                      *)
(*             Francois Clement (Francois.Clement@inria.fr)            *)
(*                                                                     *)
(* Based on original Ocaml P3L System                                  *)
(* (C) 1997 by                                                         *)
(*             Roberto Di Cosmo (dicosmo@ens.fr)                       *)
(*             Marco Danelutto (marcod@di.unipi.it)                    *)
(*             Xavier Leroy  (Xavier.Leroy@inria.fr)                   *)
(*             Susanna Pelagatti (susanna@di.unipi.it)                 *)
(*                                                                     *)
(* This program is free software; you can redistribute it and/or       *)
(* modify it under the terms of the GNU Library General Public License *)
(* as published by the Free Software Foundation; either version 2      *)
(* of the License, or (at your option) any later version.              *)
(*                                                                     *)
(* This program is distributed in the hope that it will be useful,     *)
(* but WITHOUT ANY WARRANTY; without even the implied warranty of      *)
(* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       *)
(* GNU Library General Public License for more details.                *)
(*                                                                     *)
(***********************************************************************)

(* $Id: commlib.mli,v 1.3 2007/01/23 15:50:04 weis Exp $ *)

val debug : ?mask:int -> (unit -> unit) -> unit
val context : out_channel -> unit
val timedcontext : out_channel -> unit
type vpinfo = int
type vp_chan = VpCh of vpinfo * Unix.file_descr * Unix.sockaddr
type vp_extern = VpX of vpinfo * Unix.sockaddr
val ch_set_close_on_exec : vp_chan -> unit
val dummy_vpinfo : int
val dummy_sockaddr : Unix.sockaddr
val dummy_vpch : vp_chan
val setmyvpinfo : vpinfo -> unit
val myvpinfo : unit -> vpinfo
val find_ch_of_vpinfo : vpinfo -> vp_chan list -> vp_chan
val get_free_addr : unit -> Unix.file_descr * Unix.sockaddr
val getport : Unix.sockaddr -> int
val get_addr_port : Unix.sockaddr -> Unix.inet_addr * int
val mkvp : vpinfo -> int -> vp_chan
val vp_extern : vp_chan -> vp_extern
val vp_extern_print : vp_extern -> unit
val vp_info : vp_chan -> vpinfo
val name_of_descr : ?peer:bool -> Unix.file_descr -> string
val addr_of_vpch : ?peer:bool -> vp_chan -> string
val vp_close : vp_chan -> unit
val really_write : Unix.file_descr -> string -> int -> int -> unit
val unsafe_really_read : Unix.file_descr -> string -> int -> int -> unit
val flag_to_string : Marshal.extern_flags -> string
val flags_to_string : Marshal.extern_flags list -> string
val gen_cl_send : vp_chan -> 'a -> Marshal.extern_flags list -> unit
val cl_send : vp_chan -> 'a -> unit
val nocl_send : vp_chan -> 'a -> unit
val bsize : int
val buf : string ref
val marshal_read : Unix.file_descr -> 'a
val receive : vp_chan -> 'a
val receive_any : vp_chan list -> 'a * vp_chan
val establish_smart_server : (vp_chan -> 'a) -> unit
val fd_open_connection : Unix.sockaddr -> Unix.file_descr
val get_connection : Unix.inet_addr * int -> Unix.file_descr
val doaccept : Unix.file_descr -> Unix.file_descr * Unix.sockaddr
val do_accepts : int -> Unix.file_descr -> string -> Unix.file_descr list
val proc_inet_addr : string -> (Unix.inet_addr * int) * int * int
val vpchan_of_addr : vpinfo * (Unix.inet_addr * int) -> vpinfo * vp_chan
val outchs_of_vps : vp_extern list -> vp_chan -> vp_chan list
val setup_receive_chan : vp_chan -> vp_chan
val sameorderof : vpinfo list -> vp_chan list -> vp_chan list
val setup_receive_chans : vpinfo list -> vp_chan -> string -> vp_chan list
val spawn : ('a -> 'b) -> 'a -> unit
