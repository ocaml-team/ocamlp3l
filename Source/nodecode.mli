(***********************************************************************)
(*                                                                     *)
(*                          OCamlP3l                                   *)
(*                                                                     *)
(* (C) 2004-2007                                                       *)
(*             Roberto Di Cosmo (dicosmo@dicosmo.org)                  *)
(*             Zheng Li (zli@lip6.fr)                                  *)
(*             Pierre Weis (Pierre.Weis@inria.fr)                      *)
(*             Francois Clement (Francois.Clement@inria.fr)            *)
(*                                                                     *)
(* Based on original Ocaml P3L System                                  *)
(* (C) 1997 by                                                         *)
(*             Roberto Di Cosmo (dicosmo@ens.fr)                       *)
(*             Marco Danelutto (marcod@di.unipi.it)                    *)
(*             Xavier Leroy  (Xavier.Leroy@inria.fr)                   *)
(*             Susanna Pelagatti (susanna@di.unipi.it)                 *)
(*                                                                     *)
(* This program is free software; you can redistribute it and/or       *)
(* modify it under the terms of the GNU Library General Public License *)
(* as published by the Free Software Foundation; either version 2      *)
(* of the License, or (at your option) any later version.              *)
(*                                                                     *)
(* This program is distributed in the hope that it will be useful,     *)
(* but WITHOUT ANY WARRANTY; without even the implied warranty of      *)
(* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       *)
(* GNU Library General Public License for more details.                *)
(*                                                                     *)
(***********************************************************************)

(* $Id: nodecode.mli,v 1.3 2007/01/23 15:50:04 weis Exp $ *)

val passone :
  ('a, 'b) Parp3l.p3ltreeexp list ->
  (Parp3l.nproc * Commlib.vp_chan) list ->
  (Parp3l.nproc * Commlib.vp_extern) list
val passtwo :
  ('a * Commlib.vp_extern) list ->
  (int * 'a list) list -> (int * Commlib.vp_chan) list -> unit
val root :
  ('a * ('b, 'c) Parp3l.p3ltreeexp list) *
  (Commlib.vpinfo * (Unix.inet_addr * int)) list *
  (Commlib.vpinfo * Commlib.vp_chan option ref * Commlib.vp_chan option ref)
  list -> unit
val fanin_of : Parp3l.proctemplate -> int
val nodeconfiguration : Commlib.vp_chan -> unit
val node : unit -> unit
val main : (unit -> 'a) -> unit
val pardo : (unit -> 'a) -> unit
val pktnum : Template.sequencenum ref
val cl_send_stream : Commlib.vp_chan -> 'a P3lstream.t -> unit
val receive_stream : Commlib.vp_chan -> 'a P3lstream.t
val parfun :
  (unit -> ('a, 'b) Parp3l.p3ltree) -> 'a P3lstream.t -> 'b P3lstream.t
