(***********************************************************************)
(*                                                                     *)
(*                          OCamlP3l                                   *)
(*                                                                     *)
(* (C) 2004-2007                                                       *)
(*             Roberto Di Cosmo (dicosmo@dicosmo.org)                  *)
(*             Zheng Li (zli@lip6.fr)                                  *)
(*             Pierre Weis (Pierre.Weis@inria.fr)                      *)
(*             Francois Clement (Francois.Clement@inria.fr)            *)
(*                                                                     *)
(* Based on original Ocaml P3L System                                  *)
(* (C) 1997 by                                                         *)
(*             Roberto Di Cosmo (dicosmo@ens.fr)                       *)
(*             Marco Danelutto (marcod@di.unipi.it)                    *)
(*             Xavier Leroy  (Xavier.Leroy@inria.fr)                   *)
(*             Susanna Pelagatti (susanna@di.unipi.it)                 *)
(*                                                                     *)
(* This program is free software; you can redistribute it and/or       *)
(* modify it under the terms of the GNU Library General Public License *)
(* as published by the Free Software Foundation; either version 2      *)
(* of the License, or (at your option) any later version.              *)
(*                                                                     *)
(* This program is distributed in the hope that it will be useful,     *)
(* but WITHOUT ANY WARRANTY; without even the implied warranty of      *)
(* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       *)
(* GNU Library General Public License for more details.                *)
(*                                                                     *)
(***********************************************************************)

(* $Id: p3lstream.ml,v 1.9 2007/01/23 15:50:04 weis Exp $ *)


(*                                *)
(*  UNDERLYING LAZY STREAM MODEL  *)
(*                                *)


(* Declaration *)

type 'a pac = tag list * ('a lazy_t) and tag = int * int ;;
(* m/n, m^th of [0,n] pkgs stream. n=-1 for infinite stream *)
type sign = Eos;; 
(* to be extended e.g. Eoos (End_of_one_stream) of tag *)

exception End_of_stream;;

type 'a pkg = Pac of 'a pac | Sign of sign;;

type 'a s = { mutable buf: 'a pkg list; mutable gen: unit -> 'a pkg } ;;


(* Constructor *)

(* At the end of stream, function f can keep raising any kind of exception
   or simply give "Sign Eos" repeatedly *)
let from_fun: (unit -> 'a pkg) -> 'a s =
  fun f ->
    { buf = []; 
      gen = f
    }
;;

let from_list: 'a pkg list -> 'a s = 
  fun l ->
    { buf = l;
      gen = fun _ -> Sign Eos
    }
;;

(* For construct empty stream, {buf=[]; gen=fun()->Sign Eos} is
   itself weakly typed, hence can't be exported. *)
let singleton: 'a pkg -> 'a s = 
  fun p -> 
    { buf = 
        (match p with
	      | Sign Eos -> []
	      | _ -> []
        );
      gen = fun () -> Sign Eos
    }
;;


(* Selector, destructive operations *)

let pkg_next: 'a s -> 'a pkg = 
  fun s ->
    match s.buf with
      | [] -> s.gen ()
      | h::t -> s.buf <- t; h
;;

let rec pkg_iter: ('a pkg -> unit) -> 'a s -> unit = 
  fun f s -> 
    match pkg_next s with
      | Pac _ as x -> (f x; pkg_iter f s)
      | Sign Eos -> ()
;;


(* Buffer Operation *)

(* appends (a) package(s) to the end of stream's buffer -- not the end of
   stream itself, i.e. to store pre-read packages. It's power shows in
   the operations like "split". *)
let buff_pkg: 'a pkg -> 'a s -> unit = fun p s -> s.buf <- s.buf@[p];;

let buff_list: 'a pkg list -> 'a s -> unit = fun l s -> s.buf <- s.buf@l;;






(*                           *)
(*  OUTSIDE INTERFACE MODEL  *)
(*                           *)


(* Declaration *)

type 'a t = 'a s;;

type color = int;;


(* Constructor *)

(* To keep compatability, here "of_fun" is defined in the place of
   "from" in the basic stream model *)
let of_fun: (unit -> 'a) -> 'a t = fun f ->
  let wf = 
    let i = ref (-1) in 
    fun () -> 
      (* Taking e before lazy it (instead of lazilize the taking action)
	 is absolutely necessary. If not, we alwasy produce the same
	 sequence when visiting elements in even different orders 
      *)
      try
        let e = f () in
        let () = i := !i + 1 in
        Pac([(!i,-1)], lazy e)
      with 
        | End_of_stream -> Sign Eos
        | x -> print_endline  "
               This exception is possibly raised by the expected end of your 
               input stream. If so, you should explictly raise a
               P3lstream.End_of_stream instead of any other kind of exceptions 
               (e.g. End_of_file). Because if we just try to catch ANY kind 
               of exception to produce the end, a real defeat (exception) would 
               be possibly covered by a normal termination together with some 
               incorrect result. This is definitely a dangerous choice --- 
               it's difficult for you to aware the answers is wrong and it's
               even more difficult for you to trace the reason.\n
               Anyway, I will still raise current exception for you to tell
               whether it is caused by the intended ending point (in this case, 
               you should fix it as suggested above) or a real unexpected 
               exception you should be aware.\n
               This warning message may appear several times due to the 
               recursive invocations and (and hence exits) of function calls. 
               Don't be panic and don't use Ctrl+C to break, wait until the real 
               exception message appears.\n" ;
            raise x in
  from_fun wf;;

let of_list: 'a list -> 'a t = fun l ->
  let len = (List.length l) - 1 in
  let wl = List.map 
    ( let i = ref (-1) in 
      fun x -> 
	i := !i + 1;
	Pac ([(!i, len)], lazy x)
    ) l in
  from_list wl;;

let of_array: 'a array -> 'a t = fun a ->
  let l = Array.to_list a in
  of_list l;;


(* Selector *)

let next: 'a t -> 'a = fun s ->
  match pkg_next s with
    | Sign Eos -> raise End_of_stream
    | Pac(_,v) -> Lazy.force v;;

let iter: ('a -> unit) -> 'a t -> unit = fun f s ->
  let mf = function
    | Pac(_,v) -> f (Lazy.force v)
    | Sign _ -> () in
  pkg_iter mf s;;


(* Export *)

let warning = "Since OcamlP3l now employs a lazy stream model as
underlying mechanics, it enables you to progressively compute (in
sequential mode) and I/O (in both modes) instead of compute and I/O
everything in a whole. To make use of such advantage, you should avoid
to use P3lstream.to_list and P3lstream.to_array though they are still
perfectly supported at this moment. This is because list/array are
static data structure, when using them as I/O interface, even the
internal processing is progressive, the program have to wait until all
the integrated data structure is computed before the I/O really
begins. P3lstream.iter, P3lstream.to_fun, P3lstream.next is
recommended.";;

let to_fun: 'a t -> (unit -> 'a) = fun s ->
  fun () -> next s;;

let to_list: 'a t -> 'a list = 
  fun s ->
    let () = print_endline warning in
    let rec ask repo =
      try
	    let v = next s in
	    ask (v::repo)
      with End_of_stream  -> repo in
    List.rev (ask []);;
  
let to_array: 'a t -> 'a array = 
  fun s -> 
    let () = print_endline warning in
    Array.of_list (to_list s);;










