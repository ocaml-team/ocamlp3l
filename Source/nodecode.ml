(***********************************************************************)
(*                                                                     *)
(*                          OCamlP3l                                   *)
(*                                                                     *)
(* (C) 2004-2007                                                       *)
(*             Roberto Di Cosmo (dicosmo@dicosmo.org)                  *)
(*             Zheng Li (zli@lip6.fr)                                  *)
(*             Pierre Weis (Pierre.Weis@inria.fr)                      *)
(*             Francois Clement (Francois.Clement@inria.fr)            *)
(*                                                                     *)
(* Based on original Ocaml P3L System                                  *)
(* (C) 1997 by                                                         *)
(*             Roberto Di Cosmo (dicosmo@ens.fr)                       *)
(*             Marco Danelutto (marcod@di.unipi.it)                    *)
(*             Xavier Leroy  (Xavier.Leroy@inria.fr)                   *)
(*             Susanna Pelagatti (susanna@di.unipi.it)                 *)
(*                                                                     *)
(* This program is free software; you can redistribute it and/or       *)
(* modify it under the terms of the GNU Library General Public License *)
(* as published by the Free Software Foundation; either version 2      *)
(* of the License, or (at your option) any later version.              *)
(*                                                                     *)
(* This program is distributed in the hope that it will be useful,     *)
(* but WITHOUT ANY WARRANTY; without even the implied warranty of      *)
(* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       *)
(* GNU Library General Public License for more details.                *)
(*                                                                     *)
(***********************************************************************)

(* $Id: nodecode.ml,v 1.33 2007/01/23 15:50:04 weis Exp $ *)

open Printf
open Unix
open Template
open Parp3l
open Commlib (* the architecture dependent low level communication layer *)

(* The root initialization code *)

(* first pass: send out closures 
               and gather input channel/virtual processors assoc list *)

let passone leafl mapping_c =
    debug (fun () -> printf "%t: Leafl has length %d" context (List.length leafl)); 
    List.map 
     (function (Leaf(templ,Some(Seqconf(n,Some inl,Some outl)),Some(f),col)) ->
      let ch = (List.assoc n mapping_c) in
       debug (fun () -> printf "%t has input list:" context; List.iter (fun i -> printf " %d " i) inl); 
       debug (fun () -> printf "%t has output list:" context; List.iter (fun i -> printf " %d " i) outl); 
       debug (fun () -> printf "%t: Sending closure %s to processor %d" context (string_of_template templ) n); 
      cl_send ch (n,inl,outl,templ,f);
       debug (fun () -> printf "%t: sent closure to processor %d" context n); 
       debug (fun () -> printf "%t: getting inch from virtual processor %d" context n);
      (n,((receive ch): vp_extern))
      | _ -> failwith "Nodecode: unknown structure in passone"
     )
    leafl;;

(* second pass: send out output channel lists to virtual processors *)

let passtwo inch_al nodeassoc mapping_c =
    List.iter
      (fun (n,nl) ->
        let ch = (List.assoc n mapping_c) in
        let rec translate_addr = function
            [] -> []
          | x :: l -> List.assoc x inch_al :: translate_addr l in
        let outcl = translate_addr nl in
        debug (fun () -> printf "%t: output list for %d is " context n; 
                         List.iter vp_extern_print outcl);
        cl_send ch outcl;
        debug(fun () ->  printf "%t: output channel for node %d is (%s)%s" context n (Commlib.addr_of_vpch ~peer:false ch) (Commlib.addr_of_vpch ch));
	vp_close ch;
	()
      ) 
    nodeassoc;;

(* fstnode: the node we have to send data to
   leafs: the list of configuration data for the leaf nodes
   mapping_al: an association list mapping virtual processors to inet addresses
   io_al: an association list mapping p3lroot virtual processor(s) to vpio data to connect to parfuns
   rootfun: the multi-threaded function feeding data and receiving results *)

let root ((fstnode,leafs),mapping_al,io_al) =
  (* set a fake vpinfo *)
  Commlib.setmyvpinfo (-1000);
  (* establish all the connections to the mapped nodes *)
   debug (fun () -> printf "%t: Mapping_al has length %d" context (List.length mapping_al)); 

(********* DA RIVEDERE QUI!!!!!!! spostare elegantemente a commlib.ml ********)

  let mapping_c = List.map vpchan_of_addr mapping_al in
   debug (fun () -> printf "%t: P3L root ready\nNow init nodes and gather input channels." context);
  (* first pass: send out closures 
     and gather input channel/virtual processors assoc list *)
  let slave_inch_al = (passone leafs mapping_c) in 
  (* set up all p3lroot inchannels *)
  let master_inch_al = List.map (fun (n,_,_) -> (n,(mkvp n 1))) io_al in
  (* now we have the whole picture *)
  let inch_al = slave_inch_al@(List.map (fun (n,vp) -> (n,vp_extern vp)) master_inch_al) in
  debug (fun () -> 
    printf "END PASS ONE"; print_newline();
    printf "%t: result of pass one is " context;
    List.iter (fun (n, vpx) -> printf "%d -> " n; vp_extern_print vpx) inch_al;
    print_newline();
    printf "%t: Now sending output channels to nodes..." context);
  (* second pass: send out output channel lists to virtual processors *)
  let nodeassoc = (mkoutnodesassoc leafs) in
  passtwo inch_al nodeassoc mapping_c;
  (* Now close the initialisation connections *)

  (* final step: we are virtual processor -1 and we need to connect
     the ic/oc structures of each parfun to the correct node in the
     network that will receive the input data stream(s) from us and
     send back the result stream(s).
   *)
  (* connect the output of the last nodes to the input channels of the master node *)
  List.iter2
    (fun (n,icr,ocr) (m,vpchan) ->
      if n<>m then failwith "Nodecode.root failed assertion: io_al and master_inch_al are not in same order!";
      debug (fun () -> Printf.printf "Setting up receive channel on master for parfun %d(=%d)..." n m);
      icr:=Some (setup_receive_chan vpchan);
      debug (fun () -> Printf.printf "done.")
    )
    io_al master_inch_al;
  (* connect the input of the first nodes to the output channel of the master node *)
  let inht = mkinnodesassoc leafs in (* build hashtbl mapping input nodes to their peers *)
  let ocrht =                        (* hashtbl mapping input nodes to parfuns' outchans *)
    let ht = Hashtbl.create 16
    in (List.iter (fun (n,_,ocr) -> Hashtbl.add ht n ocr) io_al; ht)
  in Hashtbl.iter (fun n ocr -> let peers = (Hashtbl.find_all inht n) in
		   if (List.length peers) <> 1
		   then failwith ("Nodecode.root: incorrect number of peers for input node"^(string_of_int n))
		   else
		     begin
                       let peer=List.hd peers in
                       let peervpext=List.assoc peer slave_inch_al
                       in let coutl = outchs_of_vps [peervpext] (List.assoc n master_inch_al) in
		       debug (fun () -> Printf.printf "Setting up output channel on master for %d connected to %d..." n peer);
		       ocr:=Some (List.hd coutl)
		     end
                  ) ocrht;
  debug (fun () -> printf "%t: Done." context); 
  ;;
  

(* the node code: just establish a server and call the config function *)

let fanin_of templ=
 let i = 
  match templ with
  | Farmcoll n -> n
  | Mapcoll n -> n
  | Reducecoll n -> n
  | Loopdist -> 2
  | _ -> 1 (* check in the future if this is correct for all the templates! *)
 in i;;

let nodeconfiguration ch = 
 (* setup the in/out channels via the distributed channel allocation
    protocol USING UNIX SOCKETS. 
    WARNING: this strategy is dangerous: there could be a deadlock if
    connection dependencies are circular. Should not be the case with
    the initial and final channels being DIFFERENT nodes otherwise,
    one needs a multi-threaded implementation of this stuff ...  *)

  (* we must declare the communication channel "close on exec" to be sure that,
     if the user code does any fork, the forked process do not keep any reference
     to this channel, which must be invisible for the user *)

  (* in principle, we should do this on ALL channels used by ocamlp3l, but it 
     seems that this is sufficient for now, since the user, reasonably, should
     only be able to fork inside a seq skeleton, and not in nodes having more
     than one i/o channel. But if you start seeing double output on your
     debug coming up, start setting this flag all over *)

  ch_set_close_on_exec ch;

    (* get the template and the user function from the root *)
     debug (fun () -> printf "%t: now trying to get the closure" context); 
    let (n,inl,outl,templ,f) = receive ch in
    debug ~mask:255 (fun () -> printf "%t: got the closure, node [%d] with template %s\n" context n (string_of_template templ));
    Commlib.setmyvpinfo n;
    (* create the input channel | vp data *)
    let myvpdata = mkvp n (fanin_of templ) in
    (* send back the inchannel address | externable vp data *)
    cl_send ch (vp_extern myvpdata);
    debug (fun () -> printf "%t: done sending inch address" context); 
    (* now wait for our output VpX list and build the connections *)
    let outvpxl = (receive ch) in
    debug (fun () -> printf "%t: address list is " context; List.iter vp_extern_print outvpxl);
    let coutl = outchs_of_vps outvpxl myvpdata in

    (* close the communications channel with the root *)
    vp_close ch;

    (* we can now finally start the real work *)

(***************** rimpiazzare qui doaccept sockin con "receive_chans inl" ****)
(* questo deve selezionare gli ingressi previsti in inl (numero di nodo virtuale)
   per metterli in una lista di VpIn(n,ch)... nel buon ordine!
   Nel caso di MPI mi ritrovo esattamente con inl *)

    match (templ,f) with
      (Userfun,Seqfun f) -> seqtempl f (setup_receive_chan myvpdata) (List.hd coutl)
    | (DemandUserfun,Seqfun f) -> (* (debug (fun () -> printf "GOT demanduserfun")); *)
                       let ic = (setup_receive_chan myvpdata)
                       in demandseqtempl f ic (List.hd coutl)
    | (Starter,Startfun (f,initf)) -> startempl f initf (setup_receive_chan myvpdata) (List.hd coutl)
    | (Stopper,Stopfun (f,initf,finalize)) -> stoptempl f initf finalize (setup_receive_chan myvpdata) (List.hd coutl)
    | (Farmemit,OutChanSel f)  -> farmetempl f (setup_receive_chan myvpdata) coutl
    | (ExtDemandFarmemit,OutChanSel f) -> extdemandfarmetempl f 
                           (setup_receive_chan myvpdata) coutl
    | (IntDemandFarmemit,OutChanSel f) -> let ic = (setup_receive_chan myvpdata)
                           in intdemandfarmetempl f ic coutl
    | (Farmcoll n,InChanSel f) -> 
          farmctempl f (setup_receive_chans inl myvpdata " FarmColl ") (List.hd coutl)
    | (Mapemit,OutChanSel f) -> mapetempl f (setup_receive_chan myvpdata) coutl
      | (Mapcoll n,InChanSel f) ->
          mapctempl f (setup_receive_chans inl myvpdata " MapColl ") (List.hd coutl)
(*
      | (Reduceseqworker,Seqfun f) -> (* here we do the fold locally! *)
          redseqtempl f (setup_receive_chan myvpdata) (List.hd coutl) 
 *)
      | (Reduceemit n,OutChanSel f) ->
	  reduceparetempl f (setup_receive_chans inl myvpdata " ReduceEmit ") coutl
      | (Reducecoll nw,InChanSel f) ->
          reduceparctempl f (setup_receive_chans inl myvpdata " ReduceColl ") coutl
      | (Loopdist,InChanSel f) ->
          loopdisttempl f (setup_receive_chans inl myvpdata " LoopDist ") (List.hd coutl)
      | (Condtester,CondChanSel f) -> debug (fun () -> printf "PID %d Condition tester with %d out channels"
                      (Unix.getpid()) (List.length coutl));
                      condtestertempl f (setup_receive_chan myvpdata) coutl
      | _ -> failwith "Nodecode: illegal template or template/function combination found in nodeconfiguration"

;;

let node () =
  debug (fun () -> printf "%t: Node client" context; print_newline());
  establish_smart_server nodeconfiguration;;
   
(* here is the main: we assume that the root process is given a processor pool
   as a parameter *)


let main expr =
   let setdebuglevel n = Basedefs.dbg:=true; Basedefs.dbgmask:=n in
   let setipnumber ip = debug (fun () -> printf "Forcing IP addres %s\n" ip);Basedefs.ip:=Some ip in
   let isroot=ref false in
   let strictflag = ref false in
   let procpool=ref [] in
   let collect_processors proc = procpool:=proc::(!procpool)
   in
   debug (fun () -> printf "%t: in main expr" context);
   Arg.parse 
   (  [
   ("--p3lroot", Arg.Set(isroot), "Declare this as the root node.");
   ("--dynport", Arg.Set(Basedefs.dynport), "Force a node to use a dynamic port number instead of p3lport and output it.");
   ("--debug",Arg.Int(setdebuglevel), "Enable debugging for this node at level n. Currently all levels are equal.");
   ("-ip",Arg.String(setipnumber), "Force an ip address. Useful when you are on a laptop named localhost");
   ("--strict", Arg.Set(strictflag), "Specify a strict mapping between physical and virtual processors");
  ]
  @(Command_options.all ()) (* get all options, including those set by the programmer *)

  )
   collect_processors ("Usage: "^Sys.argv.(0));
      (* here we open the closure which is really expr, so the parsing and allocation stuff *)
      (* is only done on the root node and not the other n generic nodes                    *) 
   if !isroot then

     let pprocpool =
       let ht = Hashtbl.create 16 in
	 List.iter 
	   (fun x -> 
	     let ((proc,port),col,cap) = proc_inet_addr x in 
	     let capext = if !strictflag then (abs cap) else cap in
	     let capold = try Hashtbl.find ht (proc,port,col) with Not_found -> 0 in
	       Hashtbl.replace ht (proc,port,col) (if (capold < 0 || capext < 0) then -1 else capold+capext)
	   ) 
	   !procpool;
	 Hashtbl.fold (fun (proc,port,col) cap  l -> ((proc,port),col,cap)::l) ht [] in

     begin (* init parallel networks *)
       root (Parp3l.p3ldoallparfuns !strictflag (List.map (fun (a,b,e) -> (a,b,e ())) !Parp3l.parfuns) pprocpool);
       debug (fun () -> printf "%t: root inited parallel networks\n" timedcontext);
       expr(); (* run the main code in the root node *)
       debug (fun () -> printf "%t: root finished running root code\n" timedcontext);
       List.iter
	 (function 
	   | (_,{contents = Some oc},_)
	     -> (cl_send oc Template.EndStream;
	     debug (fun () -> printf "%t: sent EndStream for network\n"
       timedcontext))
	   | _ -> assert false)
	 !Parp3l.parfuns; (* send EnsStream to all networks *)
       List.iter
	 (function 
	   | ({contents = Some ic},_,_)
	     -> (match (receive ic) with
		 Template.EndStream -> (debug (fun () -> printf "%t: got EndStream for network\n" timedcontext))
	       | _ -> failwith "Nodecode.main received unexpected packet type
       while expecting EndStream")
	   | _ -> assert false
	 ) !Parp3l.parfuns (* Wait for network termination, getting EndStream from each of them *)
     end (* we are done *)
   else node ();;


let pardo expr = Printexc.catch (fun () -> try Parp3l.insidepardo:=true; main expr; Parp3l.insidepardo:=false (* protect pardo scope *)
   with Unix_error(e,_,_) -> debug ~mask:255 (fun () -> printf "%t: got unix error: %s" context (Unix.error_message e));
   | e -> debug ~mask:255 (fun () -> printf "%t: got exception." context); raise e) ()
   ;;



(* function for i/o with streams, for communicating with parfuns *)

let pktnum = ref 0;; (* should be local to cl_send_stream! *)

let cl_send_stream oc is =
  P3lstream.iter
    (fun p ->
      Commlib.cl_send oc (Template.UserPacket (p,!pktnum,[Template.Seqtag]));
      debug(fun () -> printf "%t: parfun delivered (pkt %d)" timedcontext (!pktnum));
      pktnum:=!pktnum+1)
    is;
  Commlib.cl_send oc Template.EndOneStream;
  debug(fun () -> printf "%t: parfun sent EndOneStream" timedcontext);;


let receive_stream ic = P3lstream.of_fun 
  ( let flag = ref true in
    fun () ->  
      if (not !flag) then raise P3lstream.End_of_stream
      else 
	match (Commlib.receive ic) with
          | Template.UserPacket (p,pktnum,tl) -> p
	  | Template.EndOneStream -> 
	      debug 
		(fun () -> printf "%t: parfun receivedEndOneStream" timedcontext);
	      flag := false;
	      raise P3lstream.End_of_stream
	  | Template.EndLoop _ | Template.EndOneLoop _ 
	  | Template.EndStream -> 
	      failwith "Unexpected pkg type in Nodecode.receive_stream." 
  );;
	

(* problem with generalization of variables! *)
let parfun (expr: unit -> ('a,'b) Parp3l.p3ltree) =
  let ic,oc = ref None, ref None in (* set up the references to the i/o channels *)
  Parp3l.addskel ic oc expr;        (* add the i/o references and the skeleton expression to the Parp3l global data structure    *)
  fun (is: 'a P3lstream.t) ->         (* when this function will be called, the i/o channels will have been instanciated in Parp3l *)
    match (!ic,!oc) with
      | Some ic, Some oc -> 
          let () = spawn (cl_send_stream oc) is in
          let (os: 'b P3lstream.t) = receive_stream ic in 
          os
      | _ -> failwith "Uninitialised i/o channels while executing a parfun created function"
;;
