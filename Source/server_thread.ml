(***********************************************************************)
(*                                                                     *)
(*                          OCamlP3l                                   *)
(*                                                                     *)
(* (C) 2004-2007                                                       *)
(*             Roberto Di Cosmo (dicosmo@dicosmo.org)                  *)
(*             Zheng Li (zli@lip6.fr)                                  *)
(*             Pierre Weis (Pierre.Weis@inria.fr)                      *)
(*             Francois Clement (Francois.Clement@inria.fr)            *)
(*                                                                     *)
(* Based on original Ocaml P3L System                                  *)
(* (C) 1997 by                                                         *)
(*             Roberto Di Cosmo (dicosmo@ens.fr)                       *)
(*             Marco Danelutto (marcod@di.unipi.it)                    *)
(*             Xavier Leroy  (Xavier.Leroy@inria.fr)                   *)
(*             Susanna Pelagatti (susanna@di.unipi.it)                 *)
(*                                                                     *)
(* This program is free software; you can redistribute it and/or       *)
(* modify it under the terms of the GNU Library General Public License *)
(* as published by the Free Software Foundation; either version 2      *)
(* of the License, or (at your option) any later version.              *)
(*                                                                     *)
(* This program is distributed in the hope that it will be useful,     *)
(* but WITHOUT ANY WARRANTY; without even the implied warranty of      *)
(* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       *)
(* GNU Library General Public License for more details.                *)
(*                                                                     *)
(***********************************************************************)

(* $Id: server_thread.ml,v 1.4 2007/01/23 15:50:04 weis Exp $ *)

open Printf;;
open Unix;;
open Basedefs;;

(* Print context for debugging *)
let context chan =
  fprintf chan "PID %d [thread %d] (on %s)" (getpid()) (Thread.id (Thread.self())) (gethostname());;

let timedcontext chan =
  fprintf chan "PID %d(%f) [thread %d] [on %s]" (getpid()) (gettimeofday()) (Thread.id (Thread.self())) (gethostname());;

(* WARNING: there is up to now no sensible way to marshal functions that
   may access an existing mutex on the destination node, so we cannot do
   debug reasonably, and we just fail when invoked in debug mode *)

(* an output function for debugging *)

let indebug = ref false;;

let debug ?(mask = 1) f = 
  if !dbg && mask land !dbgmask > 0
  then
    (* avoid locking iolock when already locked *)
    if !indebug then (printf "[Debug inside debug]";f ()) else 
    failwith
      "Cannot use mutexes to properly serialize debug output\
     \nin the thread model.\ 
     \nDo your debugging in the process model, and then run\
     \nthe thread model without debugging."
;;

(* locking and unlocking functions with debug *)

let lock m s =
  if !dbg then (printf "%t: locking mutex %s..." timedcontext s;print_newline());
  Mutex.lock m;
  if !dbg then (printf "%t: locked mutex %s" timedcontext s;print_newline())
;;

let unlock m s =
  if !dbg then (printf "%t: unlocking mutex %s..." timedcontext s;print_newline());
  Mutex.unlock m;
  if !dbg then (printf "%t: unlocked mutex %s" timedcontext s;print_newline())
;;

(* This function makes a value local to the process/thread *)

let local = Mutex.create();;

let mklocalref vdef = 
  let ht = Hashtbl.create 16 in
  let setlocal v =
    lock local "local";
    Hashtbl.replace ht (Thread.id (Thread.self())) v;
    unlock local "local" in 
  let getlocal () =
    let v =
      lock local "local";
      (try
	Hashtbl.find ht (Thread.id (Thread.self()))
      with _ -> (Hashtbl.replace ht (Thread.id (Thread.self())) vdef; vdef));
    in (unlock local "local"; v) in
  (getlocal,setlocal);;


(* Establish a server on an input socket, keeping track of sons and exiting when no sons left *)

let establish_smart_server servfun =
  let child_count = ref 0 in
  let child_count_mutex = Mutex.create() in
  let sock = socket PF_INET SOCK_STREAM 0 in
  begin
    match !dynport with
      true -> bind sock (ADDR_INET(inet_addr_any,0));
	let port = match Unix.getsockname sock with
	    ADDR_INET(_,x) -> x | _ -> assert false
        in printf "dynport=%4d\n" port; print_newline()
    | false ->  bind sock (ADDR_INET(inet_addr_any,p3lport))
  end;
  debug (fun () -> printf "%t: Node client bound socket" context; print_newline());
  listen sock 3;
  debug (fun () -> printf "%t: Node client listens to socket" context; print_newline());
  let new_configuration (s,caller) =
    servfun s caller;
    (* N.B.: do NOT try to close s here: s points to whatever was the result of the latest accept
             executed just before arriving here, and in am ultithreaded environment, this can be
             anything! *)
    debug (fun () -> printf "%t: thread %d terminated" context (Thread.id (Thread.self())));
    Mutex.lock child_count_mutex;
    decr child_count;
    if !child_count = 0 then begin
      debug (fun () -> printf "%t: no more children, exiting" context; print_newline());
      exit 0
    end;
    Mutex.unlock child_count_mutex in
  while true do
    debug (fun () -> printf "%t: Node client accepting socket" context; print_newline());
    let (s, caller) = accept sock in
    debug (fun () -> printf "%t: Node client accepted socket" context; print_newline());
    Mutex.lock child_count_mutex;
    incr child_count;
    Mutex.unlock child_count_mutex;
    ignore (Thread.create new_configuration (s, caller))
  done;;

let spawn func arg = ignore (Thread.create func arg);;
