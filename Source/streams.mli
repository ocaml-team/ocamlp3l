(***********************************************************************)
(*                                                                     *)
(*                          OCamlP3l                                   *)
(*                                                                     *)
(* (C) 2004-2007                                                       *)
(*             Roberto Di Cosmo (dicosmo@dicosmo.org)                  *)
(*             Zheng Li (zli@lip6.fr)                                  *)
(*             Pierre Weis (Pierre.Weis@inria.fr)                      *)
(*             Francois Clement (Francois.Clement@inria.fr)            *)
(*                                                                     *)
(* Based on original Ocaml P3L System                                  *)
(* (C) 1997 by                                                         *)
(*             Roberto Di Cosmo (dicosmo@ens.fr)                       *)
(*             Marco Danelutto (marcod@di.unipi.it)                    *)
(*             Xavier Leroy  (Xavier.Leroy@inria.fr)                   *)
(*             Susanna Pelagatti (susanna@di.unipi.it)                 *)
(*                                                                     *)
(* This program is free software; you can redistribute it and/or       *)
(* modify it under the terms of the GNU Library General Public License *)
(* as published by the Free Software Foundation; either version 2      *)
(* of the License, or (at your option) any later version.              *)
(*                                                                     *)
(* This program is distributed in the hope that it will be useful,     *)
(* but WITHOUT ANY WARRANTY; without even the implied warranty of      *)
(* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       *)
(* GNU Library General Public License for more details.                *)
(*                                                                     *)
(***********************************************************************)

(* $Id: streams.mli,v 1.3 2007/01/23 15:50:04 weis Exp $ *)

val streamer :
  ('a P3lstream.pkg -> 'b P3lstream.pkg) * P3lstream.color ->
  'a P3lstream.s -> 'b P3lstream.s
val dive :
  ('a P3lstream.pkg -> 'b P3lstream.pkg array) * P3lstream.color ->
  'a P3lstream.s -> 'b P3lstream.s
val unif :
  ('a P3lstream.pkg list -> ('b P3lstream.pkg * 'a P3lstream.pkg list) option) *
  P3lstream.color -> 'a P3lstream.s -> 'b P3lstream.s
val split :
  int * ('a P3lstream.pkg -> int) * P3lstream.color ->
  'a P3lstream.s -> 'a P3lstream.s array
val combine :
  (unit -> int) * P3lstream.color -> 'a P3lstream.s array -> 'a P3lstream.s
val switch :
  ('a P3lstream.pkg -> bool) * P3lstream.color ->
  'a P3lstream.s -> 'a P3lstream.s * 'a P3lstream.s
val recur :
  int * int ->
  ('a P3lstream.s array -> 'a P3lstream.s array) ->
  'a P3lstream.s array -> 'a P3lstream.s array
