(***********************************************************************)
(*                                                                     *)
(*                          OCamlP3l                                   *)
(*                                                                     *)
(* (C) 2004-2007                                                       *)
(*             Roberto Di Cosmo (dicosmo@dicosmo.org)                  *)
(*             Zheng Li (zli@lip6.fr)                                  *)
(*             Pierre Weis (Pierre.Weis@inria.fr)                      *)
(*             Francois Clement (Francois.Clement@inria.fr)            *)
(*                                                                     *)
(* Based on original Ocaml P3L System                                  *)
(* (C) 1997 by                                                         *)
(*             Roberto Di Cosmo (dicosmo@ens.fr)                       *)
(*             Marco Danelutto (marcod@di.unipi.it)                    *)
(*             Xavier Leroy  (Xavier.Leroy@inria.fr)                   *)
(*             Susanna Pelagatti (susanna@di.unipi.it)                 *)
(*                                                                     *)
(* This program is free software; you can redistribute it and/or       *)
(* modify it under the terms of the GNU Library General Public License *)
(* as published by the Free Software Foundation; either version 2      *)
(* of the License, or (at your option) any later version.              *)
(*                                                                     *)
(* This program is distributed in the hope that it will be useful,     *)
(* but WITHOUT ANY WARRANTY; without even the implied warranty of      *)
(* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       *)
(* GNU Library General Public License for more details.                *)
(*                                                                     *)
(***********************************************************************)

(* $Id: parp3l.mli,v 1.4 2007/01/23 15:50:04 weis Exp $ *)

(* Partools.mli *)
type ('a, 'b) io = unit

type ('a, 'b) p3ltree =
  | Farm of int * int list * (('a, 'b) p3ltree * int)
  | Pipe of ('a, 'b) p3ltree list
  | Map of (int * int list * (('a, 'b) p3ltree * int))
  | Reduce of (int * int list * (('a, 'b) p3ltree * int))
  | Reduceseqfun of (int * (('a, 'b) io -> 'a -> 'b))
  | Seq of (int * (('a, 'b) io -> 'a -> 'b))
  | Cond of (int * (('a, 'b) p3ltree * ('b -> bool)))
  | Start of ((unit -> 'b) * (unit -> unit))
  | Stop of (('a -> unit) * (unit -> unit) * (unit -> unit))
type parfun =
    Commlib.vp_chan option ref * Commlib.vp_chan option ref *
    (unit -> (unit, unit) p3ltree)
val parfuns : parfun list ref
val insidepardo : bool ref
val addskel :
  Commlib.vp_chan option ref -> Commlib.vp_chan option ref -> 'a -> unit
type nproc = int
type cin = int
type cout = int
type color = int
type proctemplate =
  | Userfun
  | DemandUserfun
  | Farmemit
  | IntDemandFarmemit
  | ExtDemandFarmemit
  | Farmcoll of int
  | Mapemit
  | Mapcoll of int
  | Mapworker
  | Reduceemit of int
  | Reducecoll of int
  | Reduceworker
  | Reduceseqemit of int
  | Reduceseqcoll of int
  | Reduceseqworker
  | Starter
  | Stopper
  | Condtester
  | Loopdist
  | Farmworker
  | PipeStage
val string_of_template : proctemplate -> string
type config =
  | Seqconf of nproc * cin list option * cout list option
  | Inconf of cin option * cout option
type ('a, 'b) p3ltreeexp = Innode of ('a, 'b) innode | Leaf of ('a, 'b) node
and ('a, 'b) innode =
  | Farmexp of ('a, 'b) node * ('a, 'b) node * ('a, 'b) p3ltreeexp list *
      config option
  | Mapexp of ('a, 'b) node * ('a, 'b) node * ('a, 'b) p3ltreeexp list *
      config option
  | Reduceexp of ('a, 'b) node * ('a, 'b) node * ('a, 'b) p3ltreeexp list *
      config option
  | Reduceseqexp of ('a, 'b) node * ('a, 'b) node *
      ('a, 'b) p3ltreeexp list * config option
  | Pipexp of ('a, 'b) p3ltreeexp list * config option
  | Condexp of ('a, 'b) p3ltreeexp * ('a, 'b) node * ('a, 'b) node *
      config option
and ('a, 'b) node =
    proctemplate * config option * ('a, 'b) action option * color
and ('a, 'b) action =
  | Seqfun of (('a, 'b) io -> 'a -> 'b)
  | Startfun of ((unit -> 'b) * (unit -> unit))
  | Stopfun of (('a -> unit) * (unit -> unit) * (unit -> unit))
  | CondChanSel of ('b -> bool)
  | OutChanSel of (Commlib.vp_chan list -> Commlib.vp_chan)
  | InChanSel of (Commlib.vp_chan list -> 'a * Commlib.vp_chan)
  | InChanSelAndFun of (Commlib.vp_chan list -> 'a * Commlib.vp_chan) *
      ('a -> 'b)
val do_nothing : 'a -> unit
val genconfig : unit -> config option
val resetconfig : unit -> unit
val curconfig : unit -> nproc
val get_color : int -> int -> int
val nvlist : int -> int -> int list -> int list
val expand : color -> ('a, 'b) p3ltree -> ('a, 'b) p3ltreeexp
val allocnode : 'a * 'b * 'c * 'd -> 'a * config option * 'c * 'd
val leafalloc : ('a, 'b) p3ltreeexp -> ('a, 'b) p3ltreeexp
val chconnect :
  cin list ->
  cout list -> 'a * config option * 'b * 'c -> 'a * config option * 'b * 'c
val pe_of_node : 'a * config option * 'b * 'c -> nproc
val getchans : ('a, 'b) p3ltreeexp -> nproc * nproc
val nodeconf : ('a, 'b) p3ltreeexp -> ('a, 'b) p3ltreeexp
val bindchan :
  cin list -> cout list -> ('a, 'b) p3ltreeexp -> ('a, 'b) p3ltreeexp
val spreadc :
  cin ->
  cout -> ('a, 'b) p3ltreeexp list -> ('a, 'b) p3ltreeexp list * (cin * cout)
val rr : unit -> 'a list -> 'a
val confdist : 'a * 'b * 'c * 'd -> 'a * 'b * ('e, 'f) action option * 'd
val confemit : 'a * 'b * 'c * 'd -> 'a * 'b * ('e, 'f) action option * 'd
val confcollect : 'a * 'b * 'c * 'd -> 'a * 'b * ('e, 'f) action option * 'd
val confreduceseqcollect :
  'a * 'b * ('c, 'd) action option * 'e ->
  'a * 'b * (('c, 'd) io, 'c -> 'd) action option * 'e
val leafs_of : ('a, 'b) p3ltreeexp -> ('a, 'b) p3ltreeexp list
val p3ltreeexpr_it :
  (('a, 'b) p3ltreeexp -> ('a, 'b) p3ltreeexp) ->
  ('a, 'b) p3ltreeexp -> ('a, 'b) p3ltreeexp
val demandadjust : ('a, 'b) p3ltreeexp -> ('a, 'b) p3ltreeexp
val p3ldo :
  cin -> cout -> ('a, 'b) p3ltree -> nproc * ('a, 'b) p3ltreeexp list * nproc
val mkoutnodesassoc : ('a, 'b) p3ltreeexp list -> (nproc * cout list) list
val mkinnodesassoc : ('a, 'b) p3ltreeexp list -> (cin, nproc) Hashtbl.t
val mknodecol : ('a, 'b) p3ltreeexp list -> (nproc * color) list
val pair :
  bool ->
  (int * int) list ->
  ((Unix.inet_addr * int) * int * int) list ->
  (int * (Unix.inet_addr * int)) list
val p3ldoallparfuns :
  bool ->
  ('a * 'b * ('c, 'd) p3ltree) list ->
  ((Unix.inet_addr * int) * color * int) list ->
  (nproc * ('c, 'd) p3ltreeexp list) *
  (nproc * (Unix.inet_addr * int)) list * (cin * 'a * 'b) list

(* This is semantics.mli *)
val seq : ?col:int -> (('a, 'b) io -> 'a -> 'b) -> ('a, 'b) p3ltree
val farm :
  ?col:int -> ?colv:int list -> ('a, 'b) p3ltree * int -> ('a, 'b) p3ltree
val mapvector :
  ?col:int ->
  ?colv:int list -> ('a, 'b) p3ltree * int -> ('a array, 'b array) p3ltree
val reducevector :
  ?col:int ->
  ?colv:int list -> ('a * 'a, 'a) p3ltree * int -> ('a array, 'a) p3ltree
val pipeline : ('a, 'b) p3ltree -> ('b, 'c) p3ltree -> ('a, 'c) p3ltree
val ( ||| ) : ('a, 'b) p3ltree -> ('b, 'c) p3ltree -> ('a, 'c) p3ltree
val loop : ?col:int -> ('a -> bool) * ('b, 'a) p3ltree -> ('b, 'a) p3ltree
