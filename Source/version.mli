(***********************************************************************)
(*                                                                     *)
(*                          OCamlP3l                                   *)
(*                                                                     *)
(* (C) 2004-2007                                                       *)
(*             Roberto Di Cosmo (dicosmo@dicosmo.org)                  *)
(*             Zheng Li (zli@lip6.fr)                                  *)
(*             Pierre Weis (Pierre.Weis@inria.fr)                      *)
(*             Francois Clement (Francois.Clement@inria.fr)            *)
(*                                                                     *)
(* Based on original Ocaml P3L System                                  *)
(* (C) 1997 by                                                         *)
(*             Roberto Di Cosmo (dicosmo@ens.fr)                       *)
(*             Marco Danelutto (marcod@di.unipi.it)                    *)
(*             Xavier Leroy  (Xavier.Leroy@inria.fr)                   *)
(*             Susanna Pelagatti (susanna@di.unipi.it)                 *)
(*                                                                     *)
(* This program is free software; you can redistribute it and/or       *)
(* modify it under the terms of the GNU Library General Public License *)
(* as published by the Free Software Foundation; either version 2      *)
(* of the License, or (at your option) any later version.              *)
(*                                                                     *)
(* This program is distributed in the hope that it will be useful,     *)
(* but WITHOUT ANY WARRANTY; without even the implied warranty of      *)
(* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       *)
(* GNU Library General Public License for more details.                *)
(*                                                                     *)
(***********************************************************************)

(* $Id: version.mli,v 1.3 2007/01/23 15:50:04 weis Exp $ *)

(* Handling versioning of the software. *)

val print_version : unit -> unit;;
 (** Print the current version of the software on [stderr].
   The version number should be suitable for the format string
   ["%i.%02i\[.%i\]"].
   Intended use of this function is to handle the [-v] command line
   option parsed with the [Arg] standard library module. *)
val print_full_version : unit -> unit;;
 (** Same as above for the full version of the software (normally the
   full version should state a development information and a date, suitable
   for the format string ["%i.%02i\[.%i\]+%s (%s)"]). *)

val version : string;;
val full_version : string;;

 (** The value of the current OCamlP3l version ([version] has
  format ``version'', [full_version] has format
  ``version+subversion (date)'', reminiscent ot the versionning scheme
  of the Caml compiler). *)
