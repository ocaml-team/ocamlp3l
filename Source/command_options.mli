(***********************************************************************)
(*                                                                     *)
(*                          OCamlP3l                                   *)
(*                                                                     *)
(* (C) 2004-2007                                                       *)
(*             Roberto Di Cosmo (dicosmo@dicosmo.org)                  *)
(*             Zheng Li (zli@lip6.fr)                                  *)
(*             Pierre Weis (Pierre.Weis@inria.fr)                      *)
(*             Francois Clement (Francois.Clement@inria.fr)            *)
(*                                                                     *)
(* Based on original Ocaml P3L System                                  *)
(* (C) 1997 by                                                         *)
(*             Roberto Di Cosmo (dicosmo@ens.fr)                       *)
(*             Marco Danelutto (marcod@di.unipi.it)                    *)
(*             Xavier Leroy  (Xavier.Leroy@inria.fr)                   *)
(*             Susanna Pelagatti (susanna@di.unipi.it)                 *)
(*                                                                     *)
(* This program is free software; you can redistribute it and/or       *)
(* modify it under the terms of the GNU Library General Public License *)
(* as published by the Free Software Foundation; either version 2      *)
(* of the License, or (at your option) any later version.              *)
(*                                                                     *)
(* This program is distributed in the hope that it will be useful,     *)
(* but WITHOUT ANY WARRANTY; without even the implied warranty of      *)
(* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       *)
(* GNU Library General Public License for more details.                *)
(*                                                                     *)
(***********************************************************************)

(* $Id: command_options.mli,v 1.3 2007/01/23 15:50:04 weis Exp $ *)

val all : unit -> (string * Arg.spec * string) list;;
(** Return the list of options defined by one of the option creation
   functions of this module. *)

val add : string -> Arg.spec -> string -> unit;;
(** [add opt spec man] add the option [opt] to the command line
   with specification [spec] and man info [man]. *)

val debug : string -> string -> string -> unit;;
(** [debug opt man] add the option flag [opt] to the command line
   with associated option [opt] and man info [man].
   The flag is false by default and can be set with [opt].
   [debug] returns a function that prints its argument on [stderr] 
   when the flag is set. *) 

val flag : bool -> string -> string -> bool ref;;
(** [flag init opt man] add the option flag [opt] to the command line
   with associated option [opt] and man info [man].
   The flag has initial value [init] and the option sets the flag
   (resp. clears the flag) if the initial value is [false]
   (resp. is [true]). *)
