(***********************************************************************)
(*                                                                     *)
(*                          OCamlP3l                                   *)
(*                                                                     *)
(* (C) 2004-2007                                                       *)
(*             Roberto Di Cosmo (dicosmo@dicosmo.org)                  *)
(*             Zheng Li (zli@lip6.fr)                                  *)
(*             Pierre Weis (Pierre.Weis@inria.fr)                      *)
(*             Francois Clement (Francois.Clement@inria.fr)            *)
(*                                                                     *)
(* Based on original Ocaml P3L System                                  *)
(* (C) 1997 by                                                         *)
(*             Roberto Di Cosmo (dicosmo@ens.fr)                       *)
(*             Marco Danelutto (marcod@di.unipi.it)                    *)
(*             Xavier Leroy  (Xavier.Leroy@inria.fr)                   *)
(*             Susanna Pelagatti (susanna@di.unipi.it)                 *)
(*                                                                     *)
(* This program is free software; you can redistribute it and/or       *)
(* modify it under the terms of the GNU Library General Public License *)
(* as published by the Free Software Foundation; either version 2      *)
(* of the License, or (at your option) any later version.              *)
(*                                                                     *)
(* This program is distributed in the hope that it will be useful,     *)
(* but WITHOUT ANY WARRANTY; without even the implied warranty of      *)
(* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       *)
(* GNU Library General Public License for more details.                *)
(*                                                                     *)
(***********************************************************************)

(* $Id: basedefs.ml,v 1.8 2007/01/23 15:50:04 weis Exp $ *)

open Unix;;

(* Basic definitions shared in the implementation modules *)

(* p3lport: the p3l server port number *) 
let p3lport = 7749;;

(*** Debugging information ***)
 
(* this is just for driving debugging output *)
let dbg = ref false;;  (* must be two functions -debug- otherwise just one flag *)
let dbgmask = ref 1;;

(* an option to allow forcing the IP number *)

let (ip: string option ref) = ref None;; (* default, get it from the running machine *)

(* transitional: this variables is used here to choose between a fixed port or a dynamic port *)
(* for the establish_smart_server function                                                    *)

let dynport = ref false;;

let name_of_descr ?(peer=true) fd =
  try
    let get_name = if peer then Unix.getpeername else Unix.getsockname in
    match get_name fd with
    | Unix.ADDR_INET (ip, port) ->
      Unix.string_of_inet_addr ip ^ ":" ^ string_of_int port
    | Unix.ADDR_UNIX s -> s
  with e -> "fake";;
