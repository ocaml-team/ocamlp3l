(***********************************************************************)
(*                                                                     *)
(*                          OCamlP3l                                   *)
(*                                                                     *)
(* (C) 2004-2007                                                       *)
(*             Roberto Di Cosmo (dicosmo@dicosmo.org)                  *)
(*             Zheng Li (zli@lip6.fr)                                  *)
(*             Pierre Weis (Pierre.Weis@inria.fr)                      *)
(*             Francois Clement (Francois.Clement@inria.fr)            *)
(*                                                                     *)
(* Based on original Ocaml P3L System                                  *)
(* (C) 1997 by                                                         *)
(*             Roberto Di Cosmo (dicosmo@ens.fr)                       *)
(*             Marco Danelutto (marcod@di.unipi.it)                    *)
(*             Xavier Leroy  (Xavier.Leroy@inria.fr)                   *)
(*             Susanna Pelagatti (susanna@di.unipi.it)                 *)
(*                                                                     *)
(* This program is free software; you can redistribute it and/or       *)
(* modify it under the terms of the GNU Library General Public License *)
(* as published by the Free Software Foundation; either version 2      *)
(* of the License, or (at your option) any later version.              *)
(*                                                                     *)
(* This program is distributed in the hope that it will be useful,     *)
(* but WITHOUT ANY WARRANTY; without even the implied warranty of      *)
(* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       *)
(* GNU Library General Public License for more details.                *)
(*                                                                     *)
(***********************************************************************)

(* $Id: seqp3l.mli,v 1.5 2007/01/23 15:50:04 weis Exp $ *)

type ('a, 'b) io = unit;;

type ('a, 'b) skel =
    ?ecol:P3lstream.color -> ('a, 'b) io -> 'a P3lstream.s -> 'b P3lstream.s
val seq : ?col:P3lstream.color -> (('a, 'b) io -> 'a -> 'b) -> ('a, 'b) skel
val pipeline :
  ('a, 'b) skel -> ('b, 'c) skel -> ('a, 'c) skel
val ( ||| ) :
  ('a, 'b) skel -> ('b, 'c) skel -> ('a, 'c) skel
val farm :
  ?col:P3lstream.color ->
  ?colv:P3lstream.color list -> ('a, 'b) skel * int -> ('a, 'b) skel
val mapvector :
  ?col:P3lstream.color ->
  ?colv:P3lstream.color list ->
  ('a, 'b) skel * int -> ('a array, 'b array) skel
val reducevector :
  ?col:P3lstream.color ->
  ?colv:P3lstream.color list ->
  ('a * 'a, 'a) skel * int -> ('a array, 'a) skel
val loop :
  ?col:P3lstream.color ->
  ('a -> bool) * ('a, 'a) skel -> ('a, 'a) skel
val parfun : (('a, 'b) io -> ('a, 'b) skel) -> 'a P3lstream.s -> 'b P3lstream.s
val pardo : (unit -> 'a) -> 'a
