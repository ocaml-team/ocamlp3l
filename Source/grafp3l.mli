(***********************************************************************)
(*                                                                     *)
(*                          OCamlP3l                                   *)
(*                                                                     *)
(* (C) 2004-2007                                                       *)
(*             Roberto Di Cosmo (dicosmo@dicosmo.org)                  *)
(*             Zheng Li (zli@lip6.fr)                                  *)
(*             Pierre Weis (Pierre.Weis@inria.fr)                      *)
(*             Francois Clement (Francois.Clement@inria.fr)            *)
(*                                                                     *)
(* Based on original Ocaml P3L System                                  *)
(* (C) 1997 by                                                         *)
(*             Roberto Di Cosmo (dicosmo@ens.fr)                       *)
(*             Marco Danelutto (marcod@di.unipi.it)                    *)
(*             Xavier Leroy  (Xavier.Leroy@inria.fr)                   *)
(*             Susanna Pelagatti (susanna@di.unipi.it)                 *)
(*                                                                     *)
(* This program is free software; you can redistribute it and/or       *)
(* modify it under the terms of the GNU Library General Public License *)
(* as published by the Free Software Foundation; either version 2      *)
(* of the License, or (at your option) any later version.              *)
(*                                                                     *)
(* This program is distributed in the hope that it will be useful,     *)
(* but WITHOUT ANY WARRANTY; without even the implied warranty of      *)
(* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       *)
(* GNU Library General Public License for more details.                *)
(*                                                                     *)
(***********************************************************************)

(* $Id: grafp3l.mli,v 1.3 2007/01/23 15:50:04 weis Exp $ *)

type color = int
type ('a, 'b) tree =
    Seq of string * color
  | Pipe of ('a, 'b) tree list
  | Farm of ('a, 'b) tree * int * (color * color list)
  | Map of ('a, 'b) tree * int * (color * color list)
  | Reduce of ('a, 'b) tree * int * (color * color list)
  | Loop of ('a, 'b) tree * color
val seq :
  ?col:color -> (unit -> 'a -> 'b) -> ('a P3lstream.t, 'b P3lstream.t) tree
val pipeline : ('a, 'b) tree -> ('b, 'c) tree -> ('a, 'c) tree
val ( ||| ) : ('a, 'b) tree -> ('b, 'c) tree -> ('a, 'c) tree
val farm :
  ?col:color -> ?colv:color list -> ('a, 'b) tree * int -> ('a, 'b) tree
val loop : ?col:color -> 'a * ('b, 'b) tree -> ('b, 'b) tree
val mapvector :
  ?col:color ->
  ?colv:color list ->
  ('a P3lstream.t, 'b P3lstream.t) tree * int ->
  ('a array P3lstream.t, 'b array P3lstream.t) tree
val reducevector :
  ?col:color ->
  ?colv:color list ->
  (('a * 'a) P3lstream.t, 'a P3lstream.t) tree * int ->
  ('a array P3lstream.t, 'a P3lstream.t) tree
val defaultmaxdisplay : int ref
val evalsize : ?maxdisplay:int ref -> ('a, 'b) tree -> int * int
val get_color : int -> int -> int
val nvlist : int -> int -> int list -> int list
val ht : (color, int) Hashtbl.t
val add_tbl : ('a, int) Hashtbl.t -> 'a -> int -> unit
val evalcol : color -> ('a, 'b) tree -> unit
val draw_elem : string -> color -> int -> int -> int -> int -> int -> unit
val draw_seq : string -> color -> int -> int -> int -> int -> int -> unit
val draw_pipe :
  ('a, 'b) tree list -> color -> int -> int -> int -> int -> int -> unit
val draw_farm_like :
  ?nc:string ->
  ?ne:string ->
  ?maxdisplay:int ref ->
  ('a, 'b) tree ->
  color -> color list -> int -> int -> int -> int -> int -> int -> unit
val draw_loop :
  ('a, 'b) tree -> color -> int -> int -> int -> int -> int -> unit
val draw_expr :
  ('a, 'b) tree -> color -> int -> int -> int -> int -> int -> unit
val draw_main : int -> int -> unit
val parfuns : (unit -> (unit, unit) tree) list ref
val parfun : (unit -> ('a, 'b) tree) -> 'a -> 'b
val getparfuns : unit -> (unit -> (unit, unit) tree) list
val setsize : string -> unit
val getwidth : unit -> int
val getheight : unit -> int
val spec_list : (string * Arg.spec * string) list
val pardo : 'a -> unit
