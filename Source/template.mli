(***********************************************************************)
(*                                                                     *)
(*                          OCamlP3l                                   *)
(*                                                                     *)
(* (C) 2004-2007                                                       *)
(*             Roberto Di Cosmo (dicosmo@dicosmo.org)                  *)
(*             Zheng Li (zli@lip6.fr)                                  *)
(*             Pierre Weis (Pierre.Weis@inria.fr)                      *)
(*             Francois Clement (Francois.Clement@inria.fr)            *)
(*                                                                     *)
(* Based on original Ocaml P3L System                                  *)
(* (C) 1997 by                                                         *)
(*             Roberto Di Cosmo (dicosmo@ens.fr)                       *)
(*             Marco Danelutto (marcod@di.unipi.it)                    *)
(*             Xavier Leroy  (Xavier.Leroy@inria.fr)                   *)
(*             Susanna Pelagatti (susanna@di.unipi.it)                 *)
(*                                                                     *)
(* This program is free software; you can redistribute it and/or       *)
(* modify it under the terms of the GNU Library General Public License *)
(* as published by the Free Software Foundation; either version 2      *)
(* of the License, or (at your option) any later version.              *)
(*                                                                     *)
(* This program is distributed in the hope that it will be useful,     *)
(* but WITHOUT ANY WARRANTY; without even the implied warranty of      *)
(* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       *)
(* GNU Library General Public License for more details.                *)
(*                                                                     *)
(***********************************************************************)

(* $Id: template.mli,v 1.3 2007/01/23 15:50:04 weis Exp $ *)

type tag =
    Maptag of index * dimension
  | Farmtag
  | Reducetag of weight
  | Reduceseqtag
  | ReduceLoop
  | Reducepartag of int * int
  | Seqtag
  | Looptag
and index = int
and dimension = int
and weight = int
type ('a, 'b, 'c) packet =
    UserPacket of 'a * sequencenum * tag list
  | EndStream
  | EndOneStream
  | EndLoop of sequencenum
  | EndOneLoop of sequencenum
and sequencenum = int
type demand = Request of Commlib.vpinfo
val exceptq : 'a -> 'a list -> 'a list
val msg : ('a, 'b, 'c) packet -> string
val eloop : sequencenum -> ('a, 'b, 'c) packet -> ('d, 'e, 'f) packet
val instanciate_io : (unit -> 'a) -> 'b -> 'c -> 'a
val seqtempl :
  (unit -> 'a -> 'b) -> Commlib.vp_chan -> Commlib.vp_chan -> unit
val demandseqtempl :
  (unit -> 'a -> 'b) -> Commlib.vp_chan -> Commlib.vp_chan -> unit
val localredfunction : ('a -> 'a -> 'a) -> 'a array -> 'a
val curry : ('a * 'b -> 'c) -> 'a -> 'b -> 'c
val redseqtempl :
  (unit -> 'a * 'a -> 'a) -> Commlib.vp_chan -> Commlib.vp_chan -> unit
val parfunstartempl :
  (unit -> 'a) -> Commlib.vp_chan -> Commlib.vp_chan -> unit
val startempl :
  (unit -> 'a) -> (unit -> 'b) -> Commlib.vp_chan -> Commlib.vp_chan -> unit
val stoptempl :
  ('a -> unit) ->
  (unit -> 'b) -> (unit -> 'c) -> Commlib.vp_chan -> 'd -> unit
val farmetempl :
  (Commlib.vp_chan list -> Commlib.vp_chan) ->
  Commlib.vp_chan -> Commlib.vp_chan list -> unit
val closedemandocl : Commlib.vp_chan list -> string -> unit
val extdemandfarmetempl :
  'a -> Commlib.vp_chan -> Commlib.vp_chan list -> unit
val intdemandfarmetempl :
  'a -> Commlib.vp_chan -> Commlib.vp_chan list -> unit
val mkinctest : 'a list -> int * (unit -> bool)
val farmctempl : 'a -> Commlib.vp_chan list -> Commlib.vp_chan -> unit
val registered : 'a -> 'a list -> bool
val keeptrack : int -> int list -> int list
val unkeeptrack : int -> int list -> int list
val condtestertempl :
  ('a -> bool) -> Commlib.vp_chan -> Commlib.vp_chan list -> unit
val loopdisttempl :
  (Commlib.vp_chan list -> ('a, 'b, 'c) packet * Commlib.vp_chan) ->
  Commlib.vp_chan list -> Commlib.vp_chan -> unit
val mapetempl :
  (Commlib.vp_chan list -> Commlib.vp_chan) ->
  Commlib.vp_chan -> Commlib.vp_chan list -> unit
val vector_of_item_list : (int * 'a) list -> 'a array
val ins_packet :
  ('a, 'b, 'c) packet ->
  (sequencenum * (index * 'a) list) list ->
  (sequencenum * (index * 'a) list) list
val print_packetlist : (int * (int * 'a) list) list -> string
val list_complete :
  ('a, 'b, 'c) packet -> (sequencenum * 'd list) list -> 'd list
val pck_list_remove :
  ('a, 'b, 'c) packet -> (sequencenum * 'd) list -> (sequencenum * 'd) list
val mapctempl :
  (Commlib.vp_chan list -> ('a, 'b, 'c) packet * Commlib.vp_chan) ->
  Commlib.vp_chan list -> Commlib.vp_chan -> unit
val split_list : 'a list -> int -> int -> 'a list -> 'a list list
val packl_of_vect : 'a array -> ('a * 'a) list * 'a option
val reduceparetempl :
  (Commlib.vp_chan list -> Commlib.vp_chan) ->
  Commlib.vp_chan list -> Commlib.vp_chan list -> unit
val process_result :
  'a ->
  'b -> 'c -> ('b * 'a * 'c) list -> ('b * 'a * 'c) list * ('a * 'c) option
val reduceparctempl :
  (Commlib.vp_chan list -> (int, 'a, 'b) packet * Commlib.vp_chan) ->
  Commlib.vp_chan list -> Commlib.vp_chan list -> unit
