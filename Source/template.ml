(***********************************************************************)
(*                                                                     *)
(*                          OCamlP3l                                   *)
(*                                                                     *)
(* (C) 2004-2007                                                       *)
(*             Roberto Di Cosmo (dicosmo@dicosmo.org)                  *)
(*             Zheng Li (zli@lip6.fr)                                  *)
(*             Pierre Weis (Pierre.Weis@inria.fr)                      *)
(*             Francois Clement (Francois.Clement@inria.fr)            *)
(*                                                                     *)
(* Based on original Ocaml P3L System                                  *)
(* (C) 1997 by                                                         *)
(*             Roberto Di Cosmo (dicosmo@ens.fr)                       *)
(*             Marco Danelutto (marcod@di.unipi.it)                    *)
(*             Xavier Leroy  (Xavier.Leroy@inria.fr)                   *)
(*             Susanna Pelagatti (susanna@di.unipi.it)                 *)
(*                                                                     *)
(* This program is free software; you can redistribute it and/or       *)
(* modify it under the terms of the GNU Library General Public License *)
(* as published by the Free Software Foundation; either version 2      *)
(* of the License, or (at your option) any later version.              *)
(*                                                                     *)
(* This program is distributed in the hope that it will be useful,     *)
(* but WITHOUT ANY WARRANTY; without even the implied warranty of      *)
(* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       *)
(* GNU Library General Public License for more details.                *)
(*                                                                     *)
(***********************************************************************)

(* $Id: template.ml,v 1.29 2007/01/23 15:50:04 weis Exp $ *)

open Basedefs;;
open Printf;;
open Parp3l;;
open Commlib;;

type tag =
   | Maptag of index * dimension
     (* index is used to rebuild the data structure of a given dimension *)
   | Farmtag
     (* may add an index to guarantee ordering (TO DO) *)
   | Reducetag of weight
     (* weight is used to compute termination conditions *)
   | Reduceseqtag
   | ReduceLoop
   | Reducepartag of int * int
   | Seqtag
   | Looptag
and index = int
and dimension = int
and weight = int;;

type ('a, 'b, 'c) packet =
  | UserPacket of 'a * sequencenum * tag list
  | EndStream
    (* used to signal the end of the last stream of input data,
       to perform shutdown *)
  | EndOneStream
    (* used to signal the end of one stream of input data *)
  | EndLoop of sequencenum
    (* used to handle non-nested loop termination *)
  | EndOneLoop of sequencenum
    (* used to handle non-nested loop termination for parfun *)
and sequencenum = int;;

type demand = Request of Commlib.vpinfo;;

let rec exceptq a = function
  | [] -> []
  | b :: r ->
     if a == b then exceptq a r else b :: exceptq a r;;

(* auxiliary functions for handling EndLoop/EndStream *)

let msg = function | EndStream | EndLoop _ ->  "" | _ -> " one ";;
let eloop n = function | EndStream -> EndLoop n | _ -> EndOneLoop n;;

(* ??? this will need to be actually build the real stream io structure
   later on *)

let instanciate_io f ic oc =
  debug
   (fun () -> printf "%t: Instanciated sequential function.\n" timedcontext);
  f ();;

(* the seq[uential] templ[ate] function over sockets, using the
   distributed channel allocation facility *)

(* this the only template that can actually raise an End_of_file exn *)
let seqtempl f ic oc =
  let f = instanciate_io f ic oc in
  let notfinished = ref true in
  while !notfinished do
    try
      match receive ic with
      | UserPacket(p, seqn, tl) ->
          let dbg s tl =
            match tl with
            | Maptag (i, l) :: r ->
                debug (fun () ->
                  printf "%t: seqtempl %s one step \
                          (pkt %d [%d, %d] from %s(%s) to (%s)%s)"
                    timedcontext s seqn i l
                    (Commlib.addr_of_vpch ic)
                    (Commlib.addr_of_vpch ~peer:false ic)
                    (Commlib.addr_of_vpch ~peer:false oc)
                    (Commlib.addr_of_vpch oc))
            | _ ->
               debug (fun () ->
                 printf "%t: seqtempl %s one step \
                         (pkt %d from %s(%s) to (%s)%s)"
                    timedcontext s seqn
                    (Commlib.addr_of_vpch ic)
                    (Commlib.addr_of_vpch ~peer:false ic)
                    (Commlib.addr_of_vpch ~peer:false oc)
                    (Commlib.addr_of_vpch oc)) in
          dbg "doing" tl;
          cl_send oc (UserPacket (f p, seqn, tl));
          dbg "done" tl
      | EndLoop seqn | EndOneLoop seqn as marker ->
          debug (fun () ->
            printf "%t: seqtempl got end loop" timedcontext);
          cl_send oc marker
      | EndStream  ->
          debug (fun () ->
            printf "%t: seqtempl got end of stream" timedcontext);
          cl_send oc EndStream;
          debug (fun () ->
            printf "%t: seqtempl sent end of stream" timedcontext);
          vp_close oc; vp_close ic;
          debug (fun () ->
            printf "%t: seqtempl closed ic and oc" timedcontext);
          notfinished := false
      | EndOneStream  ->
          debug (fun () ->
            printf "%t: seqtempl got end of one stream" timedcontext);
          cl_send oc EndOneStream;
          debug (fun () ->
            printf "%t: seqtempl sent end of one stream" timedcontext);
    with
    | End_of_file ->
        debug (fun () ->
          printf "%t: seqtempl got end of file!" timedcontext);
        cl_send oc EndStream;
        notfinished := false;
        vp_close oc; vp_close ic
    | exn ->
        debug (fun () ->
          printf "%t: seqtempl got exception %s"
            timedcontext (Printexc.to_string exn));
        raise exn
  done;
  debug (fun () ->
    printf "%t: seqtempl exiting" timedcontext)
;;

(* this is the only template that can actually raise an End_of_file exn *)
let demandseqtempl f ic oc =
  let f = instanciate_io f ic oc in
  let notfinished = ref true in
  while !notfinished do
    try
      cl_send ic (Request (Commlib.myvpinfo()));
      match receive ic with
      | UserPacket(p, seqn, tl) ->
          debug (fun () ->
            printf "%t: demandseqtempl doing one step (pkt %d)"
              timedcontext seqn);
          cl_send oc (UserPacket (f p, seqn, tl));
          debug (fun () ->
            printf "%t: demandseqtempl done one step (pkt %d)"
              timedcontext seqn)
      | EndLoop seqn | EndOneLoop seqn as marker ->
          debug (fun () ->
            printf "%t: demandseqtempl got end loop" timedcontext);
          cl_send oc marker
      | EndStream  ->
          debug (fun () ->
            printf "%t: demandseqtempl got end of stream" timedcontext);
          cl_send oc EndStream;
          vp_close oc; vp_close ic;
        debug (fun () ->
          printf "%t: demandseqtempl closed ic and oc" timedcontext);
          notfinished := false
      | EndOneStream ->
          debug (fun () ->
            printf "%t: demandseqtempl got end of one stream" timedcontext);
          cl_send oc EndOneStream;
    with
    | End_of_file ->
        debug (fun () ->
          printf "%t: demandseqtempl got end of file!" timedcontext);
        cl_send oc EndStream;
        notfinished := false;
        vp_close oc; vp_close ic
  done
;;

(* the red[uce] seq[uential] templ[ate] function over sockets, using the
   distributed channel allocation facility *)

(* utility: folds the reduce function when sequential functions are used
   therefore gets a function 'a->'a->'a and a vector 'a array
   and computes the fold of f over the vector ( added by marcod to patch
   reducevector of sequential functions ) *)

let localredfunction f x =
  let y = Array.to_list x in
  match y with
  | [a] -> a
  | a :: ra -> List.fold_left f a ra
  | _ -> failwith "localredfunction: cannot handle empty array!"
;;
     
let curry f = fun x -> fun y -> f (x, y);;

let redseqtempl f ic oc =
  let f = instanciate_io f ic oc in
  let f = localredfunction (curry f) in
  let notfinished = ref true in
  while !notfinished do
    try
      match receive ic with
      | UserPacket(p, seqn, tl) ->
          cl_send oc (UserPacket((f p), seqn, tl));
          debug (fun () ->
            printf "%t: redseqtempl done one step (pkt %d)" timedcontext seqn)
      | EndLoop seqn | EndOneLoop seqn as marker ->
          debug (fun () ->
            printf "%t: redseqtempl got end loop" timedcontext);
          cl_send oc marker
      | EndStream  ->
          debug (fun () ->
            printf "%t: redseqtempl got end of stream" timedcontext);
          cl_send oc EndStream;
          vp_close oc; vp_close ic;
          notfinished := false
      | EndOneStream ->
          debug (fun () ->
            printf "%t: redseqtempl got end of one stream" timedcontext);
          cl_send oc EndOneStream;
    with
    | End_of_file ->
        debug (fun () ->
          printf "%t: redseqtempl got end of file!" timedcontext);
        notfinished := false;
        cl_send oc EndStream; vp_close oc; vp_close ic
  done;;

let parfunstartempl initf ic oc =
  let pktnum = ref 0 in
  initf (); (* let us do the initialization step first, which is impure *)
  try
    while true do
      let pl = receive ic in
      List.iter
        (fun p ->
           cl_send oc (UserPacket (p, !pktnum, [Seqtag]));
           debug (fun () ->
             printf "%t: startempl delivered (pkt %d)" timedcontext (!pktnum));
           incr pktnum)
        pl
    done
  with
  | End_of_file ->
      debug (fun () ->
        printf "%t: startempl got end of file!" timedcontext);
      cl_send oc EndStream; vp_close oc; vp_close ic;;

let startempl f initf =
  let pktnum = ref 0 in
  fun ic oc -> (* the starter does not read the input channel *)
    initf (); (* let us do the initialization step first, which is impure *)
    let notfinished = ref true in
    while !notfinished do
      try
        cl_send oc (UserPacket (f (), !pktnum, [Seqtag]));
        debug (fun () ->
          printf "%t: startempl delivered (pkt %d)" timedcontext !pktnum);
          incr pktnum
      with
      | End_of_file ->
          debug (fun () ->
            printf "%t: startempl got end of file!" timedcontext);
          cl_send oc EndStream; vp_close oc; vp_close ic;
          notfinished := false
  done;;

let stoptempl f initf finalize ic oc =
(* the stopper does not write to the output channel *)
  debug (fun () -> printf "%t Gatherer starting" timedcontext);
  initf (); (* let us do the initialization step first, which is impure *)
  let notfinished = ref true in
  while !notfinished do
    try
      match receive ic with
      | UserPacket(p, seqn, _) ->
          debug (fun () ->
            printf "%t: stoptempl finalizing (pkt %d)" timedcontext seqn);
          f p
      | EndStream ->
          debug (fun () ->
            printf "%t: stoptempl got end of stream" timedcontext);
          finalize (); (* the finalization step *)
          (* DANGEROUS BECAUSE IT TRIES TO CLOSE STDOUT: vp_close oc; *)
          vp_close ic;
          notfinished := false
(*      | EndOneStream  ->
            debug (fun () ->
              printf "%t: stoptempl got end of one stream"
                timedcontext);
            cl_send oc EndOneStream; *)
      | _ -> failwith "stoptempl: got unexpected kind of packet!"
    with
    | End_of_file ->
        debug (fun () ->
          printf "%t: stoptempl got end of file!" timedcontext);
        finalize (); (* the finalization step *)
        notfinished := false;
        (* DANGEROUS BECAUSE IT TRIES TO CLOSE STDOUT: vp_close oc; *)
        vp_close ic
  done;;

(* the round robin template for farm emitters *)
(* we should also implement a template using a demand driven approach,
   more sophisticated! *)

let farmetempl f ic ocl =
  let notfinished = ref true in
  while !notfinished do
    try
      let theoc = f ocl in
      match receive ic with
      | UserPacket(p, seqn, tl) ->
          cl_send theoc (UserPacket (p, seqn, Farmtag :: tl));
          debug (fun () ->
            printf "%t: farmetempl delivered (pkt %d)" timedcontext seqn);
      | EndLoop seqn | EndOneLoop seqn as marker ->
          debug (fun () ->
            printf "%t: farmetempl got end loop" timedcontext);
          List.iter (fun x -> cl_send x marker) ocl
      | EndStream  ->
          debug (fun () ->
            printf "%t: farmetempl got end of stream" timedcontext);         
          List.iter (fun x -> cl_send x EndStream) ocl;
          List.iter vp_close ocl; vp_close ic;
          notfinished := false
      | EndOneStream  ->
          debug (fun () ->
            printf "%t: farmetempl got end of one stream" timedcontext);
          List.iter (fun x -> cl_send x EndOneStream) ocl;
    with
    | End_of_file ->
        debug (fun () ->
          printf "%t: farmetempl got end of file!" timedcontext);
        List.iter vp_close ocl;
        notfinished := false;
        vp_close ic
  done;;

(* a function to close a list of on-demand output channels *)

let closedemandocl demandocl templatename =
  let openchans=ref (List.length demandocl)
  and pocl=ref demandocl in
  debug (fun () ->
    printf "%t: %s entered closedemandocl with %d channels)"
      timedcontext templatename !openchans);
  while !openchans <> 0 do
    let (Request n, theoc) = receive_any !pocl in
    debug (fun () ->
      printf
        "%t: %s with %d channels got Request on channel %d, closing channel %d"
        timedcontext templatename !openchans n n);
    cl_send theoc EndStream;
    vp_close theoc;
    pocl := exceptq theoc !pocl;
    openchans := !openchans - 1;
    debug (fun () ->
      printf "%t: %s delivered EndStream (%d left)"
        timedcontext templatename !openchans);
  done;;


(* the external demand driven template for farm emitters *)

let extdemandfarmetempl f ic ocl =
  let notfinished = ref true in
  if List.length ocl = 0 then
    (eprintf
       "%t: extdemandfarmetempl oc list size less than 1!"
       timedcontext;
     failwith "Internal error");
  while !notfinished do
    try
      match receive ic with
      | UserPacket (p, seqn, tl) ->
          let (Request n, theoc) = receive_any ocl in
          (* the output channels are on demand, so we read on them
              a demand packet before sending data*)
          cl_send theoc (UserPacket (p, seqn, Farmtag :: tl));
          debug (fun () ->
            printf
              "%t: extdemandfarmetempl delivered (pkt %d) on demand to [%d]"
              timedcontext (seqn) n)
      | EndLoop seqn | EndOneLoop seqn as marker ->
          debug (fun () ->
            printf "%t:  extdemandfarmetempl got end loop"
            timedcontext);
          List.iter (fun x -> cl_send x marker) ocl
      | EndStream ->
          debug (fun () ->
            printf "%t:  extdemandfarmetempl got end of stream" timedcontext);
          vp_close ic;
          (* Error: we were closing channels *without* waiting
             for a request from the inner skeleton! *)
          (* List.iter (fun x -> cl_send x EndStream) ocl; *)
          (* Correct solution:
             1) wait for a request from a connected node (worker),
             2) send an EndStream
             3) close the channel
             until no more workers are there *)
          closedemandocl ocl "extdemandfarmetempl";
          notfinished := false
      | EndOneStream  ->
          debug (fun () ->
            printf
              "%t: extdemandfarmetempl got end of one stream" timedcontext);
          List.iter (fun x -> cl_send x EndOneStream) ocl;
    with
    | End_of_file ->
        debug (fun () ->
          printf "%t: extdemandfarmetempl got end of file!" timedcontext);
        notfinished := false;
        List.iter vp_close ocl;
        vp_close ic
  done;;

(* the internal demand driven template for farm emitters *)
(* warning: the function f should be the one used in selecting
   *input* channels, but we pass a function to select
   *output* channels instead *)

(* N.B.: ic and ocl are on demand, so we nead to do reads and writes on both *)

let intdemandfarmetempl f ic ocl =
  if List.length ocl = 0 then
   (eprintf
      "%t: intdemandfarmetempl oc list size less than 1!\n"
      timedcontext;
    failwith "Internal error");
  let notfinished = ref true in
  while !notfinished do
    try
      cl_send ic (Request (Commlib.myvpinfo ()));
      match receive ic with
      | UserPacket (p, seqn, tl) ->
          let (vpinfo, _), oc = receive_any ocl in
          cl_send oc (UserPacket (p, seqn, Farmtag :: tl));
          debug (fun () ->
            printf "%t: farmetempl delivered (pkt %d)" timedcontext seqn);
      | EndLoop seqn | EndOneLoop seqn as marker ->
        debug (fun () ->
          printf "%t: farmetempl got end loop" timedcontext);
          List.iter (fun x -> cl_send x marker) ocl
      | EndStream  ->
          debug (fun () ->
            printf "%t: farmetempl got end of stream" timedcontext);
          vp_close ic;
          closedemandocl ocl "intdemandfarmetempl";
          (* Error =>
              List.iter (fun x -> cl_send x EndStream) ocl;
              List.iter vp_close ocl; vp_close ic; *)
          notfinished := false
      | EndOneStream  ->
          debug (fun () ->
            printf "%t: intdemandfarmtempl got end of one stream" timedcontext);
          List.iter (fun x -> cl_send x EndOneStream) ocl;
    with
    | End_of_file ->
        debug (fun () ->
          printf "%t: intdemandfarmetempl got end of file!" timedcontext);
        List.iter vp_close ocl;
        notfinished := false;
        vp_close ic
  done;;


(* common structure used to count up to the number of a list of channels *)

let mkinctest icl =
  let nicl = List.length icl in
  let inctest =
    let c = ref 0 in
    fun () -> incr c; if !c >= nicl then (c := 0; true) else false in
  nicl, inctest;;

let farmctempl f icl oc =
  let nicl, inctest = mkinctest icl in
  let okicl = ref icl  in
  let notfinished = ref true in
  let loopcount = ref 0 in
  while !notfinished do
    try
      let inpkt, ic = receive_any !okicl in
      match inpkt with
      | UserPacket (p, seqn, Farmtag :: r) ->
          debug (fun () ->
            printf "%t: farmctempl received (pkt %d)" timedcontext (seqn));
          cl_send oc (UserPacket(p, seqn, r));
          debug (fun () ->
            printf "%t: farmctempl delivered (pkt %d)" timedcontext (seqn))
      | EndLoop seqn | EndOneLoop seqn as marker ->
          incr loopcount;
          debug (fun () ->
            printf "%t: farmctempl got end %s loop %d out of %d"
             timedcontext (msg marker) !loopcount nicl);
          if !loopcount = nicl then
            (debug (fun () ->
               printf "%t: farmctempl sends end %s loop"
                 timedcontext (msg marker));
               cl_send oc marker;
               match marker with
               | EndOneLoop _ -> loopcount := 0
               | _ -> ()(* restore conditions for next loop *)
            );
      | EndStream  ->
          let chinfo = vp_info ic in
          okicl := exceptq ic !okicl;
          vp_close ic;
          if !okicl=[] then begin
            debug (fun () ->
              printf "%t: farmctempl got last end of stream, shutting down."
                timedcontext);
            cl_send oc EndStream;
            vp_close oc;
            notfinished := false
          end else
            debug (fun () ->
              printf "%t: farmctempl got end of stream on chan %d! "
                timedcontext chinfo;
              printf "Removing it from active list (%d left)."
                (List.length !okicl))
      | EndOneStream  ->
          debug (fun () ->
            printf "%t: farmctempl got end of one stream" timedcontext);
          if inctest ()
          then
            (debug (fun () ->
              printf "%t: farmctempl sends end of one stream" timedcontext);
             cl_send oc EndOneStream)
          else
            debug (fun () ->
              printf "%t: farmctempl does not send end of one stream \
                      (less than %d EndOfOneStream received)" timedcontext nicl)
      | _ ->
         printf "INTERNAL ERROR: farmctempl got a non-farm packet!!!";
         print_newline ();
    with End_of_file ->
      debug (fun () ->
          printf "%t: Farm collector got end of file!" timedcontext);
      vp_close oc;
      notfinished := false;
      List.iter vp_close !okicl
  done;;

(* is a packet number registered ? *)
let rec registered pckn l =
  match l with
  | [] -> false
  | n :: rl ->
      n = pckn || registered pckn rl;;

(* register a packet number *)
let keeptrack pckn ll =
  if registered pckn ll then ll else
  begin
    debug (fun () ->
      printf "%t: registering packet %d" timedcontext pckn);
    pckn :: ll
  end;;

(* unregister a packet number *)
let rec unkeeptrack pckn ll =
  match ll with
  | [] -> []
  | n :: rl ->
      if n = pckn then
      begin
        debug (fun () ->
          Printf.printf "%t: unregistering packet %d" timedcontext pckn);
        rl
      end else
      n :: unkeeptrack pckn rl;;

(* this is the implementation of the loop related process templates ...   *)
(* TODO: should properly close the input and output channels when needed! *)

let condtestertempl f ic = function
  | oc1 :: oc2 :: ocl ->
     let initial = ref true in
     let highest_seen = ref (-1) in
     (* should be more abstract than a simple integer! *)
     let loopmax = ref (-1) in
     (* this is to keep track of the n in the EndLoop n we eventually receive *)
     let in_loop_pck_list = ref [] in
     (* list of seqn for pckts within the loop *)
     let loopend = ref false in
     let notfinished = ref true in
     let currentmarker = ref EndOneStream in
     while !notfinished do
       try
         match receive ic with
         | EndLoop n | EndOneLoop n as marker ->
           (* my loop closes, after seeing packets up to n included *)
             currentmarker :=
               (match marker with EndLoop _ -> EndStream | _ -> EndOneStream);
             loopend := true;
             loopmax := n;
             if n <= !highest_seen then begin
               if (!in_loop_pck_list = []) then
                 (debug (fun () ->
                    printf
                      "%t: condtestertempl received End %s Loop %d, \
                       sending EoS as no packets are left \
                       (%s packets seen insofar)"
                      timedcontext (msg marker) n
                      (if !initial then "No" else "Some"));
                    cl_send oc1 !currentmarker
                    (* terminate the loop if no packets are waiting *))
               end else
                 debug (fun () ->
                   printf
                     "%t: condtestertempl received End %s Loop %d, \
                     not sending EoS as some packets are left \
                     or still to be seen" timedcontext (msg marker) n)
         | UserPacket (p, seqn, Looptag :: r) ->
             initial := false;
             highest_seen :=  max seqn !highest_seen;
             if f p then begin
               (* should pass back the packet onto the feedback channel *)
               (* check-in the packet *)
               if not (registered seqn !in_loop_pck_list)
               then in_loop_pck_list := keeptrack seqn !in_loop_pck_list;
               (* then deliver it to the feedback channel *)
               cl_send oc1 (UserPacket (p, seqn, r));
               debug (fun () ->
                 printf
                   "%t: condtester delivered a feedback pkt (pkt %d)"
                   timedcontext seqn)
             end else begin
               (* should be output to the output channel *)
               (* unregister the packet no *)
               in_loop_pck_list := (unkeeptrack seqn !in_loop_pck_list);
               (* if the packet list is empty, and the loop is ending,
                  deliver the packet and send EoS *)
               (* in any case: deliver the packet *)
               cl_send oc2 (UserPacket (p, seqn, r));
               if !in_loop_pck_list = [] && 
                  !loopend && !highest_seen >= !loopmax
               then begin
                 (* deliver the packet and send EndStream to loopdistr *)
                 debug (fun () ->
                   printf "%t: condtester sending out last pkt (pkt %d)"
                     timedcontext seqn);
                 (* terminate the distloop *)
                 cl_send oc1 !currentmarker;
                 debug (fun () ->
                   printf "%t: condtester terminating the loopdist"
                     timedcontext)
               end
             end
         | EndStream | EndOneStream as marker ->
             debug (fun () ->
               printf "%t: Condition tester got end of %s stream"
                 timedcontext (msg marker));
             cl_send oc2 marker;
             if marker = EndStream then begin
               vp_close oc1; vp_close oc2; vp_close ic;
               notfinished := false
             end
         | _ ->
(* FIXME: we do not exit ??? *)
             printf "INTERNAL ERROR: condtester got a non-loop packet!!!";
             print_newline ();
       with
       | End_of_file ->
           debug (fun () ->
             Printf.printf "%t: Condition tester got end of file!"
               timedcontext);
           notfinished := false;
           List.iter vp_close ocl;
           vp_close ic
     done
  | _ ->
     printf "%t: condtestertempl got less than 2 output channels!"
       timedcontext;
     failwith "Internal Error";;

(* TODO: close channels IN/OUT when neded! *)

let loopdisttempl f icl oc =
  let highest_seen = ref (-1) in
  let first_eos = ref true in
  let okicl = ref icl  in
  let notfinished = ref true in
  while !notfinished do
    try
      let inpkt, ic = f !okicl in
      match inpkt with
      | UserPacket(p, seqn, r) ->
          highest_seen := max seqn !highest_seen;
          cl_send oc (UserPacket(p, seqn, Looptag :: r));
          debug (fun () ->
            printf "%t: loopdisttempl delivered (pkt %d)" timedcontext (seqn))
      | EndLoop _ | EndOneLoop _ -> (* this should be handled, actually *)
          debug (fun () ->
            printf "%t: loopdisttempl got end loop" timedcontext);
          printf "%t: INTERNAL ERROR: probably nested loops" timedcontext;
          print_newline ();
          notfinished := false
      | EndStream | EndOneStream as marker ->
          (* as in the reduce template, when we get an EoS,
             we need first to make sure that the collector node
             is not going to send any more packets back *)
          if !first_eos then begin
            first_eos := false;
            debug (fun () ->
              printf "%t: loopdisttempl got end of %s stream (first); \
                      we send End %s Loop %d"
                timedcontext (msg marker) (msg marker) !highest_seen);
            okicl := exceptq ic !okicl;
            (* remove precisely the channel on which we got the eos *)
            cl_send oc (eloop !highest_seen marker)
          end else begin
            debug (fun () ->
              printf "%t: loopdisttempl got end of %s stream(second); \
                      we send EoS"
                timedcontext (msg marker));
            cl_send oc marker;
            if marker = EndStream then notfinished := false else begin
              (* restore initial state for the next stream *)
              highest_seen := -1;
              first_eos := true;
              okicl := icl
            end
          end
    with
    | End_of_file ->
        debug (fun () ->
          printf "%t: loopdisttempl got end of file!" timedcontext);
        vp_close oc;
        notfinished := false;
        List.iter vp_close !okicl
  done;;

(* the map emitter template, derived from the emitter one *)
let mapetempl f ic ocl =
  let notfinished = ref true in
  while !notfinished do
    try
      (* first read a packet from the unique input channel *)
      match receive ic with
      | UserPacket(p, seqn, tl) ->
          debug (fun () ->
            printf "%t: mapetempl routing (pkt %d)" timedcontext seqn);
          (* ok this should be a good vector packet *)
          (* now distribute vector items along with their index to workers:
             use a round robin strategy, do not pack (grain = 1) *)
          let al = Array.length p in
          for ii = 0 to al - 1 do
            (* pick up next worker *)
            let theoc = f ocl in
            (* send subtask *)
            debug (fun () ->
              printf "%t: mapetempl routing (pkt %d [%d, %d]) to %s "
                timedcontext seqn ii al (Commlib.addr_of_vpch theoc));
            cl_send theoc
                  (UserPacket (p.(ii), seqn, Maptag (ii, al) :: tl))
          done (* ok one item per worker, round robin *)
      (* on Eos do usual things *)
      | EndLoop seqn | EndOneLoop seqn as marker ->
          debug (fun () ->
            printf "%t: mapetempl got end loop" timedcontext);
          List.iter (fun x -> cl_send x marker) ocl
      | EndStream  ->
          debug (fun () ->
            printf "%t: mapetempl got end of stream" timedcontext);
          List.iter (fun x -> cl_send x EndStream) ocl;
          List.iter vp_close ocl; vp_close ic;
          notfinished := false
      | EndOneStream  ->
          debug (fun () ->
            printf "%t: mapetempl got end of one stream" timedcontext);
          List.iter
            (fun x ->
              cl_send x EndOneStream;
              debug (fun () ->
                printf "%t: mapetempl send end of one stream to %s"
                  timedcontext (Commlib.addr_of_vpch x)))
            ocl;
    with
    | End_of_file ->
        notfinished := false;
        debug (fun () ->
          printf "%t: mapetempl got end of file!" timedcontext);
        List.iter vp_close ocl; vp_close ic
  done;;
 
(* service functions for the map collector process:
   it must keep track of out-of-order packets before issuing out packets *)
 
(* service function: build a vector out of a list of (index, item) items *)
let vector_of_item_list vl =
  let v = Array.create (List.length vl) (snd (List.hd vl)) in
  let rec v_of_il vl v =
    match vl with
    | [] -> ()
    | (i, vi) :: rv -> Array.set v i vi; v_of_il rv v in
  v_of_il vl v;
  v;;

(* keep any-order packets *)
let rec ins_packet splitpacket packetlist =
  match splitpacket with
  | UserPacket (p, seqn, Maptag (i, n) :: _) ->
     (match packetlist with
      | [] -> [(seqn, [(i, p)])]
      | ai :: ri ->
          let (seqi, il) = ai in
          if seqi = seqn
          then (seqi, (i, p) :: il) :: ri
          else ai :: (ins_packet splitpacket ri))
  | _ ->
     printf "%t: inspacket got unexpected kind of packet!" timedcontext;
     failwith "Internal Error";;
     
(* just for debug: print the packet list *)
let rec print_packetlist pl =
  let rec print_iv_list = function
    | [] -> ""
    | (i, _) :: rl ->
        "<" ^ string_of_int i ^ ">" ^ print_iv_list rl in
  match pl with
  | [] -> "\n"
  | (n, ivl) :: rl ->
      "\nItem " ^ string_of_int n ^ ": " ^
      print_iv_list ivl ^ print_packetlist rl;;
     
(* look for a complete packet list *)
let list_complete splitpacket packetlist =
  match splitpacket with
  | UserPacket (p, seqn, Maptag (i, n) :: _) ->
      let li = List.assoc seqn packetlist in
      if List.length li = n then li else []
  | _ ->
     printf "%t: list_complete got unexpected kind of packet!"
       timedcontext;
     failwith "Internal Error";;
     
(* remove an issued packet list *)
let rec pck_list_remove splitpacket packetlist =
  match splitpacket with
  | UserPacket (p, seqn, Maptag (i, n) :: _) ->
      (match packetlist with
       | [] -> []
       | i :: ri ->
           let (seqi, il) = i in
           if seqi = seqn then ri else
           i :: pck_list_remove splitpacket ri)
  | _ ->
     printf "%t: pck_list_remove got unexpected kind of packet!"
       timedcontext;
     failwith "Internal Error";;

(* the map collector template (derived from the farm one) *)
(* this assumes no out-of order packets, actually. Should take care of
   packets relative to a different task ... Hint: use seqn ... *)
let mapctempl f icl oc =
  let okicl = ref icl  in
  let nicl, inctest = mkinctest !okicl in
  let outvectl = ref [] in (* this is used to build out the result vector *)
  let notfinished = ref true in
  let loopcount = ref 0 in
  while !notfinished do
    try
      let inpck, ic = f !okicl in
      match inpck with
      | UserPacket (p, seqn, Maptag (i, n) :: tl) ->
          debug (fun () ->
            printf "%t: mapctempl received (pkt %d [%d, %d])"
            timedcontext seqn i n);
          outvectl := ins_packet inpck !outvectl;
          let lpck = list_complete inpck !outvectl in
          debug (fun () ->
            printf "%t: mapctempl packet status: %s"
              timedcontext (print_packetlist !outvectl));
          if lpck <> [] then begin
            cl_send oc
              (UserPacket(vector_of_item_list lpck, seqn, tl));
            debug (fun () ->
              printf "%t: mapctempl delivering (pkt %d)" timedcontext seqn);
            (* reset result *)
            outvectl := (pck_list_remove inpck !outvectl);
          end
          (* else in this case do nothing *)
      | UserPacket (_, _, _) ->
          printf "%t: mapctempl got unexpected kind of UserPacket!"
            timedcontext;
          failwith "Internal Error"
      | EndLoop seqn | EndOneLoop seqn as marker ->
          incr loopcount;
          debug (fun () ->
            printf "%t: mapctempl got end %s loop %d out of %d"
              timedcontext (msg marker) !loopcount nicl);
          if !loopcount = nicl then begin
            debug (fun () ->
              printf "%t: mapctempl sends end %s loop"
                timedcontext (msg marker));
            cl_send oc marker;
            match marker with
            | EndOneLoop _ -> loopcount := 0
            | _ -> () (* restore conditions for next loop *)
          end;
      | EndStream  ->
          okicl := exceptq ic !okicl;
          vp_close ic;
          if !okicl = [] then begin
            debug (fun () ->
              printf "%t: mapctempl got last end of stream" timedcontext);
            cl_send oc EndStream;
            vp_close oc;
            notfinished := false
          end else
            debug (fun () ->
              printf "%t: mapctempl got end of stream" timedcontext)
      | EndOneStream  ->
          debug (fun () ->
            printf "%t: mapctempl got end of one stream" timedcontext);
          if inctest () then begin
            debug (fun () ->
              printf "%t: mapctempl sends end of one stream" timedcontext);
            cl_send oc EndOneStream
          end else
            debug (fun () ->
              printf "%t: mapctempl does not send end \
                      of one stream (less than %d EndOfOneStream received)"
                timedcontext nicl)
    with
    | End_of_file ->
        debug (fun () ->
          printf "%t: mapctempl got end of file!" timedcontext);
        vp_close oc;
        notfinished := false;
        List.iter vp_close !okicl
  done;;

(* well, let's have a look at a possible reduce template.  the emitter
   prepares exactly nw subpackets and delivers them one to each worker
   then the workers compute locally the reduction at the end they
   deliver the local result to the collector the collector gather the
   nw results and reduces the final result (ummmh: this means that the
   collector should know the function!)  *)

(* bin, this allows a vector to be splitted in as much packets as the
   workers are *)

let rec split_list l n i  rl =
  if l = [] then [rl] else
  if List.length rl = n then rl :: split_list l n 0 [] else
  split_list (List.tl l) n (i + 1) (rl @ [List.hd l]);;

(* now the reduce emitter, splits the vector in packets and delivers
   them to the workers, but the first item of an odd vector is sent
   directly to the collector
   Notice that the input channel list inchl contains 2 (TWO) channels,
   the first is from the outer world, the second is for looping
   packets *)

let packl_of_vect v =
  let rec aux (acc, odd) = function
    | [] -> (acc, None)
    | (a :: b :: r) -> aux ((a, b) :: acc, odd) r
    | [a] -> acc, Some a in
  aux ([], None) (Array.to_list v);;

let reduceparetempl f icl outs =
 match icl, outs with
 | [icloop;icow], collch :: ocl ->
    let activeicl = ref icl  in
    let notfinished = ref true in
    while !notfinished do
      try
        let inpkt, ic = receive_any !activeicl in
        match inpkt with 
        | UserPacket (p, seqn, ReduceLoop :: tl) ->
          (* we got a looping packet *)
            let theoc = f ocl in
            cl_send theoc (UserPacket (p, seqn, tl));
            debug (fun () ->
              printf "%t: reduceparetempl delivered (pkt %d)"
                timedcontext (seqn))
        | UserPacket (p, seqn, tl) ->
            (* we got a new vector to process *)
            (* ok, split the vector in a couple list plus eventually and
               odd element *)
            let (rpacketlist, odd) = packl_of_vect p
            and nitems = Array.length p in
            begin
              match odd with
              | Some a ->
                  cl_send collch
                    (UserPacket(a, seqn, Reducepartag(nitems, 1) :: tl));
                  debug (fun () ->
                    printf "%t: reduceparetempl delivered (pkt %d)"
                      timedcontext (seqn))
              | None -> ()
            end;
            List.iter
              (function pacch ->
                 let theoc = f ocl in
                 cl_send theoc
                   (UserPacket (pacch, seqn, Reducepartag (nitems, 2) :: tl));
                 debug (fun () ->
                   printf "%t: reduceparetempl delivered packet (pkt %d)"
                   timedcontext (seqn)))
              rpacketlist
        | EndLoop seqn | EndOneLoop seqn as marker ->
            debug (fun () ->
              printf "%t: reduceparetempl got end loop" timedcontext);
            List.iter (fun x -> cl_send x marker) ocl
        | EndStream  ->
            activeicl := exceptq ic !activeicl;
            vp_close ic;
            if !activeicl = [] then begin
              debug (fun () ->
                printf "%t: reduceparetempl got end of stream" timedcontext);
              List.iter (fun x -> cl_send x EndStream) ocl;
              List.iter vp_close ocl;
              notfinished := false
            end else
              debug (fun () ->
                printf "%t: reduceparetempl got end of stream" timedcontext)
        | EndOneStream  ->
            debug (fun () ->
              printf "%t: reduceparetempl got end of one stream" timedcontext);
            List.iter (fun x -> cl_send x EndOneStream) ocl;
      with
      | End_of_file ->
          debug (fun () ->
            printf "%t: reduceparetempl got end of file!" timedcontext);
          notfinished := false;
          List.iter vp_close ocl;
          List.iter vp_close !activeicl
    done
| _ ->
   printf "%t: reduceparetempl got insufficient number \
           of input/output channels!" timedcontext;
   failwith "Internal Error";;

(* now let's do the collection of a reduce : we need to gather exactly
   nw packets from workers then we have to locally reduce them by
   means of function f (the one of the workers !)  *)

(* process a result: if missing,  buffer it, else give back the
   previous buffered value *)

let rec process_result pre seqn level reslist =
  match reslist with
  | [] -> [(seqn, pre, level)], None
  | (seqn1, res1, level1 as x) :: rx ->
      if seqn1 = seqn then rx, Some(res1, level1) else
      let (r, v) = process_result pre seqn level rx in
      x :: r, v;;

let reduceparctempl f icl ocl =
   match ocl with
   | [ocloop; ocow] ->
      let nicl, inctest = mkinctest icl in
      let okicl = ref icl
      and buffers = ref [] in
      let notfinished = ref true in
      while !notfinished do
        try
          let inpck, ic = f !okicl in
          match inpck with
          | UserPacket (pre, seqn, Reducepartag (nitems, level) :: tl) ->
              debug (fun () ->
                printf "%t: reduceparctempl receiving (pkt %d)"
                  timedcontext seqn);
              if level = nitems then (* computation is finished *)
                cl_send ocow (UserPacket (pre, seqn, tl))
              else (* must buffer or loop *)
                let (nb, v) = process_result pre seqn level !buffers in
                begin
                  buffers := nb;
                  match v with
                  | None -> ()
                  | Some (valueb, levelb) ->
                      printf "%t: reduceparctempl found some %d %d (%d)"
                        timedcontext
                        valueb levelb (level + levelb);
                      print_newline ();
                      cl_send ocloop
                        (UserPacket ((pre, valueb), seqn,
                           ReduceLoop ::
                           Reducepartag (nitems, level + levelb) ::
                           tl));
                      debug (fun () ->
                        printf "%t: reduceparctempl delivering (pkt %d)"
                          timedcontext (seqn))
                end
          | EndLoop seqn | EndOneLoop seqn as marker ->
              (* attention !!! *)
              debug (fun () ->
                printf "%t: reduceparctempl got end loop" timedcontext);
              cl_send ocow marker
          | EndStream ->
              okicl := exceptq ic !okicl;
              vp_close ic;
              if !okicl = [] then begin
                debug (fun () ->
                  printf "%t: reduceparctempl got end of stream"
                    timedcontext);
                cl_send ocow EndStream;
                vp_close ocow;
                notfinished := false
              end else
                debug (fun () ->
                  printf "%t: reduceparctempl got end of stream"
                    timedcontext)
          | EndOneStream  ->
              debug (fun () ->
                printf "%t: reduceparctempl got end of one stream"
                  timedcontext);
              if inctest () then begin
                debug (fun () ->
                   printf "%t: reduceparctempl sends end of one stream"
                     timedcontext);
                 cl_send ocow EndOneStream
              end else
                debug (fun () ->
                  printf "%t: reduceparctempl does not send \
                         end of one stream (less than \
                         %d EndOfOneStream received)"
                    timedcontext nicl)
          | _ ->
              (*** FIXME *)
              printf "INTERNAL ERROR: reduceparcseq  got \
                      a non-reduceseq packet!!!";
              print_newline ()
        with
        | End_of_file ->
            debug (fun () ->
              printf "%t: reduceparctempl got end of file!" timedcontext);
            notfinished := false;
            List.iter vp_close ocl;
            List.iter vp_close !okicl
      done
  | _ ->
      printf "%t: reduceparctempl got less than one outptut channel!"
      timedcontext; failwith "Internal Error";;

