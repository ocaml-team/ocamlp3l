(***********************************************************************)
(*                                                                     *)
(*                          OCamlP3l                                   *)
(*                                                                     *)
(* (C) 2004-2007                                                       *)
(*             Roberto Di Cosmo (dicosmo@dicosmo.org)                  *)
(*             Zheng Li (zli@lip6.fr)                                  *)
(*             Pierre Weis (Pierre.Weis@inria.fr)                      *)
(*             Francois Clement (Francois.Clement@inria.fr)            *)
(*                                                                     *)
(* Based on original Ocaml P3L System                                  *)
(* (C) 1997 by                                                         *)
(*             Roberto Di Cosmo (dicosmo@ens.fr)                       *)
(*             Marco Danelutto (marcod@di.unipi.it)                    *)
(*             Xavier Leroy  (Xavier.Leroy@inria.fr)                   *)
(*             Susanna Pelagatti (susanna@di.unipi.it)                 *)
(*                                                                     *)
(* This program is free software; you can redistribute it and/or       *)
(* modify it under the terms of the GNU Library General Public License *)
(* as published by the Free Software Foundation; either version 2      *)
(* of the License, or (at your option) any later version.              *)
(*                                                                     *)
(* This program is distributed in the hope that it will be useful,     *)
(* but WITHOUT ANY WARRANTY; without even the implied warranty of      *)
(* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       *)
(* GNU Library General Public License for more details.                *)
(*                                                                     *)
(***********************************************************************)

(* $Id: version.ml,v 1.5 2007/01/23 15:50:04 weis Exp $ *)

(* The current version of ocamlp3l *)
let major = 2
and minor = 3
and patch_level = 0
and devinfo = ""
and version_date = "2007-01-22";;

let version =
  let patch_level =
    if patch_level > 0 then Printf.sprintf ".%i" patch_level else "" in
  Printf.sprintf "%i.%02i%s" major minor patch_level;;

let full_version = Printf.sprintf "%s+%s (%s)" version devinfo version_date;;

let print_version () = prerr_endline ("camlp3l, version " ^ version);;

let print_full_version () =
  prerr_endline ("camlp3l, version " ^ full_version);;

Command_options.add
  "--camlp3l-version"
  (Arg.Unit print_full_version)
  "CamlP3l detailed version information in the format \
   \"%i.%02i[.%i]+%s (%s)\".";
Command_options.add
  "-cv"
  (Arg.Unit print_version)
  "CamlP3l version information in the format \"%i.%02i[.%i]\".";;
