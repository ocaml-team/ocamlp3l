let generate_input_stream =
  let x = ref 0.0 in
  (function () -> 
    begin
      x := !x +. 1.0;
      if(!x < 10.0) then !x else raise End_of_file
    end);;

let print_result x =
  print_float x; print_newline();;

let farm_worker _ x = x *. x;;

let succ _ x = x+1;;

let degree = ref 4;;



(* define a parfun expression directly *)
let fbody () = seq(fun _ x -> x)|||seq(fun _ x -> x)|||(mapvector(seq(succ) |||seq(succ) ,!degree)) ;;
let f = parfun fbody;;

(* define another parfun expression directly *)
let gbody () = seq(fun _ x -> x)||| (mapvector(seq(farm_worker)|||seq(farm_worker),!degree));;
let g = parfun gbody;;


(* simulate the old pardo *)

let h = parfun (fun () -> (farm ~col:2 ~colv:[5;6;7;8] (seq(farm_worker),4)));;

(* entry points *)
let ic = open_in "config";;
let ib = (*Scanf.Scanning.from_channel*) ic;;
let _ =
  Scanf.(*b*)fscanf ib "%d"
    (fun n -> degree:=n);;
close_in ic;;

(* test parfun and pardo *)
pardo
(fun () ->

  let (s: int array P3lstream.t) = (f (P3lstream.of_list [[|1;2;3;4;5|];[|6;7;8;9;10;11;12;13|]])) in
  P3lstream.iter (fun v -> Array.iter (fun n -> (Printf.printf "%d \n" n;print_newline())) v) s;

  let (t:float array P3lstream.t) = (g (P3lstream.of_list [[|4.0;5.0;6.0|]])) in
  P3lstream.iter (fun v -> Array.iter (fun n -> (Printf.printf "%f \n" n;print_newline())) v) t;

  let (s':int array P3lstream.t) = (f (P3lstream.of_list [[|4|];[|5|];[|6|]])) in
  P3lstream.iter (fun v -> Array.iter (fun n -> (Printf.printf "%d \n" n;print_newline())) v) s';
    )
;;
