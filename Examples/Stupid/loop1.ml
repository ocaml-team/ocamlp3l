let c x = x < 20;;

let f x = x + 1;;

let generate_stream = 
  let n = ref 10 in
  (function _ -> 
    (n:=!n-1; if(!n=0) then (raise End_of_file) else !n));;

let nihil _ = ();;

let prog ()= 
  startstop
    (generate_stream,nihil)
    ((function x -> print_int x; print_newline()),nihil,nihil)
    (seq(fun x-> print_int x; print_newline();x)|||loop((c),seq(f)))
in 
  pardo prog;;

