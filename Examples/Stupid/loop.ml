(* stupid loop used to test the loop template *)
(* each process prints out the received packets and status *)

let stage1 x = 
  Printf.printf "Stage1 received %d" x;
  print_newline();
  x + 1;;

let conditionf x = 
  if(x>5) 
  then 
    (Printf.printf "Condition false on %d" x; 
     print_newline();false)
  else 
    (Printf.printf "Condition true on %d" x; print_newline();
     true);;

let condition = seq(conditionf);;

let loopbodyf x = 
  Printf.printf "loopbody received %d" x ; print_newline();
  x + 1;;

let loopbody = seq(loopbodyf);;

let stage2 = loop(conditionf,loopbody);;

let stage3 x = Printf.printf "Stage3 received %d" x; print_newline();;

let generate_stream = 
  let n = ref 10 in
  (function _ -> 
    (n:=!n-1; if(!n=0) then raise End_of_file else !n));;

let nihil _ = ();;

let prog ()= 
  startstop
    (generate_stream,nihil)
    (nihil,nihil,nihil)
    (seq(stage1) ||| stage2 ||| seq(stage3))
in 
  pardo prog;;

