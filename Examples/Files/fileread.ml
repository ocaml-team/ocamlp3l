(* how to handle files as streams ... *)

(* input data stream out of a file *)
let fd = ref stdin ;;

let init_input filename = 
  (function () ->
    fd := open_in filename);;

let generate_stream = 
  (function () -> 
    try
      (input_value !fd : int)
    with
       _ -> raise End_of_file);;

let write_to_file l nam = 
  let rec wlist fd = function 
      [] -> ()
    | x::rx -> output_value fd x; wlist fd rx in
  let fd = open_out nam in
  wlist fd l;close_out fd;;

let read_from_file nam = 
  let rec rlist fd = 
    (try
      print_string (string_of_int (input_value fd : int));
      rlist fd;
    with
      _ -> print_newline()) in
  let fd = open_in nam in
  rlist fd;;

(* output data stream to a file *)
let fdout = ref stdout;;

let init_out name = 
  (function () -> fdout := open_out name) ;;

let end_out () = 
  close_out !fdout;;

let consume_out x = 
  output_value !fdout x;;
  

let square x = x * x;;
let id x = x;;

let progr ()= 
  startstop
    (generate_stream, (init_input "cicciazza"))
    (consume_out, (init_out "cicciazzaout"), end_out)
    (seq(square))

in 
   pardo progr;;
