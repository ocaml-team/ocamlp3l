(* the life game *)
(* 
   two neighbours alive => stay
   three neighbours alive => alive
   else => dead
*)

open Graphics;;

let n = 200;; (* the board is n by n, mesh toroidal *)

let init_board () =
  let board = Array.create_matrix n n 0 in
  for i = 0 to (n-1) do
    for j = 0 to (n-1) do 
      if ((Random.int 10) < 2) 
      then Array.set board.(i) j red
      else Array.set board.(i) j white
    done
  done;
  board;;

let dist i j radius = 
  let c = n/2 in
  let di = (i-c)*(i-c) in
  let dj = (j-c)*(j-c) in
  if((di+dj)<(radius*radius)) then true else false;;

let c_init_board radius =
  let board = Array.create_matrix n n 0 in
  for i = 0 to (n-1) do
    for j = 0 to (n-1) do 
      if (dist i j radius)
      then
      	(if ((Random.int 10) < 2) 
      	then Array.set board.(i) j red
      	else Array.set board.(i) j white)
      else
	Array.set board.(i) j white
    done
  done;
  board;;


let display board =
  draw_image (make_image board) 0 0 ;;

let alive_stencil stencil = 
  let sum = ref 0 in
  let res = ref 0 in
  for i = 0 to 2 do
    for j = 0 to 2 do
      sum := !sum + (if(stencil.(i).(j) = white) then 0 else 1)
    done
  done;
  sum := !sum - (if(stencil.(1).(1) = white) then 0 else 1);
  if(!sum = 2) then res:=stencil.(1).(1);
  if(!sum = 3) then res:=1;
  if(!res = 0) then white else red
;;

let make_stencil m ii jj = 
  let s = Array.create_matrix 3 3 0 in
  for i = 0 to 2 do
    for j = 0 to 2 do
      Array.set s.(i) (j)  m.((i+ii-1+n) mod n).((j+jj-1+n) mod n)
    done
  done;
  s
;;

let lifestep_stencil m = 
  let mm = Array.create_matrix n n 0 in
  for i = 0 to (n-1) do
    for j = 0 to (n-1) do
      Array.set mm.(i) j (alive_stencil (make_stencil m i j))
    done
  done;
  mm
;;


let gameoflife ()board itern = 
  begin
    open_graph (" "^(string_of_int (n))^"x"^(string_of_int (n)));
    for i = 0 to itern do
      begin
	(* clear_graph(); *)
	display !board;
      	(* Unix.sleep 1; *)
      	let newboard =  lifestep_stencil !board in
	board := newboard;
      end
    done;
    close_graph()
  end;;

let settantapercentodi x = 
  (truncate ((float x) *. 0.70));;

gameoflife (ref (c_init_board 20)) 100;;
(* gameoflife (ref (init_board_user())) 100;;  *)








