(* the life game : skeleton version *)
(* 
   two neighbours alive => stay
   three neighbours alive => alive
   else => dead
*)

open Graphics;;

(* these are the parameters of the game *)
let n = 100;; (* the board is n by n, mesh toroidal *)
let ppp = 5;; (* point per pixel *)

let init_board () =
  let board = Array.create_matrix n n 0 in
  for i = 0 to (n-1) do
    for j = 0 to (n-1) do 
      if ((Random.int 10) < 3) 
      then Array.set board.(i) j 1
      else Array.set board.(i) j 0
    done
  done;
  board;;

let first_display = ref true;;

let display board =
  let n = Array.length board in
  let m = Array.length board.(0) in
  begin
    if(!first_display) 
    then begin
      open_graph (" "^(string_of_int (n*ppp))^"x"^(string_of_int (n*ppp)));
      first_display := false
    end;
    for i = 0 to (n-1) do
      for j = 0 to (m-1) do
	if(board.(i).(j) = 1) 
	then begin
	  set_color blue;
	  fill_circle (i*ppp+ppp/2) (j*ppp+ppp/2) (ppp/2);
	end
	else begin
	  set_color white;
	  fill_circle (i*ppp+ppp/2) (j*ppp+ppp/2) (ppp/2);
	end
      done
    done
  end;;


let display_tasks t = 
  for i = 0 to (Array.length t-1) do
    begin
      clear_graph();
      display t.(i);
      Unix.sleep 3;
    end
  done;;

let init_board_user () = 
  let board = Array.create_matrix n n 0 in
  let fine = ref false in
  begin
    open_graph (" "^(string_of_int (n*ppp))^"x"^(string_of_int (n*ppp)));
    display board;
    while(not !fine) do
      let stat = wait_next_event [Button_down;Key_pressed] in
      if(stat.keypressed) then fine:=true
      else 
	begin
	  let i = stat.mouse_x / ppp in
	  let j = stat.mouse_y / ppp in
	  begin
	    Array.set board.(i) (j) 1;
	    clear_graph();
	    display board
	  end
	end
    done;
    close_graph();
    board
  end;;

let alive_stencil stencil = 
  let sum = ref 0 in
  let res = ref 0 in
  for i = 0 to 2 do
    for j = 0 to 2 do
      sum := !sum + stencil.(i).(j)
    done
  done;
  sum := !sum - stencil.(1).(1);
  if(!sum = 2) then res:=stencil.(1).(1);
  if(!sum = 3) then res:=1;
  !res
;;

let make_stencil m ii jj = 
  let s = Array.create_matrix 3 3 0 in
  for i = 0 to 2 do
    for j = 0 to 2 do
      Array.set s.(i) (j)  m.((i+ii-1+n) mod n).((j+jj-1+n) mod n)
    done
  done;
  s
;;

let lifestep_stencil m = 
  let mm = Array.create_matrix n n 0 in
  for i = 0 to (n-1) do
    for j = 0 to (n-1) do
      Array.set mm.(i) j (alive_stencil (make_stencil m i j))
    done
  done;
  mm
;;


(* prepare the matrix of stencils :
   out of a matrix which is n x n
   take out k matrixes which are (n/k + 2) x n 
 *)

exception NotDivisible;;

(* get k+2 rows from m starting at ii-1 *)
let k_rows_from m ii k = 
  let res = Array.create (k+2) m.(0) in
  for i = 0 to (k+1) do
    Array.set res i m.((ii+i-1+n) mod n)
  done;
  res;;

(* prepares a vector of k slices of m (each slice has stencil rows) *)
let prepare_tasks k m  = 
  if (not ((n mod k) = 0)) then raise NotDivisible
  else
    let res = Array.create k (k_rows_from m 0 (n/k)) in
    begin
      for kk = 0 to (k-1) do
      	Array.set res kk (k_rows_from m (kk*(n/k)) (n/k))
      done;
      res
    end;;
    
(* compute a slice of the result *)
let compute_slice slice = 
  let k = Array.length slice - 2 in
  let res = Array.create_matrix k n 0 in
  for i = 0 to (k-2) do
    for j = 0 to (n-1) do
      Array.set res.(i) j (alive_stencil (make_stencil slice (i+1) j))
    done
  done;
  res
;;

let merge_slices slices = 
  let k = Array.length slices in
  let m = Array.length slices.(0) in
  let res = Array.create n slices.(0).(0) in
  for i = 0 to (n-1) do
    Array.set res i (slices.(i/m).(i mod m))
  done;
  res;;

let display_and_pass m = 
  display m; m;;

let lifestep = (seq((prepare_tasks 2)) ||| mapvector(seq(compute_slice),5) ||| 
                seq((merge_slices)) ||| seq(display_and_pass) );;


(* let lifestep = (seq((prepare_tasks 2)) ||| mapvector(seq(compute_slice),5) ||| 
                seq((merge_slices)) ||| seq(display_and_pass));; *)

let stream_generate = 
  let n = ref 2 in
  (function _ -> 
    if(!n = 0)
    then raise End_of_file
    else (n:=!n-1;init_board()));;

let finite_iteration n = 
  let count = ref n in
  (function _ -> (count := !count - 1; (not(!count=0))));;

let inf_loop = (loop ((fun _  -> true) , lifestep));;
let n_loop   = loop ((finite_iteration 5),lifestep);;

let open_graphics () = 
  open_graph (" "^(string_of_int (n*ppp))^"x"^(string_of_int (n*ppp)));;

(* the game of life with the skeletons *)
let gameoflife ()= 
  startstop
    (stream_generate,(fun _ -> ()))
    (display,open_graphics,close_graph)
    n_loop
in
  pardo gameoflife;;
