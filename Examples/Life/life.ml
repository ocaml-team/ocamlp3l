(* the life game *)
(* 
   two neighbours alive => stay
   three neighbours alive => alive
   else => dead
*)

open Graphics;;

(* these are the parameters of the game *)
let n = 100;; (* the board is n by n, mesh toroidal *)
let ppp = 5;; (* point per pixel *)

let init_board () =
  let board = Array.create_matrix n n 0 in
  for i = 0 to (n-1) do
    for j = 0 to (n-1) do 
      if ((Random.int 10) < 3) 
      then Array.set board.(i) j 1
      else Array.set board.(i) j 0
    done
  done;
  board;;

let dist i j radius = 
  let c = n/2 in
  let di = (i-c)*(i-c) in
  let dj = (j-c)*(j-c) in
  if((di+dj)<(radius*radius)) then true else false;;

let c_init_board radius =
  let board = Array.create_matrix n n 0 in
  for i = 0 to (n-1) do
    for j = 0 to (n-1) do 
      if (dist i j radius)
      then
      	(if ((Random.int 10) < 2) 
      	then Array.set board.(i) j 1
      	else Array.set board.(i) j 0)
      else
	Array.set board.(i) j 0
    done
  done;
  board;;

let display board =
  let n = Array.length board in
  begin
    for i = 0 to (n-1) do
      for j = 0 to (n-1) do
	if(board.(i).(j) = 1) 
	then begin
	  set_color blue;
	  fill_circle (i*ppp+ppp/2) (j*ppp+ppp/2) (ppp/2);
	end
	else begin
	  set_color white;
	  fill_circle (i*ppp+ppp/2) (j*ppp+ppp/2) (ppp/2);
	end
      done
    done
  end;;

let init_board_user () = 
  let board = Array.create_matrix n n 0 in
  let fine = ref false in
  begin
    open_graph (" "^(string_of_int (n*ppp))^"x"^(string_of_int (n*ppp)));
    display board;
    while(not !fine) do
      let stat = wait_next_event [Button_down;Key_pressed] in
      if(stat.keypressed) then fine:=true
      else 
	begin
	  let i = stat.mouse_x / ppp in
	  let j = stat.mouse_y / ppp in
	  begin
	    Array.set board.(i) (j) 1;
	    clear_graph();
	    display board
	  end
	end
    done;
    close_graph();
    board
  end;;

let alive_stencil stencil = 
  let sum = ref 0 in
  let res = ref 0 in
  for i = 0 to 2 do
    for j = 0 to 2 do
      sum := !sum + stencil.(i).(j)
    done
  done;
  sum := !sum - stencil.(1).(1);
  if(!sum = 2) then res:=stencil.(1).(1);
  if(!sum = 3) then res:=1;
  !res
;;

let make_stencil m ii jj = 
  let s = Array.create_matrix 3 3 0 in
  for i = 0 to 2 do
    for j = 0 to 2 do
      Array.set s.(i) (j)  m.((i+ii-1+n) mod n).((j+jj-1+n) mod n)
    done
  done;
  s
;;

let lifestep_stencil m = 
  let mm = Array.create_matrix n n 0 in
  for i = 0 to (n-1) do
    for j = 0 to (n-1) do
      Array.set mm.(i) j (alive_stencil (make_stencil m i j))
    done
  done;
  mm
;;


let gameoflife board itern = 
  begin
    open_graph (" "^(string_of_int (n*ppp))^"x"^(string_of_int (n*ppp)));
    for i = 0 to itern do
      begin
	(* clear_graph(); *)
	display !board;
      	(* Unix.sleep 1; *)
      	let newboard =  lifestep_stencil !board in
	board := newboard;
      end
    done;
    close_graph()
  end;;


gameoflife (ref (c_init_board 30)) 100;;
(* gameoflife (ref (init_board_user())) 100;;  *)





