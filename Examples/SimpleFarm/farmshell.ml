(* program computing a function over a stream of floats ... 
   by using a farm (very simple!) 
   BUT the funcion is computed by an external command 
   which is spawned and connected via stdin / stdout to the farm worker in the seq skeleton

   IMPORTANT: remember to fflush data output in the external command source, or you will block!
*)

let farm_worker _ = let spawned = ref false and cin = ref None and cout = ref
None in
  (fun x -> 
    if not !spawned then
       begin
       let (ic,oc) = Unix.open_process "./square" in cin := Some ic; cout := Some oc; spawned:=true
       end;
    let Some ic, Some oc = (!cin , !cout) in
    Printf.fprintf oc "%d\n" x; Pervasives.flush oc;
    let i = Scanf.fscanf ic "%d" (fun x -> x) in i
  )
;;

let compute = parfun (fun () -> farm(seq(farm_worker),4));;

let print_result x =
  print_int x; print_newline();;

pardo(fun () ->
  let is = P3lstream.of_list [1;2;3;4;5;6;7;8;9] in
  let s' = compute is in P3lstream.iter print_result s';
);;
