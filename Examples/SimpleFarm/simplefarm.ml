(* program computing a function over a stream of floats ... 
   by using a farm (very simple!) *)

let farm_worker _ = fun x -> 
  (* To prove the effect of lazy stream, for each input items 
     we delay 3 seconds to simulate the heavy computation *)
  Unix.sleep 3; 
  (* We also choose a special element which seems to be computing
     forever , however it won't affect the computation and output 
     of other elements. (In sequential semantics, since the excution 
     is single process, it will block the elements after it but not 
     the ones before it *)
  if x = 7.0 then Unix.sleep 15;
  x *. x;;

let print_result x =
  print_float x; print_newline();;

(* parfun turns a skeleton expression into a standard Ocaml function *)

let compute =
  parfun (fun () ->  (farm ~col:2 ~colv:[5;6;7;8] (seq farm_worker, 4)));;

pardo(fun () ->
  print_endline "Note that, the computation of each element will take 3 seconds except the 7.0 .* 7.0 which will take 15 seconds.";
  let is = P3lstream.of_list [1.0;2.0;3.0;4.0;5.0;6.0;7.0;8.0;9.0] in
  let s' = compute is in P3lstream.iter print_result s';
);;






