(* computes the integral of f(x) between a and b *)

(* sample f function *)
let f x = x *. x -. 3.0 *. x +. 5.0;;

let rec l_intervals a b n =
  if(n=0) then []
  else 
    let delta = (b -. a) /. (float n) in
    (a,(a+.delta))::(l_intervals (a+.delta) b (n - 1));;

let intervals n (a,b) = 
  Array.of_list (l_intervals a b n);;

let area f (a,b) = 
  (b -. a)*.(f a);;

let generate_intervals = 
  let n = ref 5 in
  let max = 10.0 in
  (function () -> 
    if(!n = 0) 
    then raise End_of_file 
    else
      let a = Random.float max in
      let b = Random.float max in
      (n:=!n-1; if(a < b) then (a,b) else (b,a)));;
  
let sum (x,y) = x +. y;;

let nihil _ = ();;

let progr ()= 
  startstop
    (generate_intervals,nihil)
    ((function (x) -> 
      Printf.printf "Integral is %f" x; print_newline()),nihil,nihil)
    (seq(intervals 10) ||| 
    mapvector(seq(area f),5) ||| 
    reducevector(seq(sum),2))
in
  pardo progr;;

