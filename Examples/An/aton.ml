(* matrix power *)

let mm a b = 
  let n = Array.length a in
  let c = Array.create_matrix  n n 0.0  in
  for i = 0 to (n-1) do
    for j = 0 to (n-1) do
      for k = 0 to (n-1) do
      	Array.set c.(i) j (c.(i).(j) +. a.(i).(k) *. b.(k).(j))
      done
    done
  done;
  c;;

let a = [| [| 1.0 ; 2.0 ; 1.0 |] ;
	   [| 3.0 ;-1.0 ; 1.0 |] ;
	   [|-1.0 ; 0.0 ;-1.0 |] |];;

let apowern a n = 
  let aorig = a in
  let rec apn a n = 
    match n with
      0 -> a
    | i -> (apn (mm a aorig) (i-1)) in

  (apn a n);;
  
let genmat n = 
  let max = 10.0 in
  let a = Array.create_matrix n n 0.0 in
  for i = 0 to (n-1) do
    for j = 0 to (n-1) do
      Array.set a.(i) j ((Random.float max)-.(max /. 2.0))
    done
  done;
  a
;;

let genid n = 
  let a = Array.create_matrix n n 0.0 in  
  for i = 0 to (n-1) do
    for j = 0 to (n-1) do
      if(i=j) then Array.set a.(i) j 1.0
    done
  done;
  a
;;

let start = 
  let n = 3 in
  let pow = 2 in
  let stream_length = ref 0 in
  let max_stream_length = 10 in
  (function () -> 
    if(!stream_length < max_stream_length) 
    then (stream_length := !stream_length + 1;((genmat n),pow))
    else raise End_of_file);;

let farm_worker (a,n) = 
  apowern a n ;;

let print_matrix a = 
  let n = Array.length a in
  for i = 0 to (n-1) do 
    print_newline();
    for j = 0 to (n-1) do 
      print_float a.(i).(j)
    done
  done;
  print_newline();;

let program ()= 
  startstop
    (start,(fun _ -> ()))
    (print_matrix,(fun _ -> ()),(fun _ -> ()))
    (farm(seq(farm_worker),4))
in 
  pardo program;;
