(***********************************************************************)
(*                                                                     *)
(*                              OCamlP3l                               *)
(*                                                                     *)
(*                  projet Estime, INRIA-Rocquencourt                  *)
(*                                                                     *)
(*  Copyright 2003-2007 INRIA.                                         *)
(*  Institut National de Recherche en Informatique et en Automatique.  *)
(*  All rights reserved. This file is distributed only by permission.  *)
(*                                                                     *)
(*  Francois Clement <Francois.Clement@inria.fr>,                      *)
(*  Pierre Weis <Pierre.Weis@inria.fr>.                                *)
(*                                                                     *)
(***********************************************************************)

(* $Id: my_unix.mli,v 1.3 2007/01/23 15:58:18 weis Exp $ *)

val spawn_init :
    (Scanf.Scanning.scanbuf * out_channel -> 'a) ->
      string -> Scanf.Scanning.scanbuf option ref -> out_channel option ref ->
        Scanf.Scanning.scanbuf * out_channel;;

val spawn :
    string -> Scanf.Scanning.scanbuf option ref -> out_channel option ref ->
      Scanf.Scanning.scanbuf * out_channel;;

val launch_connected_processes : string -> string -> unit;;
 (** [launch_connected_processes prog1 prog2] launches [prog1] and
 [prog2] with each other reading and writing to the corresponding
 [stdin/stdout] of the other program. *)
