(***********************************************************************)
(*                                                                     *)
(*                              OCamlP3l                               *)
(*                                                                     *)
(*                  projet Estime, INRIA-Rocquencourt                  *)
(*                                                                     *)
(*  Copyright 2003-2007 INRIA.                                         *)
(*  Institut National de Recherche en Informatique et en Automatique.  *)
(*  All rights reserved. This file is distributed only by permission.  *)
(*                                                                     *)
(*  Francois Clement <Francois.Clement@inria.fr>,                      *)
(*  Pierre Weis <Pierre.Weis@inria.fr>.                                *)
(*                                                                     *)
(***********************************************************************)

(* $Id: rdec.ml,v 1.7 2007/01/23 15:58:18 weis Exp $ *)

(* See "rdec.mli" *)

(* Uses scanning buffers, which needs at least OCaml-3.07 *)
(* When using buffers, we have to know the number of items to be read, *)
(* or at least know the value of the last one. *)

type rectangle = {x0 : float; y0 : float; lx : float; ly : float};;
type rectangular_grid = {domain : rectangle; nx : int; ny :int};;
type computational_rectangular_grid =
    {mesh : rectangular_grid; px : int; py : int};;
type regular_decomposition =
  {comp_grid : computational_rectangular_grid; nsbdx : int; nsbdy : int};;
type subdomain = regular_decomposition * int;;
  
let rectangle_of x0 y0 lx ly = {x0 = x0; y0 = y0; lx = lx; ly = ly};;
    
let rectangular_grid_of x0 y0 lx ly nx ny =
  let r = rectangle_of x0 y0 lx ly in
  {domain = r; nx = nx; ny = ny};;
    
let computational_rectangular_grid_of x0 y0 lx ly nx ny px py =
  let g = rectangular_grid_of x0 y0 lx ly nx ny in
  {mesh = g; px = px; py = py};;
    
let regular_decomposition_of x0 y0 lx ly nx ny px py nsbdx nsbdy =
  let g = computational_rectangular_grid_of x0 y0 lx ly nx ny px py in
  {comp_grid = g; nsbdx = nsbdx; nsbdy = nsbdy};;
    
let rectangle_of_rectangular_grid g =
  let {domain = {x0 = x0; y0 = y0; lx = lx; ly = ly}} = g in
  rectangle_of x0 y0 lx ly;;
    
let rectangle_of_computational_rectangular_grid {mesh = mesh} =
  rectangle_of_rectangular_grid mesh;;
    
let rectangle_of_regular_decomposition {comp_grid = comp_grid} =
  rectangle_of_computational_rectangular_grid comp_grid;;
    
let of_rectangle r = r.x0, r.y0, r.lx, r.ly;;
let number_of_mesh_of_rectangular_grid g = g.nx, g.ny;;
let rectangular_grid_of_computational_rectangular_grid g = g.mesh;;
let overlapping_of_computational_rectangular_grid g = g.px, g.py;;
let computational_rectangular_grid_of_regular_decomposition dec =
  dec.comp_grid;;
let number_of_subdomains_of_regular_decomposition dec = dec.nsbdx, dec.nsbdy;;
    
let print_rectangle oc r =
  Printf.fprintf oc "%f %f\n%f %f\n" r.x0 r.y0 r.lx r.ly;;
    
let print_rectangular_grid oc g =
  Printf.fprintf oc "%a%d %d\n" print_rectangle g.domain g.nx g.ny;;
    
let print_computational_rectangular_grid oc g =
  Printf.fprintf oc "%a%d %d\n" print_rectangular_grid g.mesh g.px g.py;;
    
let print_regular_decomposition oc dec =
  Printf.fprintf oc "%a%d %d\n"
    print_computational_rectangular_grid dec.comp_grid dec.nsbdx dec.nsbdy;;
    
let read_rectangle ib = Scanf.bscanf ib "%f %f\n%f %f\n" rectangle_of;;
    
let read_rectangular_grid ib =
  let r = read_rectangle ib in
  Scanf.bscanf ib "%d %d\n" (fun nx ny -> {domain = r; nx = nx; ny = ny});;
    
let read_computational_rectangular_grid ib =
  let g = read_rectangular_grid ib in
  Scanf.bscanf ib "%d %d\n" (fun px py -> {mesh = g; px = px; py = py});;
    
let read_regular_decomposition ib =
  let g = read_computational_rectangular_grid ib in
  Scanf.bscanf ib "%d %d\n"
    (fun nsbdx nsbdy -> {comp_grid = g; nsbdx = nsbdx; nsbdy = nsbdy});;

exception Wrong_Input of string;;

let check_regular_decomposition
    {comp_grid =
     {mesh = {domain = {lx = lx; ly = ly}; nx = nx; ny = ny};
      px = px; py = py};
     nsbdx = nsbdx; nsbdy = nsbdy} =
  if lx <= 0. || ly <= 0. then
    raise (Wrong_Input "Nonpositive length!");
  if nx <= 0 || ny <= 0 then
    raise (Wrong_Input "Nonpositive number of intervals!");
  if px <= 0 || py <= 0 then
    raise (Wrong_Input "Nonpositive overlapping!");
  if px > nx / nsbdx || py > ny / nsbdy then
    raise (Wrong_Input "Overlapping is too large!");
  if nsbdx <= 0 || nsbdy <= 0 then
    raise (Wrong_Input "Nonpositive number of subdomains!");
  if nsbdx > nx || nsbdy > ny then
    raise (Wrong_Input "Too many subdomains!");
  if nx mod nsbdx <> 0 || ny mod nsbdy <> 0 then
    raise (Wrong_Input
             "Number of subdomains do not divide number of intervals!");;
    
let parse_config_file fname =
  let ib =
    try Scanf.Scanning.from_file fname
    with Sys_error s -> prerr_endline s; exit 6 in
  let dec = read_regular_decomposition ib in
  check_regular_decomposition dec;
  dec;;
    
type location = EastSide | NorthSide | SouthSide | WestSide;;
module OrderedLocation = struct type t = location let compare = compare end;;
module Position = Set.Make (OrderedLocation);;
type position = Position.t;;

exception Out_of_range of int;;
    
let two_indices_numbering nx ny i =
    if i < 0 || i > nx * ny - 1 then raise (Out_of_range i) else
    (i mod nx, i / nx);;

let rectangular_grid_of_subdomain (dec, i) =
  let {comp_grid =
       {mesh = {domain = {x0 = x0; y0 = y0; lx = lx; ly = ly};
                nx = nx; ny = ny}};
       nsbdx = nsbdx; nsbdy = nsbdy} = dec in
  let ix, iy = two_indices_numbering nsbdx nsbdy i in
  let lxi = lx /. float_of_int nsbdx and lyi = ly /. float_of_int nsbdy in
  rectangular_grid_of
    (x0 +. float_of_int ix *. lxi) (y0 +. float_of_int iy *. lyi)
    lxi lyi (nx / nsbdx) (ny / nsbdy);;

let position_of_subdomain ({nsbdx = nsbdx; nsbdy = nsbdy}, i) =
  let ix, iy = two_indices_numbering nsbdx nsbdy i in
  let locations_to_add = [(ix = 0, WestSide); (ix = nsbdx - 1, EastSide);
                          (iy = 0, SouthSide); (iy = nsbdy - 1, NorthSide)] in
  let rec loop accu = function
    | [] -> accu
    | (b, e) :: l -> if b then loop (Position.add e accu) l else loop accu l
  in
  loop Position.empty locations_to_add;;
    
let complement pos =
  let fullset = Position.add WestSide (Position.add SouthSide
     (Position.add NorthSide (Position.add EastSide Position.empty))) in
  Position.diff fullset pos;;
    
let east_extension_of_computational_rectangular_grid
    {mesh = {domain = {x0 = x0; y0 = y0; lx = lx; ly = ly}; nx = nx; ny = ny};
     px = px; py = py} =
  let dx = lx /. float_of_int nx in
  computational_rectangular_grid_of
    x0 y0 (lx +. float_of_int px *. dx) ly (nx + px) ny px py;;
    
let north_extension_of_computational_rectangular_grid
    {mesh = {domain = {x0 = x0; y0 = y0; lx = lx; ly = ly}; nx = nx; ny = ny};
     px = px; py = py} =
  let dy = ly /. float_of_int ny in
  computational_rectangular_grid_of
    x0 y0 lx (ly +. float_of_int py *. dy) nx (ny + py) px py;;
    
let south_extension_of_computational_rectangular_grid
    {mesh = {domain = {x0 = x0; y0 = y0; lx = lx; ly = ly}; nx = nx; ny = ny};
     px = px; py = py} =
  let dy = ly /. float_of_int ny in
  computational_rectangular_grid_of
    x0 (y0 -. float_of_int py *. dy) lx (ly +. float_of_int py *. dy)
    nx (ny + py) px py;;
    
let west_extension_of_computational_rectangular_grid
    {mesh = {domain = {x0 = x0; y0 = y0; lx = lx; ly = ly}; nx = nx; ny = ny};
     px = px; py = py} =
  let dx = lx /. float_of_int nx in
   computational_rectangular_grid_of
    (x0 -. float_of_int px *. dx) y0 (lx +. float_of_int px *. dx) ly
    (nx + px) ny px py;;
 
let computational_rectangular_grid_of_subdomain subd =
  let pos = position_of_subdomain subd in
  let dec = fst subd in
  let g0 = {mesh = rectangular_grid_of_subdomain subd;
            px = dec.comp_grid.px; py = dec.comp_grid.py} in
  let ext_of_grid g = function
    | EastSide -> east_extension_of_computational_rectangular_grid g
    | NorthSide -> north_extension_of_computational_rectangular_grid g
    | SouthSide -> south_extension_of_computational_rectangular_grid g
    | WestSide -> west_extension_of_computational_rectangular_grid g
  in
  let rec loop accu = function
    | [] -> accu
    | cp :: l -> loop (ext_of_grid accu cp) l
  in
  loop g0 (Position.elements (complement pos));;
    
type neighbor = int;;
type neighborhood = {east_n : neighbor; north_n : neighbor;
                     south_n : neighbor; west_n : neighbor};;
type connectivity_table = neighborhood array;;
    
let neighborhood_of e n s w =
  {east_n = e; north_n = n; south_n = s; west_n = w};;
    
let change_east_neighbor
    {east_n = e; north_n = n; south_n = s; west_n = w} i =
  neighborhood_of i n s w;;
    
let change_north_neighbor
    {east_n = e; north_n = n; south_n = s; west_n = w} i =
  neighborhood_of e i s w;;
    
let change_south_neighbor
    {east_n = e; north_n = n; south_n = s; west_n = w} i =
  neighborhood_of e n i w;;
    
let change_west_neighbor
    {east_n = e; north_n = n; south_n = s; west_n = w} i =
  neighborhood_of e n s i;;
    
let neighborhood_of_subdomain (dec, i) =
  let pos = position_of_subdomain (dec, i) in
  let n = dec.nsbdx in
  let nbh0 = {east_n = - 1; north_n = - 1; south_n = - 1; west_n = - 1} in
  let change_neighborhood nbh = function
    | EastSide -> change_east_neighbor nbh (i + 1)
    | NorthSide -> change_north_neighbor nbh (i + n)
    | SouthSide -> change_south_neighbor nbh (i - n)
    | WestSide -> change_west_neighbor nbh (i - 1)
  in
  let rec loop accu = function
    | [] -> accu
    | cp :: l -> loop (change_neighborhood accu cp) l
  in
  loop nbh0 (Position.elements (complement pos));;
    
let connectivity_table_of_regular_decomposition dec =
  let n = dec.nsbdx * dec.nsbdy in
  Array.init n (fun i -> neighborhood_of_subdomain (dec, i));;
    
let east_neighbor tbl i = let {east_n = e} = tbl.(i) in e;;
let north_neighbor tbl i = let {north_n = n} = tbl.(i) in n;;
let south_neighbor tbl i = let {south_n = s} = tbl.(i) in s;;
let west_neighbor tbl i = let {west_n = w} = tbl.(i) in w;;

type edge_values = float array;;
type boundary = {east_ev : edge_values; north_ev : edge_values;
                 south_ev : edge_values; west_ev : edge_values};;
type interface = boundary array;;
    
let print_edge_values oc ev =
  let f v =
    Printf.fprintf oc "%f\n" v(* ; Printf.eprintf " %f" v *) in
  (* Printf.eprintf "ML:OUT"; *)
  Array.iter f ev;
  (* Printf.eprintf "\n"; flush stderr *);;
    
let print_boundary oc bd =
  Printf.fprintf oc "%a%a%a%a"
    print_edge_values bd.east_ev print_edge_values bd.north_ev
    print_edge_values bd.south_ev print_edge_values bd.west_ev;
  (* Printf.eprintf "ML:OUT completed!\n"; flush stderr *);;
    
(* We need the space in the format " %f\n" for the bscanf! *)
let read_edge_values ib sz =
  let f i =
    Scanf.bscanf ib " %f\n" (fun v -> (* Printf.eprintf " %f" v; *) v) in
  (* Printf.eprintf "ML:IN"; *)
  Array.init sz f;;
    
(* We need the tag "completed" to end the scan *)
let read_boundary ib (nx, ny) =
  let e = read_edge_values ib ny in (* Printf.eprintf "\n"; flush stderr; *)
  let n = read_edge_values ib nx in (* Printf.eprintf "\n"; flush stderr; *)
  let s = read_edge_values ib nx in (* Printf.eprintf "\n"; flush stderr; *)
  let w = read_edge_values ib ny in (* Printf.eprintf "\n"; flush stderr; *)
  Scanf.bscanf ib "completed" ();
  (* Printf.eprintf "ML:IN completed!\n"; flush stderr; *)
  {east_ev = e; north_ev = n; south_ev = s; west_ev = w};;
    
let size_of_boundary bd =
  Array.length bd.north_ev, Array.length bd.east_ev;;
    
let make_zero_edge_values n = Array.make n 0.;;
    
let make_zero_boundary nx ny =
  let e = make_zero_edge_values ny in
  let n = make_zero_edge_values nx in
  let s = make_zero_edge_values nx in
  let w = make_zero_edge_values ny in
  {east_ev = e; north_ev = n; south_ev = s; west_ev = w};;
    
let make_zero_interface dec =
  let f i =
    let {mesh = {nx = nx; ny = ny}} =
      computational_rectangular_grid_of_subdomain (dec, i) in
    make_zero_boundary (nx + 1) (ny + 1) in
  Array.init (dec.nsbdx * dec.nsbdy) f;;
    
let projection tbl intf =
  let f i =
    let je = east_neighbor tbl i and jn = north_neighbor tbl i
    and js = south_neighbor tbl i and jw = west_neighbor tbl i in
    let e =
      if je <> - 1 then Array.copy intf.(je).west_ev else
      Array.make (Array.length intf.(i).east_ev) 0.
    and n =
      if jn <> - 1 then Array.copy intf.(jn).south_ev else
      Array.make (Array.length intf.(i).north_ev) 0.
    and s =
      if js <> - 1 then Array.copy intf.(js).north_ev else
      Array.make (Array.length intf.(i).south_ev) 0.
    and w =
      if jw <> - 1 then Array.copy intf.(jw).east_ev else
      Array.make (Array.length intf.(i).west_ev) 0. in
    {east_ev = Array.copy e; north_ev = Array.copy n;
     south_ev = Array.copy s; west_ev = Array.copy w} in
  Array.init (Array.length tbl) f;;
    
let for_all_subdomains (nsbdx, nsbdy) to_do =
  let num = ref (nsbdx - 1 + nsbdx * nsbdy) in
  for j = nsbdy - 1 downto 0 do
    num := !num - 2 * nsbdx;
    for i = 0 to nsbdx - 1 do
      num := !num + 1;
      to_do !num;
    done;
  done;;
