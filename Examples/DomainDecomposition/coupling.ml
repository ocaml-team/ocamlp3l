(***********************************************************************)
(*                                                                     *)
(*                              OCamlP3l                               *)
(*                                                                     *)
(*                  projet Estime, INRIA-Rocquencourt                  *)
(*                                                                     *)
(*  Copyright 2003-2007 INRIA.                                         *)
(*  Institut National de Recherche en Informatique et en Automatique.  *)
(*  All rights reserved. This file is distributed only by permission.  *)
(*                                                                     *)
(*  Francois Clement <Francois.Clement@inria.fr>,                      *)
(*  Pierre Weis <Pierre.Weis@inria.fr>.                                *)
(*                                                                     *)
(***********************************************************************)

(* $Id: coupling.ml,v 1.14 2007/01/23 15:58:18 weis Exp $ *)

(******************************************************************************)
(* Program solving Poisson's equation on a rectangle with homogeneous         *)
(* Dirichlet boundary conditions, ie                                          *)
(*          { - Laplacian u = f  on Omega,                                    *)
(*          {             u = 0  on Gamma = boundary of Omega.                *)
(* The Partial Derivative Equation (PDE) is solved using Domain Decomposition *)
(* with overlapping: firstly, on each subdomain, we input some specific       *)
(* Dirichlet conditions, solve the PDE and then output Dirichlet conditions   *)
(* for the neighbor subdomains; secondly, the solution is searched for as the *)
(* fix point of the previously defined Dirichlet to Dirichlet operator.       *)
(* Computation on each subdomain can be run in parallel.                      *)
(*                                                                            *)
(* The OCamlP3l implementation uses the loop skeleton for the fix point       *)
(* and the mapvector skeleton for the concurrent computation on each          *)
(* subdomain.                                                                 *)
(* The poisson's solver is a C program implementing an implicit 5-point       *)
(* scheme which is spawned and connected via stdin/stdout to                  *)
(* computation_on_a_subdomain in a seq skeleton (see "poisson.c").            *)
(* Graphical facilities using gnuplot are provided (see Gnuplot module).      *)
(*                                                                            *)
(* The Rdec module provides functions to manipulate regular decompositions    *)
(* of regular rectangular grids.                                              *)
(*                                                                            *)
(* IMPORTANT:                                                                 *)
(* - remember to fflush data output in the C program, or you will block!      *)
(* - remember not to rely on global references/arrays; thus it is better to   *)
(*   put them inside the data stream.                                         *)
(* - Meta-data, as the number of workers for farm or mapvector skeletons,     *)
(*   must be set BEFORE the computation network is deployed, ie before any    *)
(*   use of skeleton expressions in the pardo structure.                      *)
(******************************************************************************)

open Gnuplot;;

(*************************************************************)
(* Execution is controlled by data read in a config file     *)
(* This file should contain:                                 *)
(* - the name of the file defining the regular decomposition *)
(* - the basename for result files                           *)
(* - the output type: window, ps, none                       *)
(* - the number of iterations for the fix point problem      *)
(* - the number of iterations for the local subproblems      *)
(* - the refreshment step for the graphical device           *)
(* - the number of seconds to sleep before terminating       *)
(*************************************************************)
let config = "config";;
let ib = Scanf.Scanning.from_file config;;
let rdec_config_file, resu, output, itermax, jtermax, plot_step, a_while =
  Scanf.bscanf ib "%s\n%s\n%s\n%d\n%d\n%d\n%d\n"
    (fun fn fbn out imax jmax step t -> fn, fbn, out, imax, jmax, step, t);;

if gnuplot_version_number = 0. && output <> "none" then begin
  prerr_endline "Gnuplot is not present, \"output type\" should be \"none\".";
  exit 2 end;;

(* See "rdec.mli" for the content of the config file for the Rdec module *)
let dec = Rdec.parse_config_file rdec_config_file;;
Rdec.check_regular_decomposition dec;;
let table = Rdec.connectivity_table_of_regular_decomposition dec;;
let intf0 = Rdec.make_zero_interface dec;;
let nsbdx, nsbdy = Rdec.number_of_subdomains_of_regular_decomposition dec;;
let nbproc = nsbdx * nsbdy;;

(* The type of the components of the array stream elements *)
type subdomain =
    {num : int;
     bd : Rdec.boundary;
     iter : int;
     err : float * float};;
(* Transposed of the type of the stream elements *)
type domain =
    {nums : int array;
     bds : Rdec.boundary array;
     iters : int;
     errs : (float * float) array};;

let split v =
  let n = Array.length v in
  if n = 0 then {nums = [||]; bds = [||]; iters = 0; errs = [||]} else
  let nums = Array.make n v.(0).num
  and bds = Array.make n v.(0).bd
  and iter = v.(0).iter
  and errs = Array.make n v.(0).err in
  for i = 1 to n - 1 do
    nums.(i) <- v.(i).num;
    bds.(i) <- v.(i).bd;
    let wrong_cond = iter <> v.(i).iter in
    if wrong_cond then invalid_arg "split" else
    errs.(i) <- v.(i).err;
  done;
  {nums = nums; bds = bds; iters = iter; errs = errs};;

let combine {nums = nums; bds = bds; iters = iter; errs = errs} =
  let n = Array.length nums  in
  let wrong_cond = Array.length bds <> n || Array.length errs <> n in
  if wrong_cond then invalid_arg "combine" else
  Array.init n
    (fun i -> {num = nums.(i); bd = bds.(i); iter = iter; err = errs.(i)});;

let generate_input_stream =
  let k = ref 0 in
  (fun () ->
    incr k;
    let f i bd = {num = i; bd = bd; iter = 0; err = max_float, max_float} in
    if !k = 1 then Array.mapi f intf0 else raise P3lstream.End_of_stream);;

let print_result r =
  let f {num = num} =
    Printf.eprintf "Computation on subdomain %d has completed\n" num in
  Array.iter f r;
  flush stderr;;

let sleep n = (fun () -> if output = "window" then Unix.sleep n);;

let convergence =
  let t0 = ref (Unix.gettimeofday ()) in
  (fun v ->
    let iter = v.(0).iter in
    let rel_stag, rel_err =
      Array.fold_left
        (fun (rel_stag, rel_err) {err = rs, re} ->
          max rel_stag rs, max rel_err re)
        (0. , 0.) v in
    let dt = Unix.gettimeofday () -. !t0 in
    Printf.eprintf
      "iter = %d, rel_stag = %g rel_err = %.2f%% (time = %.2f s)\n"
      iter rel_stag (100. *. rel_err) dt;
    flush stderr;
    iter < itermax);;

let spawn_init init_command command bin cout =
  match !bin, !cout with
    | Some ib, Some oc -> ib, oc
    | _ ->
       let ic, oc = Unix.open_process command in
       let ib = Scanf.Scanning.from_channel ic in
       bin := Some ib; cout := Some oc;
       init_command (ib, oc);
       ib, oc;;

let spawn command = spawn_init ignore command;;

let init_poisson (dec, num as sd) (ib, oc) =
  let g = Rdec.computational_rectangular_grid_of_subdomain sd in
  Rdec.print_computational_rectangular_grid oc g;
  Printf.fprintf oc "%d\n" jtermax;
  Printf.fprintf oc "%s\n" (resu ^ string_of_int num);
  flush oc;;

let computation_on_a_subdomain () =
  let bin = ref None and cout = ref None in
  (fun {num = num; bd = bd; iter = iter} ->
    let iter = iter + 1 in
    let ib, oc = spawn_init (init_poisson (dec, num)) "./poisson" bin cout in
    if output = "none" then Printf.fprintf oc "0\n" else
    if iter mod plot_step = 0 then Printf.fprintf oc "1\n" else
    Printf.fprintf oc "0\n";
    Rdec.print_boundary oc bd;
    flush oc;
    let erel, eabs = Scanf.bscanf ib " %f %f\n" (fun e1 e2 -> e1, e2) in
    let bd = Rdec.read_boundary ib (Rdec.size_of_boundary bd) in
    {num = num; bd = bd; iter = iter; err = erel, eabs});;

let proj () =
  (fun v ->
    let {nums = nums; bds = bds; iters = iters; errs = errs} = split v in
    combine
      {nums = nums;
       bds = Rdec.projection table bds;
       iters = iters;
       errs = errs});;

let make_gnuplot_command iter nsbdx nsbdy =
  let siter = string_of_int iter in
  let snxp1 = string_of_int (nsbdx + 1) in
  let snyp1 = string_of_int (nsbdy + 1) in
  let gnuplot = ref ("set isosample " ^ snxp1 ^ ", " ^ snyp1 ^ "\n") in
  if output <> "ps" then
    gnuplot := !gnuplot ^ "set title 'iter = " ^ siter ^ "'\n"
  else begin
    let zen_graphics =
      if gnuplot_version_number >= 4.0 then
        "unset xtics; unset ytics; unset ztics; unset border\n"
      else
        "set noxtics; set noytics; set noztics; set noborder\n" in
    gnuplot := !gnuplot ^ zen_graphics end;
  if output <> "window" then
    gnuplot := !gnuplot ^ "set term post eps color solid 22\n" ^
      "set output '" ^ resu ^ siter ^ ".eps'\n";
  gnuplot := !gnuplot ^ "splot [0:1][0:1][0:0.07]";
  let to_do n =
    let sn = string_of_int n in
    let resun = resu ^ sn and resubn = resu ^ "B" ^ sn in
    if Sys.file_exists resun then Sys.rename resun resubn else
    begin
      Printf.eprintf "%s not found!\n" resun; flush stderr;
    end;
    let style =
      if gnuplot_version_number >= 4.0 then "' notitle w p pt 7," else
      "' notitle w p pt 7 ps 0.5," in
    gnuplot := !gnuplot ^ " '" ^ resubn ^ style in
  Rdec.for_all_subdomains (nsbdx, nsbdy) to_do;
  if output <> "ps" then gnuplot := !gnuplot ^ " (x-x**2)*(y-y**2) w l\n"
  else gnuplot := !gnuplot ^ " (x-x**2)*(y-y**2) notitle w l lt 9 lw 4\n";
  !gnuplot;;

let plot () =
  let bin = ref None and cout = ref None in
  (fun v ->
    if output <> "none" then
      begin
        let ic, oc = spawn "gnuplot" bin cout in
        let iter = v.(0).iter in
        if iter mod plot_step = 0 then
          begin
            let gpl = make_gnuplot_command iter nsbdx nsbdy in
            Printf.fprintf oc "%s\n" gpl; flush oc;
          end;
      end;
    v);;

let exprbody () =
  loop (convergence,
        mapvector (seq computation_on_a_subdomain, nbproc) ||| seq proj |||
        seq plot);;
let expr = parfun exprbody;;

pardo (fun () ->
  P3lstream.iter print_result (expr (P3lstream.of_fun generate_input_stream));
  sleep a_while ());;
