(* $Id: gnuplot.mli,v 1.3 2005/08/05 13:38:28 fclement Exp $ *)

open Config;;

val gnuplot_version_number : float;;

val check_graphical_output : graphical_output -> unit;;

val make_gnuplot_command :
    graphical_output -> string -> int -> int -> int -> string;;
