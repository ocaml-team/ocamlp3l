(***********************************************************************)
(*                                                                     *)
(*                              OCamlP3l                               *)
(*                                                                     *)
(*                  projet Estime, INRIA-Rocquencourt                  *)
(*                                                                     *)
(*  Copyright 2003-2007 INRIA.                                         *)
(*  Institut National de Recherche en Informatique et en Automatique.  *)
(*  All rights reserved. This file is distributed only by permission.  *)
(*                                                                     *)
(*  Francois Clement <Francois.Clement@inria.fr>,                      *)
(*  Pierre Weis <Pierre.Weis@inria.fr>.                                *)
(*                                                                     *)
(***********************************************************************)

(* $Id: my_options.mli,v 1.4 2007/01/23 15:58:18 weis Exp $ *)

val aref : 'a -> ('a -> unit) * (unit -> 'a);;
(** Argument reference manager.
   [aref default] returns a couple of functions, the first one sets a variable
   with a default value [default], and the second one gets its value.
 *)

val uaref : string -> 'a -> ('a -> unit) * (unit -> 'a);;
(** Unique argument reference manager.
   [uaref vname default] returns a couple of functions, the first one sets, only
   once, a variable of name [vname], and the second one gets its value only if
   it has already been set. The default value [default] is used to set the type.
 *)

val optref : string -> ('a -> unit) * (unit -> 'a);;
(** Optional reference manager.
   [optref vname] returns a couple of functions, the first one sets a
   variable of name [vname], and the second one gets its value only if it
   has already been set.
 *)

val malref : 'a list -> ('a -> unit) * (unit -> 'a list);;
(** Multiple argument list reference manager.
   [malref default_list] returns a couple of functions, the first one adds an
   element to an initially empty list, and the second one gets the list, which
   is the default list [default-list] if no element has been added.
 *)
