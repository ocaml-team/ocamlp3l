(***********************************************************************)
(*                                                                     *)
(*                              OCamlP3l                               *)
(*                                                                     *)
(*                  projet Estime, INRIA-Rocquencourt                  *)
(*                                                                     *)
(*  Copyright 2003-2007 INRIA.                                         *)
(*  Institut National de Recherche en Informatique et en Automatique.  *)
(*  All rights reserved. This file is distributed only by permission.  *)
(*                                                                     *)
(*  Francois Clement <Francois.Clement@inria.fr>,                      *)
(*  Pierre Weis <Pierre.Weis@inria.fr>.                                *)
(*                                                                     *)
(***********************************************************************)

(* $Id: config.ml,v 1.6 2007/01/23 15:58:18 weis Exp $ *)

type graphical_output = Window | Postscript | No;;

let graphical_output_of_string = function
| "window" -> Window
| "ps" | "postscript" -> Postscript
| "no" | "none" -> No
| s -> failwith (Printf.sprintf "Config: Unknown graphical output %s" s)
and string_of_graphical_output = function
| Window -> "window"
| Postscript -> "postscript"
| No -> "none";;

type configuration = {
    rdec_config_filename : string;
    result_basename : string;
    graphical_output : graphical_output;
    outer_itermax : int;
    inner_itermax : int;
    display_step : int;
    a_while : int;
  };;

let configuration_of s1 s2 s3 i1 i2 i3 i4 =
  {rdec_config_filename = s1;
   result_basename = s2;
   graphical_output = graphical_output_of_string s3;
   outer_itermax = i1;
   inner_itermax = i2;
   display_step = i3;
   a_while = i4};;

let parse_configuration_file fname =
  let ib =
    try Scanf.Scanning.from_file fname
    with Sys_error s -> prerr_endline s; exit 6 in
  Scanf.bscanf ib "%s\n%s\n%s\n%d\n%d\n%d\n%d\n" configuration_of;;
