(***********************************************************************)
(*                                                                     *)
(*                              OCamlP3l                               *)
(*                                                                     *)
(*                  projet Estime, INRIA-Rocquencourt                  *)
(*                                                                     *)
(*  Copyright 2003-2007 INRIA.                                         *)
(*  Institut National de Recherche en Informatique et en Automatique.  *)
(*  All rights reserved. This file is distributed only by permission.  *)
(*                                                                     *)
(*  Francois Clement <Francois.Clement@inria.fr>,                      *)
(*  Pierre Weis <Pierre.Weis@inria.fr>.                                *)
(*                                                                     *)
(***********************************************************************)

(* $Id: config.mli,v 1.5 2007/01/23 15:58:18 weis Exp $ *)

(**
   Parsing a configuration file for the coupling program.
   
   A configuration file must contain (in that order):
   - the name of the file defining the regular decomposition
   - the basename for numerical files
   - the graphical output type
   - the maximum number of iterations for the domain decomposition algorithm
   - the maximum number of iterations for the local subproblems
   - the refreshment step for the ascii and graphical displays
   - the number of seconds to sleep before terminating
   
 *)

type graphical_output = Window | Postscript | No;;
(** The type of graphical outputs. *)

val graphical_output_of_string : string -> graphical_output;;
val string_of_graphical_output : graphical_output -> string;;
(** Conversion functions. *)

type configuration = {
    rdec_config_filename : string;
    result_basename : string;
    graphical_output : graphical_output;
    outer_itermax : int;
    inner_itermax : int;
    display_step : int;
    a_while : int;
  };;
(** The type of configurations. *)

val parse_configuration_file : string -> configuration;;
(** Parsing a configuration file. *)
