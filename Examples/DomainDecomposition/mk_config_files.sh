#!/bin/sh

SCRIPT=`echo $0 | awk -F/ '{print $NF}'`

EXIT=0
DEBUG=no
HELP=no
FORCE=no
EXIST=no

baseresname=RES
output=none
step=4
sleep=5

[ $# -eq 0 ] && HELP=yes && EXIT=2
while [ $# -ge 1 ]; do
    case $1 in
        -x) set -x;;
        -d) DEBUG=yes;;
        -h|-help|--help) HELP=yes;;
        -f) FORCE=yes;;
        -nx|-ny|-n) shift; N=$1;;
        -nsbdx|-nsbdy|-nsbd) shift; Nsbd=$1;;
        -baseresname|-basename|-bn) shift; baseresname=$1;;
        -output|-o) shift; output=$1;;
        -step) shift; step=$1;;
        -sleep) shift; sleep=$1;;
        -*) echo "Unknown option $1"; HELP=yes; EXIT=2;;
        *) echo "Unused argument $1, skipped...";;
    esac
    shift
done

[ -z "$N" -o -z "$Nsbd" ] && HELP=yes && EXIT=2

if [ $HELP = "yes" ]; then
    echo
    echo "Usage: $SCRIPT [<options>] -n <n> -nsbd <nsbd>"
    echo
    echo "Generate the files \"config-<size>\" and \"rdec-<size>.cfg\", where"
    echo "	<size> = <nsbd>x<nsbd>-<n>x<n>,"
    echo "to be used to configure the run of the coupling example and display"
    echo "the corresponding complexity."
    echo "If the configuration files already exist, they are simply displayed,"
    echo "unless the option -f is provided."
    echo ""
    echo "ARGUMENTS"
    echo "  -n <n>: set the number of grid points in each direction to <n>."
    echo "  -nsbd <n>: set the number of subdomains in each direction to <n>."
    echo
    echo "OPTIONS"
    echo "  -f: force the (re)generation of the configuration files."
    echo "  -bn <name>: set the basename for the numerical results to <name>."
    echo "              Default is \"$baseresname\"."
    echo "  -o <type>: set the output to <type>."
    echo "             Default is \"$output\". Valid entries are:"
    echo "                 \"window\" for graphical display (needs gnuplot)"
    echo "                 \"ps\" for postscript output (in directory \"res\")"
    echo "                 \"none\" for performance measurements."
    echo "  -step <n>: set the increment step for graphical outputs to <n>."
    echo "             Default is $step."
    echo "  -sleep <n>: set the amount of time in seconds during which the \"window\""
    echo "              remains displayed after the computation."
    echo "              Default is $sleep."
    echo
    exit $EXIT
fi

size=${Nsbd}x${Nsbd}-${N}x${N}
config=config-$size
rdec=rdec-${size}.cfg

n=$(($N / $Nsbd))
p=$(($n / 5))

nit=$(($n / 3 * 10))
Nit=$(((5 * $Nsbd - 1) * $Nsbd / 2 + 4))

CONFIG="\
$rdec
res/$baseresname
$output
$Nit
$nit
$step
$sleep"

RDEC="\
0. 0.
1. 1.
$N $N
$p $p
$Nsbd $Nsbd"

if [ $FORCE = "no" -a -f $config ]; then
    EXIST=yes
    echo
    echo "File $config already exists:"
    cat $config
else
    echo "$CONFIG" > $config
fi
if [ $FORCE = "no" -a -f $rdec ]; then
    EXIST=yes
    echo
    echo "File $rdec already exists:"
    cat $rdec
else
    echo "$RDEC" > $rdec
fi

simplify () {
    COMP=$1
    UNIT=$2
    while [ $COMP -gt 10000 -a $UNIT != "Pflop" ]; do
        COMP=$(($COMP / 1000))
        case "$UNIT" in
            flop) UNIT=kflop;;
            kflop) UNIT=Mflop;;
            Mflop) UNIT=Gflop;;
            Gflop) UNIT=Tflop;;
            Tflop) UNIT=Pflop;;
            *) COMP=$(($COMP * 1000));;
        esac
    done
    echo $COMP@$UNIT
}

comp=$((147 * $N * $N))
unit=flop
compunit=`simplify $comp $unit`
comp=`echo $compunit | awk -F@ '{print $1}'`
unit=`echo $compunit | awk -F@ '{print $2}'`
comp=$(($comp * $N * $Nsbd))
compunit=`simplify $comp $unit`
comp=`echo $compunit | awk -F@ '{print $1}'`
unit=`echo $compunit | awk -F@ '{print $2}'`
echo
echo "The approximated complexity, i.e. the number of floating point operations,"
echo "of the corresponding coupling example will be: $comp $unit."
echo

if [ $EXIST = "yes" ]; then
    echo "To regenerate already existing files, use the force option -f."
    echo
fi

exit $EXIT
