/***********************************************************************/
/*                                                                     */
/*                              OCamlP3l                               */
/*                                                                     */
/*                  projet Estime, INRIA-Rocquencourt                  */
/*                                                                     */
/*  Copyright 2003-2007 INRIA.                                         */
/*  Institut National de Recherche en Informatique et en Automatique.  */
/*  All rights reserved. This file is distributed only by permission.  */
/*                                                                     */
/*  Francois Clement <Francois.Clement@inria.fr>,                      */
/*  Pierre Weis <Pierre.Weis@inria.fr>.                                */
/*                                                                     */
/***********************************************************************/

/* $Id: poisson.c,v 1.8 2007/01/23 15:58:18 weis Exp $ */
/* 
   This program implements the Dirichlet-to-Dirichlet operator S_f for the
   solution of the 2D Poisson equation on a rectangular domain using an
   additive Schwarz algorithm, ie. domain decomposition with overlapping and
   Dirichlet transmission conditions.
   
                +--------------------------+
                |  o                    o  |
                |oooooooooooooooooooooooooo|
                |  o                    o  |
                |  o                    o  |
                |  o                    o  |       - + |  Gamma
                |  o                    o  |       o      ~Gamma
                |  o       Omega        o  |
                |  o                    o  |
                |  o                    o  |
                |  o                    o  |
                |  o                    o  |
                |  o                    o  |
                |oooooooooooooooooooooooooo|
                |  o                    o  |
                +--------------------------+
                
   Let Omega be a subdomain, Gamma its boundary, and ~Gamma the trace of its
   neighbors. Given right-hand sides f on Omega and g on Gamma, let u be the
   solution of the Poisson equation:
                     - Laplacian(u) = f on Omega
                                 u  = g on Gamma.
   For a given f, S_f is the operator:
                         g on Gamma -> ~g on ~Gamma
   where ~g is the trace of u on ~Gamma.
   
   The program first reads on its standard input a 2D regular computational
   grid, then it enters an infinite loop. For each Dirichlet condition g read
   on its standard input:
   - it computes using a simple SOR solver the solution u of the Poisson
     equation for a fixed right-hand side f,
   - it optionnally prints the solution u into a file,
   - it writes ~g on its standard output.
   
   Written from a C program by Arnaud Vodicka.
   
 */

#include <stdio.h>
#include <math.h>
/*#include <malloc.h>*/
#include <stdlib.h>

#define MAX(a,b) (a>=b ? a : b)
#define PI 3.1415926535589793

/* Choice of the right-hand side f */
float f(float x,float y) {return 2*(x*(1. - x) + y*(1. - y)); }

/* The corresponding solution */
float u(float x,float y) {return x*(1. - x)*y*(1. - y); }

float *vecalloc(int low,int high) {
  float *x;
  x = (float *)calloc((unsigned)(high-low)+1,sizeof(float));
  if (x==NULL) {fprintf(stderr,"unable to allocate memory"); exit(1); }
  return x-low; }

float **matalloc(int rowlow,int rowhigh,int collow,int colhigh) {
  int k;
  float **x;
  x = (float **)calloc((unsigned)(rowhigh-rowlow+1),sizeof(float *));
  if (x==NULL) {fprintf(stderr,"unable to allocate memory"); exit(1); }
  x -= rowlow;
  for (k=rowlow; k<=rowhigh; k++) {
    x[k] = (float *)calloc((unsigned)(colhigh-collow+1),sizeof(float));
    if (x[k]==NULL) {fprintf(stderr,"unable to allocate memory"); exit(1); }
    x[k] -= collow; }
  return x; }

main() {
  
  int i,j,nx,ny,px,py,flag,iter,itermax;
  float x0,y0,lx,ly,dx,dy,dx2,dy2,mdx2,mdy2,eta,w,alpha,beta;
  float rsum,eps,res,rel_stag,rel_err,max_uu;
  float *x,*y,*xp,*yp,**uold,**unew,**ff,**uu;
  char result[255];
  FILE *fp;
  
  /****************************************/
  /* READ THE COMPUTATIONAL GRID ON STDIN */
  /****************************************/
  
  scanf("%f",&x0); /* x of the lower left corner */
  scanf("%f",&y0); /* y of the lower left corner */
  scanf("%f",&lx); /* width of the domain */
  scanf("%f",&ly); /* height of the domain */
  scanf("%d",&nx); /* number of cells in the x-direction */
  scanf("%d",&ny); /* number of cells in the y-direction */
  scanf("%d",&px); /* overlapping in the x-direction */
  scanf("%d",&py); /* overlapping in the y-direction */
  scanf("%d",&itermax); /* max number of iterations */
  scanf("%256s",result); /* filename for the results */
  
  /******************/
  /* INITIALIZATION */
  /******************/
  
  dx = lx/nx; dy = ly/ny;
  dx2 = dx*dx; dy2 = dy*dy;
  mdx2 = 1./dx2; mdy2 = 1./dy2;
  
  eta = 1. - 2.*sin(PI/(2.*nx))*sin(PI/(2.*ny));
  w = 2./(1. + sqrt(1. - eta*eta));
  alpha = 0.5/(mdx2+mdy2)*w;
  beta = 1. - w;
  
  x = vecalloc(0,nx);
  for (i=0; i<=nx; i++) {x[i] = x0 + i*dx; }
  y = vecalloc(0,ny);
  for (j=0; j<=ny; j++) {y[j] = y0 + j*dy; }
  
  ff = matalloc(1,nx-1,1,ny-1);
  for (i=1; i<=nx-1; i++) {
    for (j=1; j<=ny-1; j++) {
      ff[i][j] = f(x[i],y[j]); } }
  
  uu = matalloc(0,nx,0,ny);
  for (i=0; i<=nx; i++) {
    for (j=0; j<=ny; j++) {
      uu[i][j] = u(x[i],y[j]); } }
  
  uold = matalloc(0,nx,0,ny);
  unew = matalloc(0,nx,0,ny);
  
  if (flag) {
    eps = 0.5;
      
    xp = vecalloc(0,nx);
    for (i=0; i<=nx; i++) {
      if (i<=2*px) {xp[i] = x[i] + (2.*px-i)/(2.*px)*eps*dx; }
      else if (i>=nx-2*px) {xp[i] = x[i] - (i-nx+2.*px)/(2.*px)*eps*dx; }
      else {xp[i] = x[i]; } }
    
    yp = vecalloc(0,ny);
    for (j=0; j<=ny; j++) {
      if (j<=2*py) {yp[j] = y[j] + (2.*py-j)/(2.*py)*eps*dy; }
      else if (j>=ny-2*py) {yp[j] = y[j] - (j-ny+2.*py)/(2.*py)*eps*dy; }
      else {yp[j] = y[j]; } }
  }
  
  /*****************/
  /* INFINITE LOOP */
  /*****************/
  
  while (1) {
    
    /*******************************************/
    /* READ THE DIRICHLET CONDITION g ON STDIN */
    /* Be careful, each corner is given twice! */
    /*******************************************/
    
    scanf("%d",&flag); /* flag = 1 to save the solution in a file */

    /* fprintf(stderr,"C:IN"); */
    for (j=0; j<=ny; j++) { /* East side */
      scanf("%f",&uold[nx][j]); unew[nx][j] = uold[nx][j];
      /* fprintf(stderr," %f",unew[nx][j]); */  }
    /* fprintf(stderr,"\nC:IN"); */
    for (i=0; i<=nx; i++) { /* North side */
      scanf("%f",&uold[i][ny]); unew[i][ny] = uold[i][ny];
      /* fprintf(stderr," %f",unew[i][ny]); */ }
    /* fprintf(stderr,"\nC:IN"); */
    for (i=0; i<=nx; i++) { /* South side */
      scanf("%f",&uold[i][0]); unew[i][0] = uold[i][0];
      /* fprintf(stderr," %f",unew[i][0]); */ }
    /* fprintf(stderr,"\nC:IN"); */
    for (j=0; j<=ny; j++) { /* West side */
      scanf("%f",&uold[0][j]); unew[0][j] = uold[0][j];
      /* fprintf(stderr," %f",unew[0][j]); */ }
    /* fprintf(stderr,"\nC:IN completed!\n"); */
    /* fflush(stderr); */
    
    /************************************/
    /* SOLVE {Laplacian(u) = f on Omega */
    /*       {          u  = g on Gamma */
    /************************************/
    
    iter = 0;
    
    /* Complexity = 9*(nx-1)*(ny-1)*itermax flop */
    do { 
      for (i=1; i<=nx-1; i++) {
        for(j=1; j<=ny-1; j++) {
          uold[i][j] = unew[i][j]; } }
      
      for (i=1; i<=nx-1; i++) {
        for (j=1; j<=ny-1; j++) {
          unew[i][j] = alpha*(mdx2*(uold[i+1][j] + unew[i-1][j]) \
                            + mdy2*(uold[i][j+1] + unew[i][j-1]) \
                              + ff[i][j])
                     + beta*uold[i][j]; } }
      
      iter++;
      
    } while (iter<itermax);
    
    /* Compute the relative stagnation and the relative error */
    rel_stag = 0.; rel_err = 0.; max_uu = 0.;
    
    for (i=0; i<=nx; i++) {
      for (j=0; j<=ny; j++) {
        if (fabs(uold[i][j])>0) {
          rel_stag = MAX(rel_stag, \
                         fabs(unew[i][j] - uold[i][j])/fabs(uold[i][j])); }
        max_uu = MAX(max_uu,fabs(uu[i][j]));
        rel_err = MAX(rel_err,fabs(uu[i][j] - uold[i][j])); } }
    rel_err /= max_uu;
    
    /**********************************/
    /* PRINT THE SOLUTION u IN A FILE */
    /**********************************/      
    
    if (flag) {
      fp = fopen(result,"w");  
      for (i=0; i<=nx; i++) {
        for (j=0; j<=ny; j++) {
          fprintf(fp,"%f %f %f\n",xp[i],yp[j],unew[i][j]); } }
      fclose(fp);}
    
    /**********************************/
    /* PRINT THE TRACE OF u ON STDOUT */
    /**********************************/
    
    /* fprintf(stderr,"C:OUT"); */
    printf("%f %f\n",rel_stag,rel_err);
    /* fprintf(stderr," rel_stag=%f rel_err=%f\n",rel_stag,rel_err); */
    
    /* fprintf(stderr,"\nC:OUT"); */
    for (j=0; j<=ny; j++) { /* East side */
      printf("%f\n",unew[nx-2*px][j]);
      /* fprintf(stderr," %f",unew[nx-2*px][j]); */ }
    /* fprintf(stderr,"\nC:OUT"); */
    for (i=0; i<=nx; i++) { /* North side */
      printf("%f\n",unew[i][ny-2*py]);
      /* fprintf(stderr," %f",unew[i][ny-2*py]); */ }
    /* fprintf(stderr,"\nC:OUT"); */
    for (i=0; i<=nx; i++) { /* South side */
      printf("%f\n",unew[i][2*py]); 
      /* fprintf(stderr," %f",unew[i][2*py]); */ }
    /* fprintf(stderr,"\nC:OUT"); */
    for (j=0; j<=ny; j++) { /* West side */
      printf("%f\n",unew[2*px][j]); 
      /* fprintf(stderr," %f",unew[2*px][j]); */ }
    printf("completed\n"); /* We need this tag to end the message! */
    fflush(stdout);
    /* fprintf(stderr,"\nC:OUT completed!\n"); */
    /* fflush(stderr); */
    
  } /* end while */
  
  return 0;
  
} /* end main */
