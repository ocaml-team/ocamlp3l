(***********************************************************************)
(*                                                                     *)
(*                              OCamlP3l                               *)
(*                                                                     *)
(*                  projet Estime, INRIA-Rocquencourt                  *)
(*                                                                     *)
(*  Copyright 2003-2007 INRIA.                                         *)
(*  Institut National de Recherche en Informatique et en Automatique.  *)
(*  All rights reserved. This file is distributed only by permission.  *)
(*                                                                     *)
(*  Francois Clement <Francois.Clement@inria.fr>,                      *)
(*  Pierre Weis <Pierre.Weis@inria.fr>.                                *)
(*                                                                     *)
(***********************************************************************)

(* $Id: my_unix.ml,v 1.3 2007/01/23 15:58:18 weis Exp $ *)

let spawn_init init_command command bin cout =
  match !bin, !cout with
    | Some ib, Some oc -> ib, oc
    | _ ->
       let ic, oc = Unix.open_process command in
       let ib = Scanf.Scanning.from_channel ic in
       bin := Some ib; cout := Some oc;
       init_command (ib, oc);
       ib, oc;;

let spawn command = spawn_init ignore command;;

(* Connected processes stuff. *)
let connect_stdio proc (fdin1, fdout1) (fdin2, fdout2) =
  Unix.close fdout1;
  Unix.close fdin2;
  Unix.dup2 fdin1 Unix.stdin;
  Unix.close fdin1;
  Unix.dup2 fdout2 Unix.stdout;
  Unix.close fdout2;
  proc ();;

let connect_bi_directional proc1 proc2 =
  let p1 = Unix.pipe () in
  let p2 = Unix.pipe () in
  match Unix.fork () with
  | 0 -> connect_stdio proc2 p1 p2
  | _ -> connect_stdio proc1 p2 p1;;

let launch prog () = Unix.execv prog [| prog |];;

let launch_connected_processes prog1 prog2 =
  connect_bi_directional (launch prog1) (launch prog2);;
