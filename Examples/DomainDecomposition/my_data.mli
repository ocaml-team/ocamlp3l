(***********************************************************************)
(*                                                                     *)
(*                              OCamlP3l                               *)
(*                                                                     *)
(*                  projet Estime, INRIA-Rocquencourt                  *)
(*                                                                     *)
(*  Copyright 2003-2007 INRIA.                                         *)
(*  Institut National de Recherche en Informatique et en Automatique.  *)
(*  All rights reserved. This file is distributed only by permission.  *)
(*                                                                     *)
(*  Francois Clement <Francois.Clement@inria.fr>,                      *)
(*  Pierre Weis <Pierre.Weis@inria.fr>.                                *)
(*                                                                     *)
(***********************************************************************)

(* $Id: my_data.mli,v 1.6 2007/01/23 15:58:18 weis Exp $ *)

open Config;;
open Rdec;;

type subdomain =
    {config_filename : string;
     config : configuration;
     rdec : regular_decomposition;
     num : int;
     iter : int;
     bd : boundary;
     err : float * float};;
(** The type of the components of the array data stream items. In other
   words, the items of the data stream have type [subdomain] array.
   The fields [config_filename], [config] and [rdec] are fixed and unique;
   [num] is fixed and multiple; [iter] is variable and unique;
   [bd] and [errs] are variable and multiple. *)

val get_config_filename : subdomain array -> string;;
val get_config : subdomain array -> configuration;;
val get_rdec : subdomain array -> regular_decomposition;;
val get_nums : subdomain array -> int array;;
val get_iter : subdomain array -> int;;
val get_bds : subdomain array -> boundary array;;
val get_errs : subdomain array -> (float * float) array;;
(** Extraction functions. *)

val set_iter : subdomain array -> int -> subdomain array;;
val set_bds : subdomain array -> boundary array -> subdomain array;;
val set_errs : subdomain array -> (float * float) array -> subdomain array;;
(** Insertion function. *)
