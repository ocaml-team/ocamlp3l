(***********************************************************************)
(*                                                                     *)
(*                              OCamlP3l                               *)
(*                                                                     *)
(*                  projet Estime, INRIA-Rocquencourt                  *)
(*                                                                     *)
(*  Copyright 2003-2007 INRIA.                                         *)
(*  Institut National de Recherche en Informatique et en Automatique.  *)
(*  All rights reserved. This file is distributed only by permission.  *)
(*                                                                     *)
(*  Francois Clement <Francois.Clement@inria.fr>,                      *)
(*  Pierre Weis <Pierre.Weis@inria.fr>.                                *)
(*                                                                     *)
(***********************************************************************)

(* $Id: my_options.ml,v 1.5 2007/01/23 15:58:18 weis Exp $ *)

let aref default =
  let r = ref default in
  (fun x -> r := x),
  (fun () -> !r);;

let uaref s default =
  let set = ref false in
  let r = ref default in
  (fun x ->
     if !set then
       begin prerr_endline ("Attempt to set twice uaref " ^ s); exit 5 end
     else r := x;
     set := true),
  (fun () ->
    if not !set then
      begin prerr_endline ("Attempt to get undefined uaref " ^ s); exit 5 end
    else !r);;

let optref s =
  let r = ref None in
  (fun x -> r := Some x),
  (fun () ->
    match !r with
    | None -> prerr_endline ("Undefined optional ref variable " ^ s); exit 5
    | Some x -> x);;

let malref default =
  let set = ref false in
  let r = ref default in
  (fun x -> if not !set then r := []; r := x :: !r),
  (fun () -> !r);;

