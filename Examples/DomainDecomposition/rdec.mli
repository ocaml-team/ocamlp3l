(***********************************************************************)
(*                                                                     *)
(*                              OCamlP3l                               *)
(*                                                                     *)
(*                  projet Estime, INRIA-Rocquencourt                  *)
(*                                                                     *)
(*  Copyright 2003-2007 INRIA.                                         *)
(*  Institut National de Recherche en Informatique et en Automatique.  *)
(*  All rights reserved. This file is distributed only by permission.  *)
(*                                                                     *)
(*  Francois Clement <Francois.Clement@inria.fr>,                      *)
(*  Pierre Weis <Pierre.Weis@inria.fr>.                                *)
(*                                                                     *)
(***********************************************************************)

(* $Id: rdec.mli,v 1.8 2007/01/23 15:58:18 weis Exp $ *)

(*****************************************************************)
(* Module for the representation of 2D regular rectangular grids *)
(* with regular decomposition capabilities.                      *)
(* Overlapping of the decomposition is parameterized.            *)
(*****************************************************************)

(****************************************************************************)
(* A config file contains a regular_decomposition record, ie:               *)
(*   a computational_rectangular_grid record:                               *)
(*     a rectangular_grid record:                                           *)
(*       a rectangle record:                                                *)
(*         x0 and y0, coordinates of the lower-left corner of the rectangle *)
(*         lx and ly, length of the rectangle in both directions            *)
(*       nx and ny, number of intervals in both directions                  *)
(*     px and py, half the overlapping of the subdomains in both directions *)
(*   nsbdx and nsbdy, number of subdomains in both directions.              *)
(*                                                                          *)
(* A valid regular_decomposition must satisfy:                              *)
(*   lx > 0. and ly > 0.                                                    *)
(*   nx > 0 and ny > 0                                                      *)
(*   px > 0 and py > 0                                                      *)
(*   px <= nx / nsbdx and py <= ny / nsbdy                                  *)
(*   nsbdx > 0 and nsbdy > 0                                                *)
(*   nsbdx <= nx and nsbdy <= ny                                            *)
(*   nx mod nsbdx = 0 and ny mod nsbdy = 0                                  *)
(****************************************************************************)

type rectangle and rectangular_grid
and computational_rectangular_grid and regular_decomposition
      
val rectangle_of_rectangular_grid : rectangular_grid -> rectangle
val rectangle_of_computational_rectangular_grid :
    computational_rectangular_grid -> rectangle
val rectangle_of_regular_decomposition : regular_decomposition -> rectangle
    
val of_rectangle : rectangle -> float * float * float * float
val number_of_mesh_of_rectangular_grid : rectangular_grid -> int * int
val rectangular_grid_of_computational_rectangular_grid :
    computational_rectangular_grid -> rectangular_grid
val overlapping_of_computational_rectangular_grid :
    computational_rectangular_grid -> int * int
val computational_rectangular_grid_of_regular_decomposition :
    regular_decomposition -> computational_rectangular_grid
val number_of_subdomains_of_regular_decomposition :
    regular_decomposition -> int * int
  
val print_computational_rectangular_grid :
    out_channel -> computational_rectangular_grid -> unit
        
val parse_config_file : string -> regular_decomposition
exception Wrong_Input of string
val check_regular_decomposition : regular_decomposition -> unit
        
exception Out_of_range of int
val two_indices_numbering : int -> int -> int -> int * int

type subdomain = regular_decomposition * int
val rectangular_grid_of_subdomain :
    subdomain -> rectangular_grid
val computational_rectangular_grid_of_subdomain :
    subdomain -> computational_rectangular_grid
  
type connectivity_table
val connectivity_table_of_regular_decomposition :
    regular_decomposition -> connectivity_table
val east_neighbor : connectivity_table -> int -> int
val north_neighbor : connectivity_table -> int -> int
val south_neighbor : connectivity_table -> int -> int
val west_neighbor : connectivity_table -> int -> int
  
type boundary
val print_boundary : out_channel -> boundary -> unit
val read_boundary :
(*  (*Scanf.Scanning.scanbuf*) in_channel-> int * int -> boundary*)
     Scanf.Scanning.scanbuf -> int * int -> boundary
val size_of_boundary : boundary -> int * int

type interface = boundary array
val make_zero_interface : regular_decomposition -> interface
val projection : connectivity_table -> interface -> interface

val for_all_subdomains : int * int -> (int -> 'a) -> unit
