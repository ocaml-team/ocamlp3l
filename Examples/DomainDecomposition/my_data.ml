(***********************************************************************)
(*                                                                     *)
(*                              OCamlP3l                               *)
(*                                                                     *)
(*                  projet Estime, INRIA-Rocquencourt                  *)
(*                                                                     *)
(*  Copyright 2003-2007 INRIA.                                         *)
(*  Institut National de Recherche en Informatique et en Automatique.  *)
(*  All rights reserved. This file is distributed only by permission.  *)
(*                                                                     *)
(*  Francois Clement <Francois.Clement@inria.fr>,                      *)
(*  Pierre Weis <Pierre.Weis@inria.fr>.                                *)
(*                                                                     *)
(***********************************************************************)

(* $Id: my_data.ml,v 1.6 2007/01/23 15:58:18 weis Exp $ *)

open Config;;
open Rdec;;

type subdomain =
    {config_filename : string;
     config : configuration;
     rdec : regular_decomposition;
     num : int;
     iter : int;
     bd : boundary;
     err : float * float};;

let zero_length_error () = invalid_arg "My_data: empty stream item"
and constant_field_error s =
  failwith (Printf.sprintf "My_data: field %s should be constant" s)
and same_length_error () = invalid_arg "My_data: inconsistent lengths";;

(* Extraction functions *)

let get_unique get s v =
  let n = Array.length v in
  if n = 0 then zero_length_error () else
  let value = get v.(0) in
  for i = 1 to n - 1 do if value <> get v.(i) then constant_field_error s done;
  value
and get_all get s v =
  let n = Array.length v in
  if n = 0 then zero_length_error () else
  let values = Array.make n (get v.(0)) in
  for i = 1 to n - 1 do values.(i) <- get v.(i) done;
  values;;

let get_config_filename v =
  get_unique (fun subd -> subd.config_filename) "config_filename" v
and get_config v = get_unique (fun subd -> subd.config) "config" v
and get_rdec v = get_unique (fun subd -> subd.rdec) "rdec" v
and get_nums v = get_all (fun subd -> subd.num) "nums" v
and get_iter v = get_unique (fun subd -> subd.iter) "iter" v
and get_bds v = get_all (fun subd -> subd.bd) "bds" v
and get_errs v = get_all (fun subd -> subd.err) "errs" v;;

(* Insertion functions *)

let set_unique set v x =
  let n = Array.length v in
  if n = 0 then zero_length_error () else
  for i = 1 to n - 1 do set v.(i) x done;
  v
and set_all set v xs =
  let n = Array.length v and nx = Array.length xs in
  if n = 0 || nx = 0 then zero_length_error () else
  if n <> nx then same_length_error () else
  for i = 1 to n - 1 do set v.(i) xs.(i) done;
  v;;

let set_iter v iter = set_unique (fun subd x -> {subd with iter = x}) v iter
and set_bds v bds = set_all (fun subd x -> {subd with bd = x}) v bds
and set_errs v errs = set_all (fun subd x -> {subd with err = x}) v errs;;
