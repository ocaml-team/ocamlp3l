(* simple pipeline :
   use to investigate performance tuning of pipeline ocamlp3l programs *)

(* active passing n microseconds *)
let microwait n = 
  let t = Unix.gettimeofday() in
  let now = ref (Unix.gettimeofday()) in
  while(!now < (t +. (0.001 *. (float n)))) do
    now := Unix.gettimeofday()
  done;;

let seqfunction outdatasize comptime = 
  (function _ -> 
    let out = Array.create (outdatasize) 87 in
    microwait comptime ; out);;

let stream nitems size = 
  let n = ref nitems in
  (function _ -> 
    let out = Array.create (size) 87 in
    if(!n = 0) then raise End_of_file
    else
      (n:= !n - 1; out));; 

(* change this to change the stream lenght and initial data lenght *)
let generate_stream = stream 20 400;;

(* change this to change the computational and communication weight of
   the pipeline stages *)

let stage1 = (* 100*100 ints, 100 microseconds to compute *)
  seqfunction (100*100) 100;;

let stage2 = 
  seqfunction (100*100) 500;;

let stage3 = 
  seqfunction (100) 150;;

let thepipe = seq(stage1) ||| seq(stage2) ||| seq(stage3);;

(* the timing stuff *)
let now = ref (Unix.gettimeofday());;
let stop_init _ = 
  now := (Unix.gettimeofday());;
let stop_end _ = 
  print_string "Elapsed time on stop node is ";
  print_float ((Unix.gettimeofday()) -. !now );
  print_newline();;
  
let do_nothing _ = ();;

let progr ()= 
  startstop
    (generate_stream,do_nothing)
    (do_nothing,stop_init,stop_end)
    thepipe
in pardo progr;;
    
