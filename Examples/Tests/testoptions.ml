(* references initialised when reading the command line options ... *)
(* WARNING: if we use these references on the parallel network, the
   value passed to the root node supercedes all the values present
   on the non root nodes.
   Here, for exemple, if we call

       testoptions.par &
       testoptions.par -number 123 -p3lroot localhost

   we get

       [126] false 123

   and not 
  
       [3] false 123

 *)

let flag = Options.flag false "-flag" "Test flag";;
let nproc =
  let n = ref 0 in
  Options.add
    "-number"
    (Arg.Int (fun num -> n := num))
    "Set a number parameter ";
  n;;

Printf.eprintf "nproc = %d\n" !nproc; flush stderr;;
let sk = parfun (fun () -> farm (seq (fun _ -> fun x -> x + !nproc), 1));;
Printf.eprintf "nproc = %d\n" !nproc; flush stderr;;

pardo
  (fun () ->
    Printf.eprintf "nproc = %d\n" !nproc; flush stderr;
    let [x] = P3lstream.to_list (sk (P3lstream.of_list [3])) in
    Printf.printf "Done!\n";
    Printf.printf "[%d] %s %d\n" x
      (if !flag then "true" else "false") !nproc);;
