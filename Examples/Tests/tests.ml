(***********************************************************************)
(*                                                                     *)
(*                   Objective Caml P3L system                         *)
(*                                                                     *)
(* (C) 2003 by                                                         *)
(*             Roberto Di Cosmo (roberto@dicosmo.org)                  *)
(*                                                                     *)
(* This program is free software; you can redistribute it and/or       *)
(* modify it under the terms of the GNU Library General Public License *) 
(* as published by the Free Software Foundation; either version 2      *) 
(* of the License, or (at your option) any later version.              *) 
(*                                                                     *) 
(* This program is distributed in the hope that it will be useful,     *) 
(* but WITHOUT ANY WARRANTY; without even the implied warranty of      *) 
(* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       *) 
(* GNU Library General Public License for more details.                *) 
(*                                                                     *)
(***********************************************************************)

(* $Id: tests.ml,v 1.3 2004/09/16 23:00:20 dicosmo Exp $ *)

(* tests for seq *)

(* program computing a function over a stream of floats ... 
   by using a farm (very simple!) *)

let degree = ref 1;;

let farm_worker _ = fun x -> x +. x;;

let generate_input_stream =
  let x = ref 0.0 in
  (function () -> 
    begin
      x := !x +. 1.0;
      if(!x < 10.0) then !x else raise End_of_file
    end);;

let print_result x =
  print_float x; print_newline();;

(* parfun turns a skeleton expression into a standard Ocaml function *)

(* Warning: in the current implementation, the following dynamic        *)
(* skeleton generation trick does not work in sequential mode we should *)
(* collect parfuns and instantiate them at pardo time only!             *)

let rec genpipe =
  function
      0 -> seq(farm_worker)
    | n -> seq(farm_worker)|||(genpipe (n-1));;

let pipeval =
  let f = farm_worker () in
  let rec aux =
    function
	0 -> f
      | n -> fun x -> f (aux (n-1) x)
  in aux;;

let seqpipe =
  parfun (fun () ->  genpipe !degree);;

let staticseqpipe =
  parfun (fun () -> seq(farm_worker)|||seq(farm_worker));;

(* tests for loop and mapvector *)

let array_generate_input_stream =
  let x = ref 0 in
  (function () -> 
    begin
      x := !x + 1;
      if(!x < 2) then [|0;1|] else raise End_of_file
    end);;

let map_worker _ = fun v -> v+1;;

let loopmapbody () = (loop((fun v -> let w=v.(0) in w<10), (mapvector(seq(map_worker),3))));;
let loopmap = parfun loopmapbody;;
(* let loopmap =
    parfun (fun () -> (loop((fun v -> let w=v.(0) in w<10), (mapvector(seq(map_worker),3)))));;
*)

(* tests for farm *)

let farm_worker _ = fun v -> v +. 1.0;;

let loopfarm =
    parfun
    (fun () ->
      loop((fun v -> v < 10.0),
	   (farm(farm(seq(farm_worker),4),2))));;

(*
let loopid = 
	parfun
	(fun () ->
		loop ((fun _ -> false), seq(fun () -> fun x ->x+x)));;
		*)

(* entry points *)
let ic = open_in "config";;
let _ = Scanf.fscanf ic "%d" (fun n -> degree:=n);close_in ic;;


let fail s = Printf.printf "FAILED\tTest %s\n" s;;
let success s = Printf.printf "PASSED\tTest %s\n" s;;

let tst l l' p s = (if (l=l') then success else (List.iter p l;print_newline();List.iter p l';fail)) s;;

pardo(
  fun () ->

    let is = P3lstream.of_fun generate_input_stream in

    (* test a static length pipe of seq *)
    let l = P3lstream.to_list (staticseqpipe is) in
    tst l (List.map (fun n -> n*. 4.) (P3lstream.to_list is)) print_result "static pipe of 2 seq";

    (* test a variable length pipe of seq *)
    let l = P3lstream.to_list (seqpipe is)
    and result = let f = pipeval !degree in List.map f (P3lstream.to_list is)
    in tst l result print_result "dynamically created pipe of seq";

    (* test a mapvector in a loop *)
    let s'' = loopmap (P3lstream.of_fun array_generate_input_stream) in
    let [a] = P3lstream.to_list s''
    in (if (a.(0) = 10) then success else (Printf.printf "%d =/= 10\n" a.(0);fail)) "mapvector in a loop";

    (* test a farm in a loop *)
    let l = P3lstream.to_list (loopfarm is)
    in tst l (List.map (fun _ -> 10.) l) (fun x -> Printf.printf "%f\n" x) "farm in a loop"

  );;

