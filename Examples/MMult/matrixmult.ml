(* Matrix multiplication using generalized map *)

let nothing _ = ();;

let generate_input_stream = let b =ref true in
function () -> 
  if !b then 
    begin
      b:=false;
      (Array.init 12 (fun _ -> Array.create 12 1.0),
       Array.init 12 (fun _ -> Array.create 12 3.0))
    end
  else raise End_of_file;;

let print_result m = 
  Array.iter (fun v -> 
    Array.iter (fun f -> print_float f; print_string " ") 
      v; print_newline()) 
    m;;

let inner_product (r,c) = 
  let z=ref 0. in 
  for i = 0 to (11) do 
    z:= !z +. r.(i) *. c.(i) 
  done; !z;;

let program ()= 
  startstop
    (generate_input_stream, nothing)
    (print_result,nothing,nothing)
    (P3LMap in (a[12][12], b[12][12]) out (c[12][12],init=0.0)
       nworkers [*i=2;*j=3]
       body in (a[*i][], b[][*j]) out (c[*i][*j])
         seq(inner_product)
       end)
in
  pardo program
;;
