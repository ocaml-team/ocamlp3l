(* program computing a function over a stream of floats ... 
   by using a farm (very simple!) 
   BUT the funcion is computed by an external command 
   which is spawned and connected via stdin / stdout to the farm worker in the seq skeleton

   IMPORTANT: 
      - remember to fflush data output in the external command source, or you will block!
      - remember not to rely on global references/arrays
*)

(* ocamlc -custom unix.cma seqp3l.cmo -o coupleur.seq coupleur.ml -cclib -lunix *)

(*open Seqp3l*)

type mesh          = string;;
type coupleur_data = mesh array;;



let encode oc x = Printf.fprintf oc "%d" x; Pervasives.flush oc;;
let decode ic   = Scanf.fscanf ic "%f" (fun x -> x);;

let encode_string oc x y = Printf.fprintf oc "%s %s\n" x y ; Pervasives.flush oc;;
let decode_string ic     = Scanf.fscanf ic "%s" (fun x -> x);;

let f n   = 2*n+1 ;;
let f1 n  = n+1 ;;
let f2 n  = n  ;;

(* These values depend on the number and structure of subdomains    *)
(* and must be read from a configuration file                       *)

let n    = [| 50;50;50 |]  ;;
let dim  =  Array.map f n  ;;
let c1   =  Array.map f1 n ;;
let c2   =  Array.map f2 n ;;

(* the incidence "matrix" for subdomains                *)
(* if we have n subdomains, it is a vector of n lists,  *)
(* each list containing the subdomains sharing borders, *)
(* and the range of the elements shared                 *)
(* again, this should be read from a configuration file *)

let incidence = 
  [|
    [2,(0,c1.(1));1,(1,c2.(1))];
    [0,(c1.(1),c2.(1));2,(c2.(1),c1.(1))];
    [0,(0,c1.(1));1,(c2.(1),c1.(1))]
  |];;


let v      = Array.create dim.(0) 0. ;;
let m      = Array.create_matrix 3 dim.(0) 0.0 ;; (* 3:numprog *)

(* 
   NO ...  keep separate data... separated!!!!! m is really -n- vectors, where -n- is numprog, and each
   subdomain keeps its one!!!! 
 *)

let calcul_sous_domaine =
 let spawned = ref false
     and cin = ref None
     and cout = ref None in
  (fun ((num,x,(b:float)),mp,dim) -> 
    if not !spawned then
       begin
       let (ic,oc) = Unix.open_process "./poisson" in
       cin := Some ic; cout := Some oc; spawned:=true;
       end;
       let Some ic, Some oc = (!cin , !cout) in
       let min = open_in x in
          Scanf.fscanf min "%s %s" (fun x y -> encode_string oc x y );
          Printf.fprintf oc "%d\n" num ;
	  for i=0 to (dim-1) do
          Printf.fprintf oc "%f\n" mp.(i) ;    
          done ;
	  Pervasives.flush oc ;
      	  for i=0 to (dim-1) do Scanf.fscanf ic "%f" (fun x -> mp.(i) <- x ) done;
      	  close_in min;
	  ( num,x,m.(1).(0)),mp,dim) ;;     

let  p x = x ;;
 
(* This code is generic and performs the projection of arbitrary subdomain borders, using the incidence matrix *)

let nth v n = let (_,m,_) = v.(n) in m;;
 
let proj_borders n v = 
  List.fold_left Array.append [| |] (List.map (fun (m,(b,len)) -> Array.sub (nth v m) b len) incidence.(n))
;;

let projection v = 
    Array.map (fun (((num,_,_v) as x),_,dim) -> ((x,proj_borders (num-1) v,dim))) v;;  


let bicgstab   = 
  let iter = ref 0 in                
    fun v ->
    iter := !iter + 1 ; print_string "iter = " ; print_int !iter ;print_newline();
    (v, (!iter < 50));;

 
 let generate_input_stream =
  let x = ref 0 in
  (function () -> 
    begin
      x := !x + 1;
      if(!x < 2) then ([| ((1,"mesh1",0.0),m.(0),dim.(0)) ; ((2,"mesh2",0.0),m.(1),dim.(1)); ((3,"mesh3",0.0),m.(2),dim.(2)) |],false) else raise P3lstream.End_of_stream
    end);;


let print_result (d,_) =
  Array.iteri (fun i ((num,x,b),m,mp) -> Printf.printf "Domain %d with file %s has computed %f\n" num x b) d;;
			
let plot = 
 let spawned = ref false
     and cin = ref None
     and cout = ref None in
   fun v -> 	  
	    if not !spawned then
       begin
       let (ic,oc) = Unix.open_process "gnuplot" in
       cin := Some ic; cout := Some oc; spawned:=true;
       end;
    let Some ic, Some oc = (!cin , !cout) in
          Sys.rename "RESU1" "RESUL1"; Sys.rename "RESU2" "RESUL2"; Sys.rename "RESU3" "RESUL3";
          Printf.fprintf oc "splot 'RESUL1' w p ,'RESUL2' w p ,'RESUL3' w p , 'solution' w l \n";
	  Pervasives.flush oc ;
	  v;;     

let solver = 
  parfun (fun () -> 
    (loop ((fun (v,continue) -> continue ),
           seq(fun _ -> fun (v,_) -> v) 
         ||| mapvector(seq(fun _ -> calcul_sous_domaine),3)
         ||| seq(fun _ -> projection)
         ||| seq(fun _ -> bicgstab)
         ||| seq(fun _ -> plot)
          ) )
	 )
;;
			  				  
pardo( fun () ->
  P3lstream.iter print_result (solver (P3lstream.of_fun generate_input_stream)));;








