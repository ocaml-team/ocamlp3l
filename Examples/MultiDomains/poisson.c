/* Fast Poisson solver using SOR on a domain omega 
     Laplacien(u) = f on Omega
               u  = 0  on bord Omega */



/*
           ----------------------/----------------------
          |                      /                      |
          |                      /                      |
          |                      /                      |<< Gamma2
          |                      /                      |
          |                      / << Sigma21           |
          |      Omega1          /                      |
          |                      /                      |
          |                      /                      |   
  Gamma1>>|                      /     Omega2           |
          |                      /                      |
          |                      /                      |
          |                      /                      |
	  ///////////////////////\\\\\\\\\\\\\\\\\\\\\\\\
          |                                             |         
          |                                             |
          |                                             |
          |                                             |
          |                                             |
          |                    Omega3                   |
          |                                             | <<Gamma3
          |                                             |
          |                                             |
          |                                             |
           --------------------------------------------- 
   
*/                          



#include <math.h>
#include <malloc.h>
#include <stdio.h>

#define MAX(a,b) ((a)>=(b) ? (a) : (b) )
#define PI 3.1415926535589793

#define SIZE 128

double maxnum (int m, double *x)
{
  int k;
  double z;
 
  z = MAX(fabs(x[1]),fabs(x[2]));
  for (k=3;k<=m;k++)
    z=MAX(fabs(x[k]),z);
  return z ;
}


double f(double x , double y)
{
  return  2*(y*y - y + x*x - x ) ;
  
  
  /*-sin(PI*x)*(2.0+PI*PI*y*(1.0-y))  ; */
  
  
  
  /* - exp(1- 1/x) */   
  
  
   /*(-sin(PI*x)*(2.0+PI*PI*y*(1.0-y)))   ; */       
  
  /* y*y*y; */    
  
  /*-x*y*(1-x)*(1-y) (-sin(PI*x)*(2.0+PI*PI*y*(1.0-y))) */  ; 
}

double *vecalloc(int low, int high)
{
  double *x;

  x=(double *)calloc((unsigned)(high-low)+1,sizeof(double));
  if (x==NULL) {
    fprintf(stderr,"unable to allocate memory");
    exit(1); }
  return (x-low);

}


double **matalloc(int rowlow, int rowhigh,int collow,int colhigh)
{
  int k;
  double **x;

  x=(double **)calloc((unsigned)(rowhigh-rowlow+1),sizeof(double *));
  if (x==NULL) {
    fprintf(stderr,"unable to allocate memory");
    exit(1); }
  x -= rowlow;

 for(k=rowlow;k<=rowhigh;k++){
    x[k] = (double *)calloc((unsigned)(colhigh-collow+1),sizeof(double ));
  if (x[k]==NULL) {
    fprintf(stderr,"unable to allocate memory");
    exit(1);}
    x[k] -= collow ;}
    return x;
}


main ()

{
 
  
  int      k,j,m,N,M;
  double   a,b,eta,w,dx,dy,rsum;
  double   f(double x , double y );
  double   maxnum ( int m , double * x) ;
  double   *e, **uold, **unew;
  FILE     *fp;
  
  int      j1,j2,k1,k2,j3,k3;
  char     str[SIZE];
  double   x0 , y0 ;
  float    var_muette ,var_muette33; /*double  or float?*/
  int      var_muette2 ,var_muette11,var_muette22;
  

  FILE     *DONNEES ;
  char     donnees[7]; 
  char     resultat[7];

  double   vara , temp ;
   
  int      i,NumDomain ; 
  
  
   
   

   while(1){
   
  
 
   scanf("%8s %8s" ,donnees,resultat);
   scanf("%d",&NumDomain) ;
  


   /*fgets(donnees,SIZE,stdin);*/
   /*printf("%s\n",donnees);*/
   /* printf("%s\n",resultat) ;*/
   /*printf("%d\n",111) ;*/
   /*  scanf("%8s",resultat); printf("%s\n",resultat) ;*/
   /*fgets(resultat,SIZE,stdin); printf("%s\n",resultat) ; */
   
   
    
  
 
   
/*************************************************/   
/*         LECTURE FICHIER                        */
/*************************************************/

      
      DONNEES = fopen(donnees,"r") ;
      

      fscanf(DONNEES,"%f",&var_muette);
      x0 = var_muette ;
      fscanf(DONNEES,"%f",&var_muette);
      y0 = var_muette ;
      fscanf(DONNEES,"%f",&var_muette);
      a  = var_muette ;
      fscanf(DONNEES,"%f",&var_muette);
      b  = var_muette ;
      fscanf(DONNEES,"%d",&var_muette2);
      N  = var_muette2 ;
      fscanf(DONNEES,"%d",&var_muette2);
      M  = var_muette2 ;
  
      fclose(DONNEES) ;
      
      
 /*****************************************************/     
      
      
  



    dx=a/(N+1.0);dy=b/(M+1.0);
    
    eta = 1.0-2.0*sin(PI/(2.0*(N+1.0)))*sin(PI/(2.0*(M+1.0)));

    w=2.0/(1.0+sqrt(1.0-eta*eta));
  
    e=vecalloc(1,N);
    uold=matalloc(0,N+1,0,M+1);
    unew=matalloc(0,N+1,0,M+1);
    

/******************************************************/
/*            LECTURE VECTEUR BORD SUR Stdin          */
/******************************************************/

 
  if (NumDomain==1) {
  
     for (i=1;i<=N+1;i++){ 
          scanf("%f",&var_muette);
          uold[i][0] = var_muette  ;
	  unew[i][0] = var_muette ;}
      

     for (j=1;j<=N;j++){ 
          scanf("%f",&var_muette);
          uold[N+1][j] = var_muette ;
	  unew[N+1][j] = var_muette;} }
                                            
 

 if (NumDomain==2) {
    
     for (j=1;j<=N;j++){ 
          scanf("%f",&var_muette);
          uold[0][j] =   var_muette  ;
          unew[0][j] =   var_muette  ;}
     for (i=0;i<=N;i++){ 
          scanf("%f",&var_muette);
          uold[i][0] = var_muette ;
	  unew[i][0] = var_muette ;} }  
	  
	  
  /* if (NumDomain==2) {
     
     for (i=0;i<=N;i++){ 
          uold[N-i][0] = var_muette ;
          unew[N-i][0] = var_muette ;}
     
     for (j=1;j<=N;j++){ 
           uold[0][j] =var_muette ;
	   unew[0][j] =var_muette ;} }*/  
  
 
 if (NumDomain==3) {
  
     for (i=1;i<=N;i++){ 
          scanf("%f",&var_muette);
          uold[i][N+1] = var_muette  ; 
	  unew[i][N+1] = var_muette  ;}}
        
	      
	  
	                                
					
							
			  	  



                                                                                                 
/**********************************************************/
/*          RESOLUTION Laplacien(u) = f       on Omega    */
/*                               u  = 0       on Gamma    */
/*                               u  = lambda  on Sigma    */
/**********************************************************/
                          
     fp=fopen(resultat,"w");  
    
    

     do { 
      
      for (k=1;k<=M;k++){
	for (j=1;j<=N;j++){
          unew[j][k] = (0.25)*w*(uold[j+1][k]+uold[j][k+1]+\
                                unew[j-1][k]+unew[j][k-1] - \
				dx*dy*f(j*dx+x0,k*dy+y0))+(1.0-w)*uold[j][k];
	}
      }

        
       for (j=1;j<=N;j++){
	  rsum=0.0;       
          for(k=1;k<=M;k++){
 	    rsum += fabs(unew[j][k]-uold[j][k]);
            e[j] = rsum;
			
	}
      } 
	
       for (j=1;j<=N;j++){
	  for(k=1;k<=M;k++){
 	    uold[j][k] = unew[j][k];
       }
     } 
	       

     }while(maxnum(N,e)>5.0e-9);
 
 
         
/*********************************************************/
/*       ECRITURE VECTEUR BORD SUR Stdout                */
/*********************************************************/      

     if (NumDomain==1) {
   
     for (i=1;i<=N;i++){ 
       printf("%f\n",  uold[i][1]);} 
       
       printf("%f\n",  uold[N][1] );                         
    
     for (j=1;j<=N;j++){ 
       printf("%f\n",  uold[N][j] ) ; }}                       

 /*   if (NumDomain==1) {
   
     for (i=1;i<=N;i++){ 
       printf("%f\n",0);} 
       
       printf("%f\n",0);                         
    
                      
    
     for (j=1;j<=N+1;j++){ 
       printf("%f\n", 0 ) ; }}  */                      
    
    
   








    /* if (NumDomain==2) {
   
     for (j=N+1;j>=0;j--){ 
          printf("%f\n",uold[1][j] );}
     
     printf("%f\n",uold[1][1]);    
     
     for (i=1;i<=N+1;i++){ 
       printf("%f\n",uold[i][1] );} }  */
       
       
if (NumDomain==2) {
          
           
          for (j=1;j<=N;j++){ 
          printf("%f\n",uold[1][j] );}
     
           printf("%f\n",uold[1][1] );   
     
          for (i=1;i<=N;i++){ 
          printf("%f\n", uold[i][1] );} }                                   
	
   
     /*if (NumDomain==2) {
     
       for (i=0;i<=N-1;i++){ 
          printf("%f\n",uold[N-i][1] );}
     
          printf("%f\n",uold[1][1] );  
     
      for (i=1;i<=N;i++){ 
      printf("%f\n", uold[1][j] );} } */                    
	  



    if (NumDomain==3) {
   
       for (i=1;i<=N;i++){ 
          printf("%f\n",uold[i][N] );}}
     
     
	  
	  
	  
                                         
	                   	                                 
	   
 
          fflush(NULL);
	        
          fflush  (stdout) ;




/*********************************************************/
/*       ECRITURE SOLUTION SUR Omega DANS FICHIER        */
/*********************************************************/      

    
   
     for (i=0;i<=N+1;i++){ 
       for (j=0;j<=N+1;j++){ 
       fprintf(fp,"%f %f %f\n",x0+i*dx,y0+j*dy,uold[i][j] );} }
    
        fclose(fp); 
          
	        
 /*********************************************************/            
 
}  /* end while */


return 0;

 

}  /* end main */












