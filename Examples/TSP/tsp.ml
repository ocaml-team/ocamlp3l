open Graphics;;
(* define cities *)
let cities = 
  [| (1.0, 1.0); (2.0,3.0); (1.0, 4.0);
    (3.0, 1.0); (4.0, 2.0); (6.0,0.0);
    (6.0, 6.0); (3.0, 5.0); (2.0, 6.0);
    (5.0, 4.0); (3.0, 4.0); (4.0, 3.0);
    (6.0, 3.0); (1.0, 6.0); (4.0, 6.0) |];;

let n_cities = 15;;


(* define initial points *)
let radius = 0.4;;
let ix     = 3.0;;
let iy     = 3.5;;

let n_points = 50;;

let points = Array.create n_points (ix,iy);;

let init_points _ = 
  let st = (2.0 *. 3.1415) /. (float n_points) in
  for i=0 to (n_points-1) do
    begin
      Array.set points i 
	((ix +.(radius *. (cos ((float i)*.st)))),
	 (iy+. (radius *. (sin ((float i)*.st)))))
    end
  done;;
      

(* normalize screen coordinates *)
let max_x coords = 
  let n = Array.length coords in
  let max = ref (fst coords.(0)) in
  for i=1 to (n-1) do
    if(fst coords.(i) > !max) then max := fst coords.(i)
  done;
  !max;;

let min_x coords = 
  let n = Array.length coords in
  let min = ref (fst coords.(0)) in
  for i=1 to (n-1) do
    if(fst coords.(i) < !min) then min := fst coords.(i)
  done;
  !min;;

let max_y coords = 
  let n = Array.length coords in
  let max = ref (snd coords.(0)) in
  for i=1 to (n-1) do
    if(snd coords.(i) > !max) then max := snd coords.(i)
  done;
  !max;;

let min_y coords = 
  let n = Array.length coords in
  let min = ref (snd coords.(0)) in
  for i=1 to (n-1) do
    if(snd coords.(i) < !min) then min := snd coords.(i)
  done;
  !min;;

let normalize (x,y) coords xdim ydim =
  let xmin = min_x coords in
  let ymin = min_y coords in
  let xmax = max_x coords in
  let ymax = max_y coords in
  let xc   = (x *. (float xdim)) /. (xmax-.ymin) in
  let yc   = (y *. (float ydim)) /. (ymax-.ymin) in
  (truncate xc, truncate yc);;

(* plotting di una serie di punti *)
let dimx = 400;;
let dimy = 400;;

let draw_point x y c = 
  begin
    set_color c;
    for i= -2 to 2 do
      begin
	plot (x+i) y;
	plot x (y+i);
      end
    done
  end;;

let plot_p coords how = 
  let n = Array.length coords in
  for i=0 to (n-1) do
    let (xc,yc) = normalize coords.(i) cities (dimx-10) (dimy-10) in
    begin
      set_color how;
      draw_point xc yc how
    end
  done;;
    
let display c p = 
  begin
    open_graph (" "^(string_of_int dimx)^"x"^(string_of_int dimy));
    plot_p c black;
    plot_p p red;
  end;;

init_points();;

display cities points;;

(* compute distance *)
let distance (a,b) (c,d) =
  let dx = (c -. a) *. (c -. a) in
  let dy = (d -. b) *. (d -. b) in
  sqrt(dx+.dy);;

(* compute strength *)
let phi d k =
  exp ((-. (d *. d))/. (k *. k));;

let strenght i j k = 
  let d = distance cities.(i) points.(j) in
  let suml = ref 0.0 in
  begin
    for l=0 to (n_points - 1) do
      suml := !suml +. (phi (distance cities.(i) points.(l)) k)
    done;
    ((phi (distance cities.(i) points.(j)) k) /. (!suml))
  end;;

let strenght_x i j k = 
  let d = (fst cities.(i)) -. (fst points.(j)) in
  let suml = ref 0.0 in
  begin
    for l=0 to (n_points - 1) do
      suml := !suml +. (phi ((fst cities.(i)) -. (fst points.(l))) k)
    done;
    ((phi ((fst (cities.(i))) -. (fst points.(j))) k) /. (!suml))
  end;;

let strenght_y i j k = 
  let d = (snd cities.(i)) -. (snd points.(j)) in
  let suml = ref 0.0 in
  begin
    for l=0 to (n_points - 1) do
      suml := !suml +. (phi ((snd cities.(i)) -. (snd points.(l))) k)
    done;
    ((phi ((snd (cities.(i))) -. (snd points.(j))) k) /. (!suml))
  end;;

(* now compute the new position of a point *)
let alpha = 0.1;;
let beta  = 0.2;;

let k = ref 10.0;;

let px j k =
  let sumi = ref 0.0 in
  let j1   = if (j+1)>=n_points then 0 else (j+1) in
  let jm1  = if (j-1)<0 then (n_points-1) else (j-1) mod n_points in
  begin
    for i=0 to (n_cities-1) do
      sumi := !sumi +. (strenght_x i j k)
    done;
    alpha *. !sumi +. k *. beta *. 
      ((fst points.(j1)) -. 2.0 *. (fst points.(j)) +. (fst points.(jm1)))
  end;;

let py j k =
  let sumi = ref 0.0 in
  let j1   = if (j+1)>=n_points then 0 else (j+1) in
  let jm1  = if (j-1)<0 then (n_points-1) else (j-1) mod n_points in
  begin
    for i=0 to (n_cities-1) do
      sumi := !sumi +. (strenght_y i j k)
    done;
    alpha *. !sumi +. k *. beta *. 
      ((snd points.(j1)) -. 2.0 *. (snd points.(j)) +. (snd points.(jm1)))
  end;;

let newp i k = 
  ((px i k)+.(fst points.(i))),((py i k)+.(snd points.(i)));;

let do_iter k =
  let new_points = Array.create n_points (0.0,0.0) in
  begin
    clear_graph();
    for i=0 to (n_points - 1) do
      Array.set new_points i (newp i k)
    done;
    for i=0 to (n_points - 1) do
      Array.set points i (new_points.(i))
    done;
    display cities points
  end;;

let _ = read_int();;
