(* all pair shortest path *)

let wm = 
  [| [|  0;  4;  1;  0;  7;  0;  0 |];
    [|   0;  0;  8;  0;  0;  0;  0 |];
    [|   0;  5;  0;  0;  0;  0;  1 |];
    [|   0;  0;  0;  0;  0;  0;  0 |];
    [|   0;  0;  0;  2;  1;  0;  0 |];
    [|   0;  3;  0;  0;  0;  1;  0 |] |];;


type path = Int of int | Inf ;;

let build_dm wm = 
  mapvector(mapvector((function 0 -> Inf | n -> Int n),10),10) wm;;

let pathmin l1 l2 =
  if(l1 = Inf) then l2
  else 
    (if(l2 = Inf) then l1 
    else (if(l1 < l2) then l1 else l2));;

let pathsum l1 l2 =
  if(l1 = Inf or l2=Inf) then Inf
  else let (Int lv1)=l1 and (Int lv2)=l2 in
  (Int (lv1+lv2));;


let v_by_v v1 v2 = 
  let sum = ref 0 in
  for i=0 to Array.length v1 - 1 do
    sum := !sum + v1.(i)*v2.(i)
  done;
  !sum;;

let 
