#!/usr/bin/perl
#
# 
# File "template.ml", line 716, characters 58-3065:
# Warning: this pattern-matching is not exhaustive

while (<STDIN>) {
    if(/File "template.ml", line ([0-9]*), characters ([0-9]*)-([0-9]*):/ or
       /File "nodecode.ml", line ([0-9]*), characters ([0-9]*)-([0-9]*):/ or
       /File "parp3l.ml", line ([0-9]*), characters ([0-9]*)-([0-9]*):/) {
	$skipping = 1;
	next;
    }
    if(/Warning: this pattern-matching is not exhaustive/ and $skipping) {
	$skipping = 0;
	next;
    }
    print "$_\n";
}
