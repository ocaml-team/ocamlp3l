open Pavages;;

(* definition du module des generateurs *)

module M = Make_canonical_generator(Group_hyp_3_3_4);;

let depth = 10;;

let initdegree= 3;;

let nfarm = 10;;

(* the elementary step *)

let generate _ e = M.generate_from e depth;;

(* the farm_based stream processor *)

let pargenerate = parfun (fun () -> farm(seq(generate),nfarm));;

(* now let's try the new version, with load balancing *)

(* the elementary step:  *)

let granularity = 3;;

let generateb _ = 
  function
      [] -> Pavage_ocamlp3l.generate granularity
    | l ->
	List.concat
	  (List.map (fun e -> Pavage_ocamlp3l.generate_from e granularity) l);;

let pargenerateb = parfun (fun () -> farm(seq(generateb),nfarm));;

(* spread a list in nfarm sublists *)

let ncut n l =
  let rec aux =
    function
	(0,l,l') -> (0,l,l')
      | (n,l,a::r) -> aux (n-1,a::l,r)
      | (n,l,l') -> (n,l,l')
  in match aux (n,[],l) with
     (n,l,l') -> (n,List.rev l,l');;

let rec combine =
  function
      ([],[]) -> []
    | (a::r,b::s) -> (a::b)::(combine (r,s))
    | ([],r) -> r
	| (l,[]) -> [l];;

let rec mkacc =
  function
      0 -> []
    | n -> []::(mkacc (n-1));;

let spread nfarm l = 
  let rec aux acc = 
    function 
	[] -> acc
      | r -> if (List.length r) >= nfarm then let _,l,l' = ncut nfarm r in aux (combine (l,acc)) l' 
             else combine(r,acc)
  in List.filter (fun l -> l<>[]) (aux (mkacc nfarm) l);;

(* show the resulting list *)

let show_a_glist l = 
      List.iter
	(fun a -> (Printf.printf "%s " (Group_hyp_3_3_4.print a))) l; print_newline();;

let show_a_result ll =
  List.iter show_a_glist (List.filter (fun l -> List.length l <= depth) ll);;

(* the main computation *)

pardo (fun () ->
  let l1,l2 = List.partition (fun l -> List.length l = initdegree) (M.generate initdegree) in
  let ll = P3lstream.to_list (pargenerate (P3lstream.of_list l1)) in
  List.iter show_a_result ([l2]@ll);

let sep () = Printf.printf "---------------------------------------------------------\n" in
let disp l = List.iter (fun (n,l,e) -> (* Printf.printf "%d, " n; *) show_a_glist l) l in 
sep();
  (* second strategy *)

  let rec loop l k = 
    let l' = List.concat (P3lstream.to_list (pargenerateb (P3lstream.of_list l))) in
    let feedback = List.filter (fun (n,_,_) -> n = k && n< depth) l' in
    let _ = disp (List.filter (fun (n,_,_) -> n<=depth && n > k-granularity) l') in
    if feedback <> [] then loop (spread nfarm feedback) (k+granularity)
  in loop [[]] granularity
)

(*
La fonction a utiliser pour engendrer est:
Pavage_ocamlp3l.generate_from

La fonction a utiliser pour produire les "picture"
a partir des elements  engendres  est:

Poisson_escher_ocamlp3l.visualize
*)
