open List

(* Ligne idiote mais necessaire car Mlgraph redefinit "float" *)

let float_of_int = float;;

(* fichier util.ml *)

module Util =
struct
open Mlgraph;;
let middle {xc=x1; yc=y1} {xc=x2; yc=y2} =
  {xc=(x1+.x2)/.2.; yc=(y1+.y2)/.2.};;

let sigma l = List.fold_right (+.) l 0.;;

let moyenne l =
 let n = float_of_int (List.length l) in
    (sigma l) /. n;;
 
let barycentre ptl = 
 {xc = moyenne (map (fun {xc=x; yc=y} -> x) ptl);
  yc = moyenne (map (fun {xc=x; yc=y} -> y) ptl)};;

let point_translation {xc=x1; yc=y1} {xc=x2; yc=y2} =
   translation (x2-.x1,y2-.y1);;
let middle {xc=x1; yc=y1} {xc=x2; yc=y2} =
  {xc=(x1+.x2)/.2.; yc=(y1+.y2)/.2.};;


let rec interval m n =
  if m > n then [] else m::interval (m+1) n;;

let rec intervalstep x y s =
 if x>y then [] else x::intervalstep (x+.s) y s;;

let points_of_arc seg n = match seg with
  (Arc (center,radius,a1,a2))
  -> let a = (a2 -. a1) /. (float_of_int n) in
     let pt1= transform_point
                (point_translation origin center)
                {xc= radius *. (cosinus a1); yc= radius *. (sinus a1)}
     in (map (fun a -> transform_point (rotation center a) pt1)
           (intervalstep a1 a2 a))
  | _ ->  raise (Failure "Not an arc");;

let transform_points t pts =
       map (transform_point t) pts;;


let text s sz col place =
   center_picture
     (make_text_picture (make_font Helvetica sz) col s)
      place;;


let abs x = if x>=0. then x else -.x;;

 let rec map_try f = function
     [] -> [] 
   | (a::l) -> if (try f a; true with _ -> false)
                  then (f a) :: map_try f l
                  else map_try f l;;

let rec product f l1 l2 =
  match l1 with
        []  -> []
    | (a::l) -> map_try (f a) l2 @ product f l l2;;


(* power : ('a -> 'b -> 'b) -> 'b -> 'a list -> int -> 'b list *)
let power f l x = 
let rec pwr ll = function
          0   ->  ll
   |      n   ->  ll@ pwr (product f l ll) (n-1)
in pwr [x];;

let power_with_index f l x =
 let g y (n,z)  = (n+1,f y z)
 in power g l x;;
end;;



(* fichier complex.ml *)

module Complex =
struct
  open Mlgraph;;


let pi=acos (-1.);;
let sinus a = sin (a*.pi/.180.);;
let cosinus a = cos (a*.pi/.180.);;
let tangente a = tan (a*.pi/.180.);;

(* +type_complex+ *)
type complex= {re_part:float; im_part:float};;
(* +type_complex+ *)
(* +complex+ *)
let mk_cx r i= {re_part=r; im_part=i};;
let cx_1 = mk_cx 1.0 0.0
and cx_0 = mk_cx 0.0 0.0;;
let cx_of_pol rho theta= {re_part=rho*.cosinus theta; 
                          im_part=rho*.sinus theta};;

(* +complex+ *)



(* +operations_complexes+ *)
let conjugate {re_part=r; im_part=i} = {re_part=r; im_part= -.i};;
let modulus {re_part=r; im_part=i}= sqrt(r*.r +. i*.i);;
let rec modulo m a =
 if 0. <= a & a < m then a else
 if a < 0. then modulo m (a+.m)
           else modulo m (a-.m);;
let argument ({re_part=x; im_part=y} as c)=
 let r = modulus c in 
 if y>=0. then acos (x/.r) else modulo (2.*.pi) (-. acos (x/.r) );;
let arg = argument;;
let add_cx {re_part=r1; im_part=i1} {re_part=r2; im_part=i2}=
  {re_part=r1+.r2; im_part=i1+.i2};;
let sub_cx {re_part=r1; im_part=i1} {re_part=r2; im_part=i2}=
  {re_part=r1-.r2; im_part=i1-.i2};;
let uminus_cx {re_part=r; im_part=i}= {re_part= -.r; im_part= -.i};;
let mult_cx {re_part=r1; im_part=i1} {re_part=r2; im_part=i2}=
  {re_part=r1*.r2-.i1*.i2; im_part=i1*.r2+.i2*.r1};;
let div_cx {re_part=r1; im_part=i1} {re_part=r2; im_part=i2} =
   let rho= r2*.r2 +. i2*.i2 in
     if rho=0.0 then failwith "cdiv: division by zero"
       else {re_part=(r1*.r2+.i1*.i2)/.rho;
             im_part=(i1*.r2-.r1*.i2)/.rho};;

(* +operations_complexes+ *)


(*
(* +complex_seq+ *)
let complex_seq c1 c2 n =
 let p = (c2.im_part-.c1.im_part) /. (c2.re_part-.c1.re_part)
 and dx = (c2.re_part-.c1.re_part) /. (float_of_int n)
 in (map (fun x -> {re_part= c1.re_part+. x; im_part=c1.im_part+. p*. x})
        (interval_float 0.0 (c2.re_part-.c1.re_part) dx));;
(* +complex_seq+ *)
*)

let distance c1 c2 =  modulus (sub_cx c1 c2);;
let euclidian_distance = distance;;
let point_of_cx {re_part=r; im_part=i} = {xc=r; yc=i};;
let cx_of_point {xc=r; yc=i} = {re_part=r; im_part=i};;
end;;


(* fichier permutations.ml  *)




(* fichier color_maps.ml *)

module type COLORMAP =
sig
  open Mlgraph;;
  val map: int -> color;;
end;;

module ColorMap1 =
struct
open Mlgraph;;
let map = 
  let col0=Gra 0.3 and col1=Gra 0.5
  and col2=Gra 0.7 and col3=Gra 0.8
  in function 0 -> col0 | 1 -> col1 | 2 -> col2 | 3 -> col3
| _ -> failwith "wrong color";;
end

module ColorMap2 =
struct
open Mlgraph;;
type color = Mlgraph.color;;
let map=
  let c = 255.0 in
  let col0=Rgb (255.0 /. c, 182.0 /. c, 48.0 /. c) 
  and col1=Rgb (110.0 /. c, 156.0 /. c, 84.0 /. c)
  and col2=Rgb (182.0 /. c, 65.0 /. c, 33.0 /. c)
  and col3=Rgb (51.0 /. c, 102.0 /. c, 90.0 /. c)  
  in function 0 -> col0 | 1 -> col1 | 2 -> col2 | 3 -> col3
| _ -> failwith "wrong color";;
end;;

module ColorMap3 =
struct
open Mlgraph;;
type color = Mlgraph.color;;
let map=
  let c = 255.0 in
  let col0=Rgb (255.0 /. c, 182.0 /. c, 96.0 /. c) 
  and col1=Rgb (140.0 /. c, 186.0 /. c, 114.0 /. c)
  and col2=Rgb (222.0 /. c, 105.0 /. c, 73.0 /. c)
  and col3=Rgb (91.0 /. c, 142.0 /. c, 130.0 /. c)  
  in function 0 -> col0 | 1 -> col1 | 2 -> col2 | 3 -> col3
| _ -> failwith "wrong color";;
end;;


(* Fichier groups.ml *)


(* Les deux definitions suivantes definissent la signatures des groupes *)
(* et la notion de produit direct *)
(* Elles ne sont pas utilisees pour le moment dans la bibliotheque *)
(* mais pourraient l'etre a l'avenir *)
(* Dans ces definitions l'operation inverse n'est pas utilisee *)
(* mais elle pourrait etre rajoutee si necessaire *)


module type GROUP =
sig
 type element
 val compose : element -> element -> element
 val identity : element
end;;


module Pair (G1:GROUP) (G2:GROUP)  =
struct
 type element = G1.element * G2.element;;
 let compose (a1,b1) (a2,b2) = G1.compose a1 a2 , G2.compose b1 b2;;
 let identity = G1.identity , G2.identity;;
end;;


(*  Le groupe des permutations servant au traitement des couleurs *)
(*  Il s'agit de permutations sur 4 elements *)

module Color_Group  =
struct
  type element = int array;;
  let compose v' v =
    let n = Array.length v and n' = Array.length v' in
    if n <> n' then failwith "Permutation.compose: uncompatible args"
    else let w = Array.create n 0 in
         let rec cp=
          function 0 -> (w.(0)<-v.(v'.(0));w)
           |  i ->  (w.(i)<-v.(v'.(i)); cp(i-1)) in 
       (try  cp (n-1)
        with _ -> failwith "Permutation.compose: wrong arg");;
  let identity =
    let w = Array.create 4 0
    in 
     let rec ic = function
                    0 -> (w.(0)<-0; w)
                  | i -> (w.(i)<-i; ic(i-1))
      in ic 3;;
end;;


module Tag_Group =
struct
  type element = string;;
  let compose s1 s2 = s1 ^ s2;;
  let identity = ""
end
;;



(* Ce qu'on appelle ici "groupe canonique" est une presentation *)
(* de groupe qui est canonique au sens des systemes de reecriture *)
(* canoniques c'est-a-dire normalisant et confluent *)
(* Ce type de presentation permet de resoudre "le probleme du mot" *)
(* par simple identification des formes normales *)
(* La fonction "compose" est supposee prendre en arguments *)
(* un generateur et une liste de generateurs *)
(* Elle ajoute le premier argument en tete du second si la *)
(* liste de generateurs ainsi obtenue est irreductible *)
(* Il s'agit donc tout betement d'une operation "cons" *)
(* mais une operation cons qui ne produit que des formes normales *)
(* sous l'hypothese que son second argument est en forme normale *)
(* et qui echoue sinon *)
(* Ce type de presentation est precisement destinee a permettre *)
(* d'enumerer exactement l'ensemble des forme normale du groupe *)

module type CANONICAL_GROUP =
sig
 type element
 val generators : element list
 val compose : element -> element list -> element list
end;;


(* Presentation canonique du groupe de symetrie P1 *)
(* TA et TB representent deux translations independantes *)

module Group_euclid_p1 =
struct
 type element = TA | TB  | Ta | Tb ;;
 let generators = [TA;TB;Ta;Tb];;
 exception Group_gen_exc;;
 let compose = function
            TA  -> (function [] -> [TA]
                        | (((TA|TB|Tb)::tl) as tl') -> TA::tl'
                        | (Ta::_) -> raise Group_gen_exc)
     |      TB  -> (function [] -> [TB]
                        | ((TB::tl) as tl') -> TB::tl'
                        | ((TA|Ta|Tb)::_) -> raise Group_gen_exc)
     |      Ta  -> (function [] -> [Ta]
                        | (((Ta|TB|Tb)::tl) as tl') -> Ta::tl'
                        | (TA::_) -> raise Group_gen_exc)
     |      Tb  -> (function [] -> [Tb]
                        | ((Tb::tl) as tl') -> Tb::tl'
                        | ((TA|Ta|TB)::_) -> raise Group_gen_exc);;
end;;


(* Presentation canonique du groupe de symetrie P2 *)
(* TA et TB representent deux translations independantes *)
(* et TC un demi-tour *)

module Group_euclid_p2 =
struct
 type element = TA | TB | TC | Ta | Tb | Tc;;
 let generators = [TA;TB;TC;Ta;Tb;Tc];;
 exception Group_gen_exc;;
 let compose = function
            TA  -> (function [] -> [TA]
                        | (((TA|TB|TC|Tb|Tc)::tl) as tl') -> TA::tl'
                        |  (Ta::_) -> raise Group_gen_exc)
     |      TB  -> (function [] -> [TB]
                        | (((TB|TC|Tc)::tl) as tl') -> TB::tl'
                        | ((TA|Ta|Tb)::_) -> raise Group_gen_exc)
     |      TC  -> (function [] -> [TC]
                        | ((TA|TB|TC|Ta|Tb|Tc)::_) -> raise Group_gen_exc)
     |      Ta  -> (function [] -> [Ta]
                        | (((Ta|TB|TC|Tb|Tc)::tl) as tl') -> Ta::tl'
                        |  (TA::_) -> raise Group_gen_exc)
     |      Tb  -> (function [] -> [Tb]
                        | (((Tb|TC|Tc)::tl) as tl') -> Tb::tl'
                        | ((TA|Ta|TB)::_) -> raise Group_gen_exc)
     |      Tc  -> (function [] -> [TC]
                        | ((TA|TB|TC|Ta|Tb|Tc)::_) -> raise Group_gen_exc);;
end;;

(* Presentation canonique du groupe de symetrie PG *)

module Group_euclid_pg =
struct
 type element = TG | TX | Tg | Tx;;
 let generators = [TG;Tg;TX;Tx];;
 exception Group_gen_exc;;
 let compose = function
            TG  -> (function [] -> [TG]
                        | ((TG::tl) as tl') -> TG::tl'
                        |  ((Tg|TX|Tx)::_) -> raise Group_gen_exc)
     |      TX  -> (function [] -> [TX]
                        | (((TX|TG|Tg)::tl) as tl') -> TX::tl'
                        | (Tx::_) -> raise Group_gen_exc)
     |      Tg  -> (function [] -> [Tg]
                        | ((Tg::tl) as tl') -> Tg::tl'
                        |  ((TG|TX|Tx)::_) -> raise Group_gen_exc)
     |      Tx  -> (function [] -> [Tx]
                        | (((Tx|TG|Tg)::tl) as tl') -> Tx::tl'
                        | (TX::_) -> raise Group_gen_exc)
end;;



(* Presentation canonique du groupe de symetrie PM *)

module Group_euclid_pm =
struct
 type element = TT | TX | TY | Ty;;
 let generators = [TT;TX;TY;Ty];;
 exception Group_gen_exc;;
 let compose = function
            TT  -> (function [] -> [TT]
                        | ((TX::tl) as tl') -> TX::tl'
                        |  _ -> raise Group_gen_exc)
     |      TX  -> (function [] -> [TX]
                        | ((TX::tl) as tl') -> TX::tl'
                       |  _ -> raise Group_gen_exc)
     |      TY  -> (function [] -> [TY]
                        | (((TY|TX|TT)::tl) as tl') -> TY::tl'
                        |  (Ty::_) -> raise Group_gen_exc)
     |      Ty  -> (function [] -> [Ty]
                        | (((TT|TX|Ty)::tl) as tl') -> Ty::tl'
                        | (TY::_) -> raise Group_gen_exc)
end;;




(* Presentation canonique du groupe de symetrie P4 *)
(* engendre par deux rotations d'angle pi/4 *)

module Group_euclid_p4 =
struct
 type element = TA | TB | Ta | Tb;;
 let generators = [TA;TB;Ta;Tb];;
 exception Group_gen_exc;;
 let compose = function
            TA  -> (function [] -> [TA]
                        | ([TA] as tl') -> TA::tl'
                        | ([TB] as tl') -> TA::tl'
                        | (Tb::tl as tl') -> TA::tl'
                        | (TB::TB::tl as tl') -> TA::tl'
                        | (TB::Ta::tl as tl') -> TA::tl'
                        | (TA::TB::tl as tl') -> TA::tl'
                        | (TA::Tb::tl as tl') -> TA::tl'
                        |  _    -> raise Group_gen_exc)

     |      TB  -> (function [] -> [TB]
                        | ([TB] as tl') -> TB::tl'
                        | ([TA] as tl') -> TB::tl'
                        | (Ta::tl as tl') -> TB::tl'
                        | (TA::TA::tl as tl') -> TB::tl'
                        | (TA::Tb::tl as tl') -> TB::tl'
                        | (TB::TA::tl as tl') -> TB::tl'
                        | (TB::Ta::tl as tl') -> TB::tl'
                        |  _    -> raise Group_gen_exc)

     |      Ta  -> (function [] -> [Ta]
                        | ([TB] as tl') -> Ta::tl'
                        | (TB::Ta::tl as tl') -> Ta::tl'
                        |  _    -> raise Group_gen_exc)

     |      Tb  -> (function [] -> [Tb]
                        | ([TA] as tl') -> Tb::tl'
                        | (TA::Tb::tl as tl') -> Tb::tl'
                        |  _    -> raise Group_gen_exc)

end;;













(* Presentation canonique du groupe hyperbolique de rotations  *)
(* d'ordre 3,3 et 4 *)
(* Ce groupe est celui qui est utilise dans les pavages de type *)
(* Circle Limit d'escher *)


module Group_hyp_3_3_4  =
struct
 type element = TA | TB | TC | Ta | Tb | Tc ;;
 let generators = [TA;TB;TC;Ta;Tb;Tc];;
 exception Group_gen_exc;;
let compose = function
  TA -> (function [] -> [TA]
           | ((TA::_)|(TC::_)|(Ta::_)) -> raise Group_gen_exc
           | tl -> TA::tl)
| TB -> (function [] -> [TB]
           | ((TB::_)|(TA::_)|(Tb::_)) -> raise Group_gen_exc
           | tl -> TB::tl)
| TC -> (function [] -> [TC]
           | ((TB::_)|(Tc::_)|(TC::TC::_)) -> raise Group_gen_exc
           | tl -> TC::tl)
| Ta -> (function [] -> [Ta]
           | ((Ta::_)|(Tb::_)|(TC::_)|(TB::_)|(TA::_))
             -> raise Group_gen_exc
           | tl -> Ta::tl)
| Tb -> (function [] -> [Tb]
           | ((TA::_)|(Tc::_)|(Tb::_)|(TC::TC::_)|(TB::_))
             -> raise Group_gen_exc
           | tl -> Tb::tl)
| Tc -> (function [] -> [Tc]
           | ((TA::_)|(Tc::_)|(Ta::_)|(TB::_)|(TC::_))
             -> raise Group_gen_exc
           | tl -> Tc::tl);;

let print = function TA  -> "A" | TB-> "B" | TC-> "C"
              | Ta -> "a" | Tb -> "b" | Tc -> "c";;

let print_list l =  " " ^ (List.fold_left (^) "" (List.map print l));;

end;;



(* Fichier geometries.ml *)



(* On appelle ici "geometrie" un groupe de transformations *)
(* associe a une fonction permettant d'appliquer une transformation *)
(* a un point *)

module type GEOMETRY =
sig
 type point
 type transformation
 module Tgroup : GROUP with type element = transformation
 val apply : transformation -> point -> point
end;;


(* Le module "Euclidean_geometry" utilise comme transformations *)
(* une representation matricielle des transformations affines *)
(* Il contient par ailleurs un certain nombre de fonctions *)
(* permettant de construire des isometries *)
(* Les seules transformations manipulees seront des isometries *)

module Euclidean_geometry  =
struct
 open Mlgraph;;
 type point = Mlgraph.point
 type transformation = Mlgraph.transformation
 let identity = {m11=1.; m12=0.; m13=0.; m21=0.; m22=1.; m23=0.}
 let compose
  {m11=a; m12=b; m13=c; m21=d; m22=e; m23=f}
  {m11=a'; m12=b'; m13=c'; m21=d'; m22=e'; m23=f'}
   =
   {m11=a*.a' +. b*.d'; 
    m12=a*.b' +. b*.e'; 
    m13=a*.c' +. b*.f' +. c; 
    m21=d*.a' +. e*.d'; 
    m22=d*.b' +. e*.e'; 
    m23=d*.c' +. e*.f' +. f}
 module Tgroup =
  struct
     type element = transformation
     let compose = compose
     let identity = identity
  end
 let apply tr pt =
    {xc = tr.m11 *. pt.xc +. tr.m12 *. pt.yc +. tr.m13; 
     yc = tr.m21 *. pt.xc +. tr.m22 *. pt.yc +. tr.m23};;
 let translation (dx,dy) =
  {m11=1.; m12=0.; m13=dx;
   m21=0.; m22=1.; m23=dy}

 let origin_rotation alpha = 
  {m11=cosinus alpha ; m12= -. (sinus alpha); m13=0.0 ; 
   m21=sinus alpha ; m22=cosinus alpha ; m23 = 0.0}

 let rotation {xc=x;yc=y} alpha =
  compose_transformations
       [translation(x,y); origin_rotation alpha; translation(-.x,-.y)]

(*
 let scaling (a,b) = {m11=a ; m12=0.0 ; m13=0.0 ; m21=0.0 ; m22=b ; m23 = 0.0}
*)

 let vsymmetry a =      (* symetrie par rapport a la droite x=a   *)
   {m11= -.1.0; m12=0.0; m13=2.0*.a; m21=0.0; m22=1.0; m23=0.0}

 let hsymmetry a =      (* symetrie par rapport a la droite y=a   *)  
   {m11=1.0; m12=0.0; m13=0.0 ; m21=0.0; m22= -.1.0; m23=2.0*.a}
  

 let symmetry (({xc=x1;yc=y1} as pt1),({xc=x2;yc=y2} as pt2)) =
  if pt1=pt2 
    then failwith "Cannot define a line symmetry with two equal points"
    else if y1=y2
           then let a = y1 in hsymmetry a
	   else if x1=x2
	          then let a=x1 in vsymmetry a
		  else let a = (y2-.y1)/.(x2-.x1)
		       in let b= y1-.a*.x1
		          in Mlgraph.symmetry(a,b)
			  
 let half_turn {xc=x;yc=y} =
        {m11= -.1.0; m12=0.0;  m13=2.0*.x;
         m21=0.0;  m22= -.1.0; m23=2.0*.y}

 let inverse {m11=a;m12=b;m13=c;m21=d;m22=e;m23=f} =
  let det = a*.e -. d*.b 
  in
     if det =0.0 then failwith "inverse_transformation: non inversible matrix"
               else  {m11=e/.det ; m12 = -.b/.det ; m13=(f*.b-.c*.e)/.det ;
                      m21 = -.d/.det ; m22 = a/.det ; m23=(d*.c-.a*.f)/.det}


 let handle_transform ({xc=x1;yc=y1},{xc=x2;yc=y2}) 
                     ({xc=x3;yc=y3},{xc=x4;yc=y4})
 =
 let a'=x2-.x1 and b'=y2-.y1 and a''=x4-.x3 and b''=y4-.y3 in
 let det = a''*.a'' +. b''*.b''  in
 if det = 0.0 
   then failwith "Wrong handle"
   else
     let c = (a'*.a''+.b'*.b'')/.det
     and d = (b'*.a''-.a'*.b'')/.det 
      in
        {m11=c;m12= -.d;m13= -.c*.x3+.d*.y3+.x1;
         m21=d;m22=c;m23= -.d*.x3-.c*.y3+.y1};;
end;;


(* Le module "Hyperbolic_geometry" definit un type transformation *)
(* permettant de representer des isometries hyperbolique dans le *)
(* modele de Poincare *)
(* Ces transformations operent sur des complexes de module <1 *)




module Hyperbolic_geometry  =
struct
open Mlgraph;;
open Complex;;
type point = Mlgraph.point
type transformation = {iso_m:complex; iso_a:complex}
let identity = {iso_m=cx_1;iso_a=cx_0}
let compose {iso_m=mu2; iso_a=a2}  {iso_m=mu1; iso_a=a1} =
  {iso_m= div_cx  (mult_cx mu1 (add_cx mu2 (mult_cx a1 (conjugate a2))))
                   (add_cx cx_1 (mult_cx mu2 (mult_cx (conjugate a1) a2))); 
   iso_a= div_cx (add_cx a1 (mult_cx mu2 a2)) 
                 (add_cx mu2 (mult_cx a1 (conjugate a2)))}

 module Tgroup =
  struct
     type element = transformation
     let compose = compose
     let identity = identity
  end

let apply_cx {iso_m=mu; iso_a=a} z=
    mult_cx mu (div_cx (add_cx z a) 
                       (add_cx cx_1 (mult_cx (conjugate a) z)))

let apply_cx {iso_m={re_part=rm; im_part=im}; 
                   iso_a={re_part=ra; im_part=ia}} 
                  {re_part=r; im_part=i} =
let ra_conj =ra and ia_conj= -.ia in
let r1 = ra_conj*.r-.ia_conj*.i
and i1 = ia_conj*.r+.i*.ra_conj in
let r2 = r1+.1.
and i2 = i1  in
let r3 = r+.ra
and i3 = i+.ia in
let rho = r2*.r2 +. i2*.i2 in 
let r4 = (r3*.r2+.i3*.i2)/.rho
and i4 = (i3*.r2-.r3*.i2)/.rho in
{re_part=rm*.r4-.im*.i4; im_part=im*.r4+.i4*.rm}



let apply iso pt = point_of_cx (apply_cx iso (cx_of_point pt))





let hyp_rotation center angle=
  compose 
     (compose 
           {iso_m=cx_1;iso_a=uminus_cx center}
           {iso_m=mk_cx (cosinus angle) (sinus angle); 
            iso_a=cx_0})
     {iso_m=cx_1; iso_a=center}

(*
let hyp_rotation center angle=
  compose 
     (compose {iso_m=cx_1; iso_a=center}
           {iso_m=mk_cx (cosinus angle) (sinus angle); 
            iso_a=cx_0})
     {iso_m=cx_1;iso_a=uminus_cx center}
*)

let euclidian_distance {re_part=r1; im_part=i1}
                       {re_part=r2; im_part=i2}
= let x=r1-.r2 and y=i1-.i2
  in sqrt(x*.x+.y*.y)
end;;


(* Fichier morphisms.ml *)




(* On appelle "mapping" une application avec un domaine et un codomaine *)
(* specifies. *)

module type MAPPING =
sig
 type source
 type dest
 val map : source -> dest
end;;

module MakePair (Map1: MAPPING) 
                (Map2: MAPPING with type source = Map1.source)
 =
struct
  type source = Map1.source
  type dest = Map1.dest * Map2.dest
  let map s = Map1.map s , Map2.map s
end;;




(* Le foncteur "Make_morphism" permet d'etendre un mapping envoyant *)
(* les generateurs d'un groupe canonique vers des transformations *)
(* geometriques en un morphisme canonique *)

module Make_morphism (G1:CANONICAL_GROUP) 
                    (T:MAPPING with type source = G1.element) 
                    (G2:GROUP with type element = T.dest) 
=
struct
  type source = G1.element list;;
  type dest = G2.element;;
  let rec map = function [] -> G2.identity
                   | a::l  -> G2.compose (T.map a) (map l);;
end;;






(*  Le foncteur "Make_colored_morphism" etend Make_morphism *)
(* en prenant un parmetre supplementaire qui envoie les generateurs *)
(* du groupe canonique sur des permutations *)
(* Il produit un morphisme canonique du groupe G1 (premier argument) *)
(* dans un groupe qui est le produit d'un groupe geometrique et d'un *)
(* groupe de permutations *)

module Make_colored_morphism (G1:CANONICAL_GROUP) 
                    (T:MAPPING with type source = G1.element) 
                    (C:MAPPING with type source = G1.element
                                 and type dest = Color_Group.element)
                    (G2:GROUP with type element = T.dest) 
=

MakePair (Make_morphism (G1) (T) (G2))
         (Make_morphism (G1) (C) (Color_Group));;



(*  Le foncteur "Make_tagged_morphism" remplace la permutation *)
(* par une chaine de caracteres qui represente la suite des generateurs *)
(* du groupe canonique sur des permutations *)

module Make_tagged_morphism (G1:CANONICAL_GROUP) 
                    (T:MAPPING with type source = G1.element) 
                    (Tag:MAPPING with type source = G1.element
                                 and type dest = Tag_Group.element)
                    (G2:GROUP with type element = T.dest) =

MakePair (Make_morphism (G1) (T) (G2))
         (Make_morphism (G1) (Tag) (Tag_Group));;

module Make_colored_tagged_morphism (G1:CANONICAL_GROUP) 
                    (T:MAPPING with type source = G1.element) 
                    (C:MAPPING with type source = G1.element
                                 and type dest = Color_Group.element)
                    (Tag:MAPPING with type source = G1.element
                                 and type dest = Tag_Group.element)
                    (G2:GROUP with type element = T.dest) =

MakePair
         (Make_morphism (G1) (T) (G2))
         (MakePair
              (Make_morphism (G1) (C) (Color_Group))
              (Make_morphism (G1) (Tag) (Tag_Group)));;



(* Fichier generators.ml *)



(* On definit un generateur comme un procede permettant d'engendrer *)
(* une liste d'elements a partir d'un entier *)

(* Le foncteur  "Make_canonical_generator" fabrique un generateur *)
(* a partir d'une description canonique de groupe *)
(* Ce generateur produit toute les formes normales du groupe*)
(* obtenues par composition d'au plus n entiers *)

module type GENERATOR =
sig
 type base
 type product
 val generate_from : base -> int -> product list
 val generate : int -> product list
end;;


module Make_canonical_generator (G:CANONICAL_GROUP)  =
struct
 open Util;;
 type base = G.element list;;
 type product =  G.element list ;;

 let generate_from  x n =
   power G.compose  G.generators x n;;

 let generate  n = generate_from [] n;;

end;;

(* Le foncteur "Make_generator_by_morphism" produit *)
(* un nouveau generateur a partir d'un generateur de groupe canonique, *)
(* d'une geometrie et d'un mapping associant a chaque generateur du *)
(* groupe formel canonique une transformation geometrique *)
(* Ce nouveau generateur produit la liste des image des formes normales *)
(* du groupe formel par homomorphisme dans le groupe des transformations *)
(* geometrique qui lui est associe par le mapping *)

module Make_generator_by_morphism
     (Gen: GENERATOR) 
     (M: MAPPING with type source = Gen.product) =
struct
 type base = Gen.base;;
 type product = M.dest;;
 let generate_from x n = List.map M.map (Gen.generate_from x n);;
 let generate n = List.map M.map (Gen.generate n);;
end;;

(* Le foncteur "Make_mixed_generator" produit *)
(* produit directement un generateur produisant des transformations *)
(* Il est un peu plus efficace car il permet de memoriser au fur et a mesure *)
(* les images des elements du groupe formel *)
module Make_mixed_generator 
      (CG:CANONICAL_GROUP) 
      (M: MAPPING with type source = CG.element) 
      (Gr:GROUP with type element = M.dest)
=
struct
  open Util;;
  type base = int * CG.element list * M.dest;;
  type product = int * CG.element list * M.dest;;
  let generate n =
       power (fun x (n,y,z) ->  n+1, CG.compose x y, Gr.compose (M.map x) z)
               CG.generators (0,[],Gr.identity)  n;;
 let generate_from x n = 
       power (fun x (n,y,z) ->  n+1, CG.compose x y, Gr.compose (M.map x) z)
               CG.generators x n;;

end;;

  





(* Fichier tilings.ml *)


(* On definit un motif de pavage (PATTERN) comme un module *)
(* fournissant une fonction de visualisation et une fonction *)
(* de selection *)
(* La fonction de visualisation produit l'image obtenue *)
(* en appliquant une transformation au motif *)
(* Il est commode de presenter les choses ainsi dans la mesure *)
(* ou la construction de l'image peut prendre des formes variees *)
(* Le type transformation pourra correspondre, suivant le cas, *)
(* a une simple transformation geometrique ou a un objet *)
(* plus complique incluant une transformation et une permutation *)
(* de couleurs *)
(* La fonction "select" permet de ne pas prendre en compte certaines *)
(* transformations, par exemple, lorsqu'elles produident une image *)
(* trop eloignee *)

module type PATTERN =
sig
 open Mlgraph
 type transformation
 val visualize : transformation  -> picture
 val select : transformation -> bool
end


module Add_tags_to_pattern (Pat: PATTERN) =
struct
  type transformation = Pat.transformation * string;;
  open Mlgraph;;
  open Util;;
  let visualize (t,tag) = 
     let p = Pat.visualize t
     in group_pictures [p; text tag (picture_height p /. 4.) black 
                                     (picture_center p)];;
  let select (t,p) = Pat.select t;;
end;;

module Add_tags_to_colored_euclidean_pattern 
         (Pat: PATTERN with type transformation = 
          Euclidean_geometry.transformation * Color_Group.element) =
struct
  type transformation = Euclidean_geometry.transformation * 
                        (Color_Group.element  * string);;
  open Mlgraph;;
  open Util;;
  let visualize (t,(c,tag)) = 
     let p = Pat.visualize (t,c)
     in group_pictures [p; text tag (picture_height p /. 4.) black 
                                     (picture_center p)];;
  let select (t,(c,tag)) = Pat.select (t,c);;
end;;

module Add_tags_to_colored_hyperbolic_pattern 
         (Pat: PATTERN with type transformation = 
          Hyperbolic_geometry.transformation * Color_Group.element) =
struct
  type transformation = Hyperbolic_geometry.transformation * 
                        (Color_Group.element  * string);;
  open Mlgraph;;
  open Util;;
  let visualize (t,(c,tag)) = 
     let p = Pat.visualize (t,c)
     in group_pictures [p; text tag (picture_height p /. 4.) black 
                                     (picture_center p)];;
  let select (t,(c,tag)) = Pat.select (t,c);;
end;;




(* Un pavage (TILING) est simplement definit ici comme une fonction *)
(* qui produit un dessin a partir d'un entier indiquant l'ordre maximal *)
(* des element du groupe de symetrie qui nous interesse *)

module type TILING =
sig
  open Mlgraph
 val draw : int -> picture
end

(* Le foncteur "Make_tiling" permet de construire un pavage a partir *)
(* d'un generateur de transformations et d'un motif. *)

module Make_tiling 
    (Gen: GENERATOR)
    (Pat: PATTERN with type transformation = Gen.product)
=
struct
  open Mlgraph;;
 let rec map_filter select f l =
    match l
    with []  -> []
      | (a::l) -> if select a then f a:: map_filter select f l
                              else map_filter select f l;;
 let draw n = group_pictures 
               (map_filter Pat.select  Pat.visualize (Gen.generate n));;
end;;



module Construct_tiling 
    (Group: CANONICAL_GROUP)
    (Geom: GEOMETRY)
    (GenMap: MAPPING with type source = Group.element
                     and type dest = Geom.transformation)
    (GenColorMap: MAPPING with type source = Group.element
                     and type dest =  Color_Group.element)
    (Pat: PATTERN with type transformation = 
                  Geom.transformation * Color_Group.element)
=
Make_tiling
  (Make_generator_by_morphism
       (Make_canonical_generator(Group)) 
       (Make_colored_morphism
               (Group) (GenMap) (GenColorMap) (Geom.Tgroup)))
  (Pat);;


module Construct_tiling_ocamlp3l
    (Group: CANONICAL_GROUP)
    (Geom: GEOMETRY)
    (GenMap: MAPPING with type source = Group.element
                     and type dest = Geom.transformation)
    (GenColorMap: MAPPING with type source = Group.element
                     and type dest =  Color_Group.element)
    (Pat: PATTERN with type transformation = 
            int * Group.element list * (Geom.transformation * Color_Group.element))
=
 Make_tiling
  (Make_mixed_generator  
         (Group) 
         (MakePair (GenMap) (GenColorMap))
         (Pair (Geom.Tgroup) (Color_Group)))
  (Pat)     ;;





module Construct_tagged_tiling 
    (Group: CANONICAL_GROUP)
    (Geom: GEOMETRY)
    (GenMap: MAPPING with type source = Group.element
                     and type dest = Geom.transformation)
    (GenColorMap: MAPPING with type source = Group.element
                     and type dest =  Color_Group.element)
    (GenTagMap: MAPPING with type source = Group.element
                     and type dest =  Tag_Group.element)
    (Pat: PATTERN with type transformation = 
        (Geom.transformation * (Color_Group.element * Tag_Group.element)))
=
Make_tiling
  (Make_generator_by_morphism
       (Make_canonical_generator(Group)) 
       (Make_colored_tagged_morphism
               (Group) (GenMap) (GenColorMap) (GenTagMap) (Geom.Tgroup)))
       (Pat);;



(* fichier poincare.ml  *)

module Poincare =
struct
open Mlgraph;;
open Util;;
open Complex;;
type angle = Angle of float;;

type circle = {center:complex; radius:float};;

type hyperbolic_line =
   Diameter of angle  |  Circle of circle * angle * angle;;

type hyperbolic_segment =
   HSegment of complex * complex  |  HArc of circle * angle * angle;;


let angle_value (Angle a)  = a;;
let angle_add (Angle a) (Angle b) = Angle (a+.b);;
let angle_sub (Angle a) (Angle b) = Angle (a-.b);;
let angle_mult (Angle a) x = Angle (x*.a);;
let angle_div (Angle a) x = Angle (a/.x);;
let angle_pi = Angle pi;;
let epsilon = 0.00001;;
let equiv x y = abs(x-.y) < epsilon;;
let convert_to_degrees a = 180. *. a /. pi;;
let convert_to_radians a = 180. *. a *. pi /. 180.;;

let pi_over n = Angle(pi /. (float_of_int n));; 


let point_of_cx {re_part=r; im_part=i} = {xc=r; yc=i};;
let cx_of_point {xc=r; yc=i} = {re_part=r; im_part=i};;


let check_point {re_part=x; im_part=y} = sqrt(x*.x+.y*.y) <= 1.;;

let diameter {re_part=xa; im_part=ya} {re_part=xb; im_part=yb} =
  abs(xa*.yb-.xb*.ya) < epsilon;;

let rec modulo m a =
 if 0. <= a & a < m then a else
 if a < 0. then modulo m (a+.m)
           else modulo m (a-.m);;

let angle_modulo (Angle a) (Angle b) = Angle (modulo a b);;

let order_pair (a1,a2) = if a1 <= a2 then (a1,a2)  else (a2,a1) ;;
let angle_order_pair (Angle a1, Angle a2) =
  if a1 <= a2 then (Angle a1, Angle a2)  else (Angle a2,Angle a1) ;;


(*  La fonction "vision_angle" calcule quelle est la direction
du point (complexe) c2 vu depuis le point (complexe) c1 *)

let vision_angle {re_part=x1; im_part=y1}  {re_part=x2; im_part=y2} =
if equiv x1 x2 then if y1>=y2 then Angle (3.*.pi/.2.) 
                              else Angle (pi/.2.)
               else Angle (modulo (2.*.pi)
                                  (atan((y2-.y1)/.(x2-.x1)) 
                                   +. if x1>=x2 then pi else 0.));;

(*  La fonction "extremities" calcule les points d'intersection
d'une droite hyperbolique avec le cercle unite' *)

let extremities c =
match c 
with Diameter (Angle a) -> {re_part=cos a; im_part=sin a} , {re_part= -.cos a; im_part= -. sin a}
 | Circle  ({center= {re_part=x; im_part=y}; radius=rad} 
          , Angle a1,Angle a2)
    -> let x1 = x +. rad *. cos a1
       and y1 = y +. rad *. sin a1
       and x2 = x +. rad *. cos a2
       and y2 = y +. rad *. sin a2 
       in {re_part=x1; im_part=y1} ,  {re_part=x2; im_part=y2};;


(* Construction de la droite hyperbolique joignant deux points 
situe's a l'interieur du cercle unite' *)

let construct_hyp_line1 ({re_part=xa; im_part=ya} as aa) ({re_part=xb; im_part=yb} as bb) =
if not(check_point aa && check_point bb)
then raise (Failure "Not hyperbolic points")
else if diameter aa bb then 
          let dx = xa-.xb and dy = ya-.yb in
          if abs dx < epsilon then Diameter (Angle(pi/.2.))
                              else Diameter (Angle(atan(dy/.dx)))
else
  let da = xa*.xa +. ya*.ya  +. 1.
  and db = xb*.xb +. yb*.yb  +. 1.
  and c = xa*.yb -. xb*.ya in
  let x=(yb*.da-.ya*.db)/.(2.*.c)
  and y= (xa*.db-.xb*.da)/.(2.*.c) in
  let rad = sqrt((x*.x+.y*.y) -.1.)
  and center = {re_part=x; im_part=y} in
  let delta = Angle (atan (1./.rad)) 
  and alpha = vision_angle center cx_0 in 
     Circle  ({center= center;radius=rad} 
              ,  angle_sub alpha delta 
              , angle_add alpha delta);;

(*
let construct_hyp_segment ({re_part=xa; im_part=ya} as aa) ({re_part=xb; im_part=yb} as bb) =
if not(check_point aa && check_point bb)
then raise (Failure "Not hyperbolic points")
else if diameter aa bb then HSegment (aa,bb)
else 
  let da = xa*.xa +. ya*.ya  +. 1.
  and db = xb*.xb +. yb*.yb  +. 1.
  and c = xa*.yb -. xb*.ya in
  let x=(yb*.da-.ya*.db)/.(2.*.c)
  and y= (xa*.db-.xb*.da)/.(2.*.c) in
  let rad = sqrt((x*.x+.y*.y) -.1.)
  and center = {re_part=x; im_part=y} in
  let a = angle_value (vision_angle center aa)
  and b = angle_value (vision_angle center bb)
  in let (a,b) = order_pair (a,b) 
     in let (a,b) = if b-.a > pi then (b,a) else (a,b)
        in HArc ({center= center;radius=rad},Angle a, Angle b);;
*)

let construct_hyp_segment ({re_part=xa; im_part=ya} as aa) ({re_part=xb; im_part=yb} as bb) =
match construct_hyp_line1 aa bb
with  Diameter _  ->   HSegment (aa,bb)
  | Circle({center= center;radius=rad} ,_,_)
    ->    let a = angle_value (vision_angle center aa)
          and b = angle_value (vision_angle center bb)
          in let (a,b) = order_pair (a,b) 
             in let (a,b) = if b-.a > pi then (b,a) else (a,b)
                 in HArc ({center= center;radius=rad},Angle a, Angle b);;




(* Construction d'une droite hyperbolique a partir d'un centre 
exterieur au cercle unite' *)

let construct_hyp_line2 ({re_part=xm; im_part=ym} as mm)  =
if check_point mm then raise (Failure "Point should be outside")
else let  rad = sqrt((xm*.xm+.ym*.ym) -.1.) in
     let delta = Angle(atan (1./.rad))
     and alpha = vision_angle mm cx_0 in 
        Circle  ({center= mm;radius=rad}, angle_sub alpha delta
                      , angle_add alpha delta);;


(* Construction d'une droite hyperbolique a partir d'un point 
et d'une tangente en ce point *)

let construct_hyp_line3 ({re_part=xa; im_part=ya} as aa) (Angle a) =
if not(check_point aa)
then raise (Failure "Not hyperbolic point")
else if equiv (ya/.xa) (tan a)
then Diameter (Angle a)
else
 if equiv (modulo (2.*.pi) a) (pi/.2.)
 or equiv (modulo (2.*.pi) a) (3.*.pi/.2.)
     then let x= (xa*.xa-.ya*.ya+.1.)/.(2.*.xa) in
          let rad = sqrt ((xa-.x)*.(xa-.x))
          and center = {re_part=x; im_part=ya} in
          let delta = Angle(atan (1./.rad))
          and alpha = vision_angle center cx_0 in
          Circle  ({center= center; radius=rad}
                    ,angle_sub alpha delta
                    ,angle_add alpha delta)

     else
     let t = tan a in
     let x = ((xa*.xa-.ya*.ya+.1.)*.t -. 2.*.xa*.ya)/.(2.*.(xa*.t-.ya)) in
     let y = (xa-.x)/.t+.ya in
     let rad = sqrt ((xa-.x)*.(xa-.x)+.(ya-.y)*.(ya-.y))
     and center =  {re_part=x; im_part=y} in
    let delta = Angle(atan (1./.rad)) 
    and alpha = vision_angle center cx_0 in 
     Circle  ({center= center;radius=rad}
               ,angle_sub alpha delta 
               ,angle_add alpha delta);;


(* Construction a partir d'une droite hyperbolique
existante , d'un point de cette droite et d'un angle
a faire avec la premiere droite.*)

let construct_hyp_line4 c ({re_part=xa; im_part=ya} as aa) (Angle a) =
let b=
match c 
with Diameter (Angle b) ->  b
 |   Circle (c,_,_)
      -> let {re_part=x; im_part=y} = c.center in
           modulo (2.*.pi)(atan((ya-.y)/.(xa-.x))-.pi/.2.)
in
         construct_hyp_line3 aa (Angle(modulo (2.*.pi)(b+.a)));;


(* Construction a partir d'un point A sur le cercle et d'un
angle alpha definssant la droite passant par l'origine
ou se trouve le centre du cercle
Cette fonction n'est pas correcte telle qu'elle est ecrite
A CORRIGER *)

let construct_hyp_line5 ({re_part=xa; im_part=ya} as aa) (Angle alpha) =
if not (abs(xa*.xa +. ya*.ya -.1.)<=epsilon)
 then raise (Failure "Point should be on the circle")
 else let theta = if ya >=0. then asin ya 
                             else -. asin ya
      and alpha = modulo pi alpha +. if ya<0. then pi else 0.
     in if equiv (modulo pi (theta-.alpha))  0.
              then Diameter (Angle theta)
      else 
      let rad = abs(tan(theta-.alpha))
      and x = cos alpha /. abs(cos(theta-.alpha))
      and y = sin alpha /. abs(cos(theta-.alpha)) in
      let delta = atan (1./.rad)
      in
        Circle ({center= {re_part=x; im_part=y}; radius=rad}
                 ,Angle(pi +. alpha -. delta)
                  ,Angle(pi +. alpha +. delta));;


let intersect c1 c2 =
 let p1,p1' = extremities c1
 and p2,p2' = extremities c2 in
 let th1,th1' = order_pair (argument p1 , argument p1')
 and th2,th2' = order_pair (argument p2 , argument p2')
 in (th1 < th2 & th2 < th1' & th1' < th2')
 or (th2 < th1 & th1 < th2' & th2' < th1');;

let parallel c1 c2 =
 let p1,p1' = extremities c1
 and p2,p2' = extremities c2 in
 let th1,th1' = order_pair (argument p1 , argument p1')
 and th2,th2' = order_pair (argument p2 , argument p2')
 in abs(th2 -. th1) < epsilon 
 or abs(th2 -. th1') < epsilon
 or abs(th2' -. th1) < epsilon
 or abs(th2' -. th1') < epsilon;;

let in_range (Angle a) (Angle a1,Angle a2) =
          let a = modulo (2.*.pi) a
          and a1,a2 = order_pair (modulo (2.*.pi) a1,modulo (2.*.pi) a2)
          in a1 <= a & a <= a2;;

let second_degre(a,b,c) =
 let det = b*.b-.4.*.a*.c in
  if det < 0. then raise (Failure "No solution")
   else ((-.b-.sqrt det)/.(2.*.a) , (-.b+.sqrt det)/.(2.*.a));;

(*
(* Resolution de a*sin t + b*cos t = c *)
let sin_cos(a,b,c) =
  let (s1,s2) = second_degre(a*.a+.b*.b , -.2.*.a*.c , c*.c-.b*.b )
  in (asin s1, pi -.asin s1, asin s2, pi -.asin s2);;

*)

let circles_intersection (c1,s1,e1)  (c2,s2,e2) =
 let d = distance c1.center c2.center
 and r1 = c1.radius and r2 = c2.radius
 in let a= Angle(acos ((d*.d -.r1*.r1+.r2*.r2)/.(2.*.d*.r2)))
    in let alpha =  vision_angle c2.center c1.center
       in let a1 = angle_sub alpha a and a2 = angle_add alpha a
          in let a=
              if  in_range a1 (s2,e2) then angle_value a1 else
              if  in_range a2 (s2,e2) then angle_value a2 
                else raise (Failure "")
           in {re_part=c2.center.re_part +. r2*.cos a; 
               im_part=c2.center.im_part +. r2*.sin a};;


let circle_diameter_intersection (c,s,e) (Angle a) =
  let x = c.center.re_part and y = c.center.im_part
  and r = c.radius  in
  if equiv a (pi/.2.) or equiv a (3.*.pi/.2.) 
    then let (y1,y2) = second_degre(1., -.2.*.y, x*.x+.y*.y-.r*.r)
         in let yy = if abs(y1)<= abs(y2) then y1 else y2
            in {re_part=0.; im_part=yy}
     else let t=tan a in
        let (x1,x2) = second_degre(1.+.t*.t, -.2.*.(x+.y*.t), x*.x+.y*.y-.r*.r)
        in let xx = if abs(x1)<= abs(x2) then x1 else x2
           in {re_part=xx; im_part=xx *. t};;


let intersection circle1 circle2 =
if not(intersect circle1 circle2) 
 then raise (Failure "No intersection")
 else 
match circle1,circle2
with Circle (c1,s1,e1) , Circle (c2,s2,e2) 
             -> circles_intersection (c1,s1,e1) (c2,s2,e2)
 |  Circle (c,s,e) , Diameter a  -> circle_diameter_intersection (c,s,e)  a
 |  Diameter a , Circle (c,s,e)  -> circle_diameter_intersection (c,s,e)  a
 |  Diameter a , Diameter b  -> cx_0;;


let circles_angle ((c1,_,_) as circle1) ((c2,s2,e2) as circle2) =
 let {re_part=x; im_part=y} = circles_intersection circle1 circle2 
 and {re_part=x1; im_part=y1} = c1.center
 and {re_part=x2; im_part=y2} = c2.center
 in let a1 = modulo pi (atan((y-.y1)/.(x-.x1))) 
    and a2 = modulo pi (atan((y-.y2)/.(x-.x2)))
    in let x = abs(a1-.a2)
    in Angle(min x (pi-.x));;

let circle_diameter_angle  ((c1,s1,e1) as circle1) (Angle a) =
 let {re_part=x; im_part=y} = circle_diameter_intersection circle1 (Angle a) 
 and {re_part=x1; im_part=y1} = c1.center
 in let a1 = modulo pi (atan((y-.y1)/.(x-.x1)) +. pi/.2.)
    and a2 = atan a
    in let x = abs(a1-.a2)
    in Angle(min x (pi-.x));;

let hyp_angle circle1 circle2 =
if not(intersect circle1 circle2) 
 then raise (Failure "No intersection")
 else 
match circle1,circle2
with Circle (c1,s1,e1) , Circle (c2,s2,e2) -> circles_angle (c1,s1,e1) (c2,s2,e2)
 |  Circle (c,s,e) , Diameter a  -> circle_diameter_angle (c,s,e)  a
 |  Diameter a , Circle (c,s,e)  -> circle_diameter_angle (c,s,e)  a
 |  Diameter (Angle a1) , Diameter (Angle a2) 
         -> let x = abs(a1-.a2)
            in Angle(min x (pi-.x));;



let reseau r n =
  List.map construct_hyp_line2 
     (List.map (fun p -> let a= (2.*.pi/.(float_of_int n))*.(float_of_int p)
                    in {re_part=r*.cos a; im_part=r*.sin a})
          (interval 0 (n-1)));;


(* 
Construit un triangle hyperbolique ABC d'angles alpha, beta et gamma
par une construction approchee
A est l'origine, C est sur l'axe des x  
*)

(*
let  hyper_triangle (Angle alpha) (Angle beta) (Angle gamma) =
 let angle3 x =
   let a =  mk_cx x (x  *. tan alpha) in
   let oa = Diameter (Angle alpha)
   and ob = Diameter (Angle 0.) in
   let ab = construct_hyp_line3 a (Angle (alpha+.beta)) in
   let (Angle beta') = hyp_angle ob ab
          in beta'
 in let xa = fst (dicho((fun x -> angle3 x -. gamma),
                        epsilon, cos alpha -. epsilon,0.00001)) in
    let a =  mk_cx xa (xa  *. tan alpha) in
    let oa = Diameter (Angle alpha)
    and ob = Diameter (Angle 0.) in
    let ab = construct_hyp_line3 a (Angle (alpha+.beta)) in
    let b = intersection ob ab
    in (cx_0,a,b);;
*)



(* Construit un triangle hyperbolique ABC d'angles alpha, beta et gamma
par une construction exacte 
A est l'origine, C est sur l'axe des x  
*)

let  hyper_triangle (Angle alpha) (Angle beta) (Angle gamma) =
  let delta = pi -. (alpha +. beta +. gamma) in
  let ta = tan alpha  and tgd = tan (gamma+.delta/.2.)  in
  let b = mk_cx (tgd /. (ta +. tgd)) (ta *. tgd /. (ta +. tgd)) 
  and c = mk_cx 1. 0. in
  let bc = distance b c in
  let y = bc /. (2. *. tan(delta/.2.)) 
  and tr = translation (1.-. bc /.2.,0.)
  and rot = rotation (point_of_cx c) (-. 180. *. (gamma+.delta/.2.) /. pi) in
  let d = cx_of_point
           (transform_point rot (transform_point tr  {xc=0.; yc=y})) in
  let od = distance cx_0 d and cd = distance c d in
  let r = sqrt (od*.od -. cd *. cd) in
  let b' = mk_cx (b.re_part/.r) (b.im_part/.r)
  and c' = mk_cx (c.re_part/.r) (c.im_part/.r) 
  in (cx_0,b',c');;

let hyper_bitriangle (Angle alpha) (Angle beta) (Angle gamma) =
  let (a,b,c) =  hyper_triangle (Angle alpha) (Angle beta) (Angle gamma) in
  let rot = Hyperbolic_geometry.hyp_rotation a 
                (convert_to_degrees (2.*.alpha)) in
  let d =  Hyperbolic_geometry.apply_cx rot c in
    (a,d,b,c);;





let lw = 0.002;;
let lsty = {linewidth= lw;linecap=Buttcap;
            linejoin=Beveljoin;dashpattern=[]};;


let poincare = make_draw_picture (lsty,black)
                (make_sketch [Arc (origin,1.,0.,360.)]);;
let poincarec c r = make_fill_picture (Nzfill,c)
                (make_sketch [Arc (origin,r,0.,360.)]);;

let draw_point p =
 make_fill_picture (Nzfill,black)
       (make_sketch [Arc(point_of_cx p,2.*.lw,0.,360.)]);;
let draw_points pl =
  group_pictures (map draw_point pl);;
let draw_polygon_line pl =
  make_draw_picture (lsty,black)
           (make_sketch [Seg (map point_of_cx pl)] );;

let draw_hyp_line hl =
let geom = match hl 
           with Diameter (Angle a) -> Seg [{xc=cos a; yc=sin a};
                                           {xc= -.cos a; yc= -.sin a}]
            |  Circle ({center=c; radius=r},Angle a1,Angle a2)
              -> Arc (point_of_cx c,r,convert_to_degrees a1,
                                      convert_to_degrees a2)
in make_draw_picture (lsty,black)
        (make_sketch [geom]);;

let draw_hyp_lines hls =
  group_pictures (map draw_hyp_line hls);;


let draw_hyp_segment hs =
let geom = match hs 
           with HSegment (aa,bb) -> Seg [point_of_cx aa ;point_of_cx bb]
            |  HArc ({center=c; radius=r},Angle a1,Angle a2)
              -> Arc (point_of_cx c,r,convert_to_degrees a1,
                                      convert_to_degrees a2)
in make_draw_picture (lsty,black)
        (make_sketch [geom]);;

let rec split_polygon =
function ([]|[_])  -> raise (Failure "Wrong polygon")
  |       [p1;p2]  ->  [p1,p2]
  |    p1::p2::pl  ->  (p1,p2):: split_polygon (p2::pl);;

let  poincare_polygon_picture pl =
group_pictures
 (map (fun (p1,p2) -> draw_hyp_segment (construct_hyp_segment p1 p2))
     (split_polygon  pl));;
end;;




(* Fichier circle_limit.ml *)


(* Le module "Circle_limit_mapping" est un mapping qui envoie *)
(* les generateurs du groupe de  hyperbolique 3-3-4 *)
(* defini dans le module Groups dans  des transformations *)
(* geometriques correspondant au pavage Circle Limit d'Escher *)

module Circle_limit_mapping =
struct
  open Complex;;
  open Hyperbolic_geometry;;
  open Group_hyp_3_3_4;;
  type source = Group_hyp_3_3_4.element;;
  type dest = Hyperbolic_geometry.transformation;;
  let ptA= cx_of_pol (sqrt(sinus 7.5 /. cosinus 37.5)) 0.0;;
  let ptB= cx_of_pol (sqrt(sinus 7.5 /. cosinus 37.5)) 45.0;;
  let ptC= cx_0;;
  let  tA = hyp_rotation ptA 120.0
  and  tB = hyp_rotation ptB 120.0
  and  tC = hyp_rotation ptC 90.0
  and  ta = hyp_rotation ptA (-.120.0)
  and  tb = hyp_rotation ptB (-.120.0)
  and  tc = hyp_rotation ptC (-.90.0);;

  let map = function   
    TA -> tA
  | TB -> tB
  | TC -> tC
  | Ta -> ta
  | Tb -> tb
  | Tc -> tc;;
end;;

(* Le module "Circle_limit_color_mapping" associe des permutations *)
(* de couleurs aux generateurs du groupe 3-3-4 *)

module Circle_limit_color_mapping =
struct
  open Group_hyp_3_3_4;;
  type source = Group_hyp_3_3_4.element;;
  type dest =  Color_Group.element;;
  let map = function
    TA ->  [|3; 0; 2; 1|]
  | TB -> [|3; 1; 0; 2|]
  | TC -> [|1; 0; 3; 2|]
  | Ta -> [|1; 3; 2; 0|]
  | Tb -> [|2; 1; 3; 0|]
  | Tc -> [|1; 0; 3; 2|];;
end;;

(* Le module "Circle_limit_tag_mapping" associe des etiquettes *)
(* aux generateurs du groupe 3-3-4 *)

module Circle_limit_tag_mapping =
struct
  open Group_hyp_3_3_4;;
  type source = Group_hyp_3_3_4.element;;
  type dest =  Tag_Group.element;;
  let map = function
    TA ->  "A"
  | TB ->  "B"
  | TC -> "C"
  | Ta -> "a"
  | Tb -> "b"
  | Tc -> "c";;
end;;


(* Le module Poisson_escher definit le motif orginal d'Escher *)
(* pour le pavage Circle Limit *)


module Poisson_escher =
struct
  open Mlgraph;;
  open Complex;;
  type transformation = Hyperbolic_geometry.transformation * 
                        Color_Group.element;;


  let ptA= cx_of_pol (sqrt(sinus 7.5 /. cosinus 37.5)) 0.0;;
  let ptB= cx_of_pol (sqrt(sinus 7.5 /. cosinus 37.5)) 45.0;;
  let ptC= cx_0;;
  let rotA= Hyperbolic_geometry.hyp_rotation ptA 120.0;;
  let rotB= Hyperbolic_geometry.hyp_rotation ptB 120.0;;
  let rotC=  Hyperbolic_geometry.hyp_rotation ptC 90.0;;
  let ptD= Hyperbolic_geometry.apply_cx rotC ptA;;
  let centre = cx_of_point 
          (Util.barycentre (List.map point_of_cx [ptA;ptB;ptC;ptD]));;


(* +points_du_poisson+ *)
let corps,quad,dessin =
let p0	=ptA
and p1	=mk_cx	(0.32)	(-0.018)
and p2	=mk_cx	(0.24)	(0.0)
and p3	=mk_cx	(0.2)	(-0.1)
and p4	=mk_cx	(0.09)	(-0.14)
in
let p5	=ptC
and p6	=Hyperbolic_geometry.apply_cx rotC p4
and p7	=Hyperbolic_geometry.apply_cx rotC p3
and p8	=Hyperbolic_geometry.apply_cx rotC p2
and p9	=Hyperbolic_geometry.apply_cx rotC p1
and p10	=Hyperbolic_geometry.apply_cx rotC p0
in
let p11	=mk_cx	(0.08)	(0.355)
and p12	=mk_cx	(0.155)	(0.338)
and p13	=mk_cx	(0.18)	(0.25)
and p14	=mk_cx	(0.25)	(0.2)
in
let p15	=ptB
and p16	=Hyperbolic_geometry.apply_cx rotB p14
and p17	=Hyperbolic_geometry.apply_cx rotB p13
and p18	=Hyperbolic_geometry.apply_cx rotB p12
and p19	=Hyperbolic_geometry.apply_cx rotB p11
in




(* +interieur_du_poisson+ *)
let q1	=mk_cx	(0.26)	(0.09)
and q2	=mk_cx	(0.16)	(0.19)
and q3	=mk_cx	(0.08)	(0.28)
and q4	=mk_cx	(0.22)	(0.02)
and q5	=mk_cx	(0.16)	(0.07)
and q6	=mk_cx	(0.305)	(0.17)
and q7	=mk_cx	(0.27)	(0.19)
and q8	=mk_cx	(0.09)	(0.22)
and q9	=mk_cx	(0.08)	(0.25)
and q10	=mk_cx	(0.11)	(0.285)
and q11	=mk_cx	(0.16)	(0.26)
and q12	=mk_cx	(0.34)	(0.01)
and q13	=mk_cx	(0.31)	(0.01)
and q14	=mk_cx	(0.32)	(0.03)
and q15	=mk_cx	(0.365)	(0.05)
and q16	=mk_cx	(0.35)	(0.08)
and q17	=mk_cx	(0.338)	(0.068)
and  r1	=mk_cx	(0.15)	(0.065)
and r2	=mk_cx	(0.09)	(0.005)
and r3	=mk_cx	(0.055)	(-0.055)
and r4	=mk_cx	(0.18)	(0.04)
and r5	=mk_cx	(0.12)	(-0.025)
and r6	=mk_cx	(0.1)	(-0.105)
and r7	=mk_cx	(0.215)	(0.015)
and r8	=mk_cx	(0.165)	(-0.05)
and r9	=mk_cx	(0.16)	(-0.095)
and r10	=mk_cx (0.3)	(0.175)
and r11	=mk_cx	(0.35)	(0.25)
and r12	=mk_cx	(0.275)	(0.195)
and r13	=mk_cx	(0.315)	(0.275)
and r14	=mk_cx	(0.035)	(0.29)
and r15	=mk_cx	(0.09)	(0.34)
in




(* +corps+ *)
(function t -> let tt z = point_of_cx (Hyperbolic_geometry.apply_cx t z)
in make_sketch [Seg (map tt [p0;p1;p2;p3;p4;p5;p6;p7;p8;p9;
                             p10;p11;p12;p13;p14;p15;p16;p17;
                             p18;p19;p0])])
,


(* +quad+ *)
(function t ->  let tt z = point_of_cx (Hyperbolic_geometry.apply_cx t z)in
make_sketch [Seg (map tt [ptA;ptB;ptD;ptC;ptA])])
,


(* +dessin+ *)
(function t  -> let tt z = point_of_cx (Hyperbolic_geometry.apply_cx t z)
in
group_sketches [
make_sketch [Seg (map tt [p0;q1;q2;q3;p10])];
make_sketch [Seg (map tt [q4;q5])];
make_sketch [Seg (map tt [q6;q7])];
make_sketch [Seg (map tt [q8;q9;q10;q11])];
make_sketch [Seg (map tt [q12;q13;q14;q12])];
make_sketch [Seg (map tt [q15;q16;q17;q15])];
make_sketch [Seg (map tt [r1;r2;r3])];
make_sketch [Seg (map tt [r4;r5;r6])];
make_sketch [Seg (map tt [r7;r8;r9])];
make_sketch [Seg (map tt [r10;r11])];
make_sketch [Seg (map tt [r12;r13])];
make_sketch [Seg (map tt [q9;r14])];
make_sketch [Seg (map tt [q10;r15] )]
]);;






let poisson color_map t col =
  let contours_sk = corps t
  and decor_sk = dessin t
  in group_pictures
       [make_fill_picture
          (Nzfill, color_map col) contours_sk;
        make_default_draw_picture
          (group_sketches [contours_sk; decor_sk])];;

(* +le_poisson_pour_escher+ *)



let visualize (t,c) =
  let contours_sk = corps t
  and decor_sk = dessin t
  in group_pictures
       [make_fill_picture
          (Nzfill, ColorMap3.map (c.(0))) contours_sk;
        make_default_draw_picture
          (group_sketches [contours_sk; decor_sk])];;

let select (t,col) = euclidian_distance cx_0  
                        (Hyperbolic_geometry.apply_cx t centre) < 0.98;;


end;;


module Poisson_escher_tagged =
  Add_tags_to_colored_hyperbolic_pattern (Poisson_escher);;

module Poisson_escher_ocamlp3l =
struct
  type transformation = int * Group_hyp_3_3_4.element  list *
         (Hyperbolic_geometry.transformation * Color_Group.element);;
  let visualize (_,_,(t,col)) = Poisson_escher.visualize (t,col);;
  let select  (_,_,(t,col)) = Poisson_escher.select (t,col);;
end;;



(* Le module  Kangourou_escher  definit un autre  motif *)
(* pour le pavage Circle Limit.  *)
(* Il a ete dessine par le sculpteur Raoul Raba qui a reussi a rendre *)
(* esthetique un motif un peu calamiteux que j'avais produit *)
(* moi-meme auparavant en collaboration avec Claudie Missenard *)
(* Raoul Raba a dessine ce pavage sur papier a partir d'une grille *)
(* de triangle hyperbolique qe je lui avait fournie. *)
(* Pour reussir a produire d'autre motifs interessants, il faudrait *)
(* disposer d'un editeur graphique de motifs, c'est-a-dire d'un editeur *)
(* graphique connaissant les groupes de symetries de pavages *)
(* A faire quand j'aurai du temps ... *)



module Kangourou_escher =
struct
  open Mlgraph;;
  open Complex;;
   type transformation = Hyperbolic_geometry.transformation * 
                         Color_Group.element;;
  open Hyperbolic_geometry;;

  let ptA= cx_of_pol (sqrt(sinus 7.5 /. cosinus 37.5)) 0.0;;
  let ptB= cx_of_pol (sqrt(sinus 7.5 /. cosinus 37.5)) 45.0;;
  let ptC= cx_0;;
  let rotA= hyp_rotation ptA 120.0;;
  let rotB= hyp_rotation ptB 120.0;;
  let rotC=  hyp_rotation ptC 90.0;;
  let ptD= apply_cx rotC ptA;;
  let centre = cx_of_point 
          (Util.barycentre (List.map point_of_cx [ptA;ptB;ptC;ptD]));;


(* +points_du_kangourou+ *)
let tr x = x *. 0.4056 /. 72.0;;
let mk x y = mk_cx (tr x) (tr y);;


let k1 = mk (-3.0) (-0.5)
and k2 = mk (-7.5) (-2.5)
and k3 = mk (-9.0) (-4.0)
and k4 = mk (-9.0)  (-6.0)
and k5 = mk (-8.5)  (-9.0)
and k6 = mk  (-6.5)  (-12.0)
and k7 = mk  (-4.0)  (-14.0)
and k8 = mk  0.0  (-15.0)
and k9 = mk  4.5  (-14.5)
and k91 = mk 8.5 (-13.5)
and k92 = mk 11.5 (-12.5)
and k93 = mk 13.5 (-11.5)
and k10 = mk 18.0  (-8.0)
and k11 = mk 19.5  (-11.5)
and k12 = mk 19.0  (-15.0)
and k13 = mk 16.0  (-18.0)
and k14 = mk 13.5 (-20.0)
and k15 = mk 9.5  (-22.5)
and k16 = mk 5.5  (-24.0)
and k17 = mk 3.0  (-25.5)
and k18 = mk 2.5 (-29.0)
and k19 = mk 4.0 (-31.0)
and k20 = mk 8.0 (-30.0)
and k21 = mk 21.0 (-30.0);;

let kl_1_21 = [k1;k2;k3;k4;k5;k6;k7;k8;k9;k10;k11;k12;k13;
               k14;k15;k16;k17;k18;k19;k20;k21];;

let kl_1_21_rotC = map (apply_cx rotC) kl_1_21;;


let l0 = mk 21.0 (-30.0)
and l1 = mk 24.0 (-30.0)
and l2 = mk 23.5 (-37.5)
and l3 = mk 25.0 (-41.5)
and l4 = mk 28.0 (-44.0)
and l5 = mk 30.0 (-43.0)
and l6 = mk 31.0 (-40.0)
and l7 = mk 31.5 (-35.0)
and l8 = mk  31.0  (-30.0)
and l9 = mk  31.5  (-22.0)
and l10 = mk 34.0  (-16.5)
and l11 = mk 37.0  (-11.0)
and l12 = mk 42.0  (-7.5)
and l13 = mk 46.0  (-5.5)
and l14 = mk 50.0 (-4.0)
and l15 = mk 55.0 (-2.5)
and l16 = mk 60.0 (-2.0)
and l17 = mk 67.0 (-1.0);;


let ll_0_17 = [l0;l1;l2;l3;l4;l5;l6;l7;l8;l9;l10;l11;l12;l13;l14;l15;l16;l17] ;;

let ll_0_17_rot2A = map (fun p -> apply_cx rotA (apply_cx rotA p))
                        ll_0_17;;




let m1 = mk 19.0 67.0
and m2 = mk 18.5 61.0
and m3 = mk 19.0 57.5
and m4 = mk 20.0 54.5
and m41 = mk 22.0 52.0
and m42 = mk 23.5 50.0
and m5 = mk 25.5 48.5
and m6 = mk 28.5 46.0
and m7 = mk 36.0 42.5
and m8 = mk  44.0  38.0
and m9 = mk  47.5  35.0
and m10 = mk 50.0  30.0
and m11 = mk 51.0  26.5
and m12 = mk 50.0  23.0
and m13 = mk 46.5  20.0
and m14 = mk 43.0 19.0
and m15 = mk 36.0 19.5
and m16 = mk 29.0 21.5;;

let ml_1_16 = [m1;m2;m3;m4;m5;m6;m7;m8;m9;m10;m11;m12;m13;m14;m15;m16];;
let ml_1_16_rotB = map (apply_cx rotB) ml_1_16;;




let o1 = mk 27.0 (-28.0)
and o2 = mk 27.0 (-38.0)
and o3 = mk 28.5 (-36.0)
and o4 = mk 28.5 (-30.0);;


let mu1 = mk 5.0 (-30.5)
and mu2 = mk 6.5 (-27.5)
and mu3 = mk 8.0 (-26.5)
and mu4 = mk 9.5 (-27.5)
and mu5 = mk 11.0 (-29.5)
and mu6 = mk 9.0 (-29.5);;
(* +points_du_kangourou+ *)


(* +contour_kangourou+ *)

let contour_kangourou = 
  let  points =
     [ptC] @ kl_1_21 @ ll_0_17 @
     [ptA] @ (List.rev ll_0_17_rot2A) @
     (List.rev ml_1_16_rotB) @
     [ptB] @ ml_1_16 @
     (List.rev kl_1_21_rotC) @
     [ptC]
(*
     [ptC;k1;k2;k3;k4;k5;k6;k7;k8;k9;k91;k92;k93;k10;k11;k12;k13;
      k14;k15;k16;k17;k18;k19;k20;k21;
      l1;l2;l3;l4;l5;l6;l7;l8;l9;l10;l11;l12;l13;l14;l15;l16;l17;ptA;
      l17';l16';l15';l14';l13';l12';l11';l10';l9';l8';
      l7';l6';l5';l4';l3';l2';l1';l0';
      m16';m15';m14';m13';m12';m11';m10';m9';m8';m7';m6';
      m5';m42';m41';m4';m3';m2';m1';ptB;m1;m2;m3;m4;m41;m42;
      m5;m6;m7;m8;m9;m10;m11;m12;m13;m14;m15;m16;
      k21'; k20';k19';k18';k17';
      k16';k15';k14';k13';k12';k11';k10';k93';k92';k91';k9';k8';k7';k6';k5';
      k4';k3';k2';k1'; ptC]
*)
  in function t -> 
   let tt z = point_of_cx (apply_cx t z)
in make_sketch [Seg (map tt points)]
;;

(* +contour_kangourou+ *)

let contours_oeil t = 
let tt z = point_of_cx (apply_cx t z) 
and centre_oeil = cx_of_point {xc=0.1183;yc= -0.142}
and bord_oeil = cx_of_point {xc=0.134;yc= -0.142}
in let c = tt centre_oeil
   and r= euclidian_distance 
             (apply_cx t centre_oeil)
             (apply_cx t bord_oeil)
   in make_sketch[Arc(c, r, 0.0, 360.0)];;

let contours_interieur_oeil t = 
let tt z = point_of_cx (apply_cx t z) 
and centre_interieur_oeil = cx_of_point {xc=0.11;yc= -0.142}
and bord_interieur_oeil = cx_of_point {xc=0.117;yc= -0.142}
in let c = tt centre_interieur_oeil
   and r= euclidian_distance 
             (apply_cx t centre_interieur_oeil)
             (apply_cx t bord_interieur_oeil)
   in make_sketch[Arc(c, r, 0.0, 360.0)];;

let contours_museau =
  let points = [mu1;mu2;mu3;mu4;mu5;mu6;mu1]
  in function t ->
    let tt z = point_of_cx (apply_cx t z) 
    in make_sketch [Seg (map tt points)];;

let contours_oreille = 
  let points = [o1;o2;o3;o4;o1]
  in function t ->
let tt z = point_of_cx (apply_cx t z) 
in make_sketch
    [Seg (map tt points)];;


set_default_linewidthcoef 0.01;;
set_default_color black;;

let kang3 color_map t  col =
  let contours_sk =contour_kangourou t
  and oeil_sk = contours_oeil t
  and interieur_oeil_sk = contours_interieur_oeil t
  and museau_sk = contours_museau t
  and oreille_sk = contours_oreille t
  in group_pictures
       [make_fill_picture
          (Eofill, color_map col) contours_sk;
        make_default_draw_picture contours_sk  ;
        make_fill_picture
          (Eofill, white) oeil_sk;
        make_fill_picture
          (Eofill, black) interieur_oeil_sk;
        make_fill_picture
          (Eofill, black) museau_sk;
        make_fill_picture
          (Eofill, black) oreille_sk];;

let visualize (t,c) =
  let contours_sk =contour_kangourou t
  and oeil_sk = contours_oeil t
  and interieur_oeil_sk = contours_interieur_oeil t
  and museau_sk = contours_museau t
  and oreille_sk = contours_oreille t
  in group_pictures
       [make_fill_picture
          (Eofill, ColorMap3.map (c.(0))) contours_sk;
        make_default_draw_picture contours_sk  ;
        make_fill_picture
          (Eofill, white) oeil_sk;
        make_fill_picture
          (Eofill, black) interieur_oeil_sk;
        make_fill_picture
          (Eofill, black) museau_sk;
        make_fill_picture
          (Eofill, black) oreille_sk];;

let select (t,col) = euclidian_distance cx_0  
                        (Hyperbolic_geometry.apply_cx t centre) < 0.98;;

end;;



module Kangourou_escher_tagged =
  Add_tags_to_colored_hyperbolic_pattern (Kangourou_escher);;


module Hyper_quad =
struct
  open Poincare;;
 type transformation = Hyperbolic_geometry.transformation * int array;;
let qpoints = let a,d,b,c = hyper_bitriangle  (pi_over 4) (pi_over 3)  (pi_over 3)
              in [a;d;b;c];;
let visualize (t,col) = let pat = List.map (Hyperbolic_geometry.apply_cx t) qpoints
                in  poincare_polygon_picture pat;;
let select (t,col)  = true;;
end;;

module Hyper_quad_tagged =
  Add_tags_to_colored_hyperbolic_pattern (Hyper_quad);;


module  Circle_limit_poisson_tiling =
  Construct_tiling  (Group_hyp_3_3_4) (Hyperbolic_geometry)
                    (Circle_limit_mapping) (Circle_limit_color_mapping)
                    (Poisson_escher);;

module  Circle_limit_poisson_tiling_ocamlp3l  =
  Construct_tiling_ocamlp3l
                    (Group_hyp_3_3_4) (Hyperbolic_geometry)
                    (Circle_limit_mapping) (Circle_limit_color_mapping)
                    (Poisson_escher_ocamlp3l);;

module Pavage_ocamlp3l  =
      Make_mixed_generator  
             (Group_hyp_3_3_4) 
             (MakePair (Circle_limit_mapping)(Circle_limit_color_mapping))
             (Pair (Hyperbolic_geometry.Tgroup) (Color_Group) )
  ;;

module  Circle_limit_kangourou_tiling =
  Construct_tiling  (Group_hyp_3_3_4) (Hyperbolic_geometry)
                    (Circle_limit_mapping) (Circle_limit_color_mapping)
                    (Kangourou_escher);;

module Circle_limit_quad_tiling =
  Construct_tiling  (Group_hyp_3_3_4) (Hyperbolic_geometry)
                    (Circle_limit_mapping) (Circle_limit_color_mapping)
                    (Hyper_quad);;
  


module  Circle_limit_poisson_tagged_tiling =
  Construct_tagged_tiling  (Group_hyp_3_3_4) (Hyperbolic_geometry)
                    (Circle_limit_mapping) (Circle_limit_color_mapping)
                    (Circle_limit_tag_mapping)
                    (Poisson_escher_tagged);;

module  Circle_limit_kangourou_tagged_tiling =
  Construct_tagged_tiling  (Group_hyp_3_3_4) (Hyperbolic_geometry)
                    (Circle_limit_mapping) (Circle_limit_color_mapping)
                    (Circle_limit_tag_mapping)
                    (Kangourou_escher_tagged);;

module Circle_limit_quad_tagged_tiling =
  Construct_tagged_tiling  (Group_hyp_3_3_4) (Hyperbolic_geometry)
                    (Circle_limit_mapping) (Circle_limit_color_mapping)
                    (Circle_limit_tag_mapping)
                    (Hyper_quad_tagged);;




(* Fichier p1.ml *)

module P1_hex1_pattern =
struct
  open Mlgraph;;
  open Util;;
  type transformation = Euclidean_geometry.transformation *  
                        Color_Group.element;;

let ptA1 = {xc=300.0;yc=300.0} and ptB1={xc=320.0;yc=280.0}
and ptC1 = {xc=360.0;yc=300.0} and ptD1={xc=340.0;yc=350.0};;
let trans1 = point_translation ptC1 ptA1;;
let trans2 = point_translation ptB1 ptD1;;
let ptE1 = transform_point trans2 ptA1;;
let ptF1 = transform_point trans1 ptD1;;
let trans3 = point_translation ptB1 ptF1;;

let visualize (t,col) =
  let line = List.rev (points_of_arc(Arc (origin,1.0,0.0,40.0)) 10) in
  let lineAB= transform_points
             (handle_transform (ptB1,ptA1) 
                               ({xc=1.0;yc=0.0},
                                {xc=cosinus 40.0;yc=sinus 40.0}))
              line in
  let lineBC= transform_points
             (handle_transform (ptC1,ptB1) 
                               ({xc=1.0;yc=0.0},
                                {xc=cosinus 40.0;yc=sinus 40.0}))
              line in 
let lineCD= transform_points
             (handle_transform (ptD1,ptC1) 
                               ({xc=1.0;yc=0.0},
                                {xc=cosinus 40.0;yc=sinus 40.0}))
              line in
let lineFA= transform_points trans1 (List.rev lineCD) in
let lineEF= transform_points trans3 (List.rev lineBC) in
let lineDE= transform_points trans2 (List.rev lineAB) in

let lsty = {linewidth= 1.0;linecap=Buttcap;
            linejoin=Beveljoin;dashpattern=[]} in 

let sk =  transform_sketch t
 (make_sketch [Seg (lineAB@lineBC@lineCD@lineDE@lineEF@lineFA)]) in 

  group_pictures
     [make_fill_picture (Nzfill, ColorMap3.map (col.(0))) sk;
       make_draw_picture (lsty,black) sk]
 ;;

   let visible p =  p.xc < 800. && p.xc > -800.
                 &&  p.yc < 800. && p.yc > -800.;;

   let select (t,col) = visible (transform_point t ptA1)
                   ||   visible (transform_point t ptB1)
                   ||   visible (transform_point t ptC1)
                   ||   visible (transform_point t ptD1)
                   ||   visible (transform_point t ptE1)
                   ||   visible (transform_point t ptF1);;
end;;


module P1_hex1_tagged_pattern =
  Add_tags_to_colored_euclidean_pattern (P1_hex1_pattern);;


module P1_hex1_mapping  =
struct
 open Mlgraph;;
  type source = Group_euclid_p1.element;;
  type dest = Euclidean_geometry.transformation;;
  open Group_euclid_p1;;
  open Euclidean_geometry;;
  open P1_hex1_pattern;;

  let map =
      function
                   TA  ->  inverse_transformation trans1
             |     TB  ->  trans2
             |     Ta  ->  trans1
             |     Tb  ->  inverse_transformation trans2
end;;


module P1_hex1_color_mapping =
struct
  type source = Group_euclid_p1.element;;
  type dest =  Color_Group.element;;
  open Group_euclid_p1;;

  let map = function   
      TA -> [|1;2;0;3|]
  |   Ta -> [|2;0;1;3|]
  |   TB -> [|2;0;1;3|]
  |   Tb -> [|1;2;0;3|]
;;
end;;


module P1_hex1_tag_mapping =
struct
  type source = Group_euclid_p1.element;;
  type dest =  string;;
  open Group_euclid_p1;;

  let map = function   
      TA -> "A"
  |   Ta -> "a"
  |   TB -> "B"
  |   Tb -> "b"
;;
end;;






module P1_hex1_tiling  =
  Construct_tiling (Group_euclid_p1)  (Euclidean_geometry)
                   (P1_hex1_mapping) (P1_hex1_color_mapping) 
                   (P1_hex1_pattern);;


module P1_hex1_tagged_tiling  =
  Construct_tagged_tiling (Group_euclid_p1)  (Euclidean_geometry)
                   (P1_hex1_mapping) (P1_hex1_color_mapping) (P1_hex1_tag_mapping) 
                   (P1_hex1_tagged_pattern);;



(* Fichier p2.ml *)



module type QUAD =
sig
  open Mlgraph;;
 val pA : point
 val pB : point
 val pC : point
 val pD : point
end


module Make_quad_p2_mapping (Q : QUAD)  =
struct
  open Mlgraph;;
  open Util;;
  type source = Group_euclid_p2.element;;
  type dest = Euclidean_geometry.transformation;;
  open Group_euclid_p2;;
  open Euclidean_geometry;;

  let map = 
    let tr1' = point_translation (middle Q.pA Q.pB) (middle Q.pB Q.pC)
    and tr2' = point_translation (middle Q.pA Q.pB) (middle Q.pA Q.pD)
    in
    let tr1 = compose tr1' tr1'
    and tr2 = compose tr2' tr2'
    and tr3 = point_symmetry (middle Q.pA Q.pB)
    in
  function
     TA ->  tr1
  |  TB ->  tr2
  |  TC ->  tr3
  |  Ta -> inverse_transformation tr1
  |  Tb -> inverse_transformation tr2
  |  Tc -> inverse_transformation tr3;;
end;;


module P2_color_mapping =
struct
  type source = Group_euclid_p2.element;;
  type dest =  Color_Group.element;;
  open Group_euclid_p2;;

  let map = function   
    (TA|TB|Ta|Tb) -> [|0;1;2;3|]
  | (TC|Tc) -> [|1;0;2;3|];;
end;;


module Make_quad_p2_pattern (Q : QUAD) =
struct
   open Mlgraph;;
   type transformation = Euclidean_geometry.transformation * 
                         Color_Group.element;;
   let visualize (t,col) =
     let sk = transform_sketch t (make_sketch [Seg [Q.pA;Q.pB;Q.pC;Q.pD;Q.pA]]) in
     group_pictures
      [make_fill_picture (Nzfill, ColorMap3.map (col.(0))) sk;
       make_default_draw_picture sk];;
   let visible p =  p.xc < 300. && p.xc > -300.
                 &&  p.yc < 300. && p.yc > -300.;;

   let select (t,col) = visible (transform_point t Q.pA)
                   ||   visible (transform_point t Q.pB)
                   ||   visible (transform_point t Q.pC)
                   ||   visible (transform_point t Q.pD)

;;
end;;



module Construct_p2_quad_tiling (Q : QUAD) =
  Construct_tiling (Group_euclid_p2)  (Euclidean_geometry)
                   (Make_quad_p2_mapping (Q)) (P2_color_mapping) 
                   (Make_quad_p2_pattern (Q));;






module P2_hex2_pattern =
struct
  open Mlgraph;;
  open Util;;
  type transformation = Euclidean_geometry.transformation *  
                        Color_Group.element;;
  let xunit={xc=1.0;yc=0.0};;
  let ptA2={xc=300.0;yc=300.0} and ptB2={xc=320.0;yc=280.0}
  and ptC2={xc=360.0;yc=300.0} and ptD2={xc=340.0;yc=350.0}
  and ptE2={xc=320.0;yc=380.0};;
  let ptF2= transform_point (point_translation ptC2 ptB2) ptE2;;

  let visualize (t,col) =
  let xunit={xc=1.0;yc=0.0} in
  let alpha=60.0 in
  let line1= 
     points_of_arc (Arc (origin,1.0,0.0,alpha)) 10 in
  let line2= 
     List.rev line1 @ (transform_points (rotation xunit 180.0) line1) in
  let extrem1= {xc=cosinus alpha;yc=sinus alpha} in
  let extrem2= transform_point (rotation xunit 180.0)
                 extrem1 in

let lineAB = transform_points
             (handle_transform (ptA2, ptB2) 
                               (extrem1,extrem2))
              line2 in
let lineBC = transform_points
             (handle_transform (ptB2, ptC2) 
                               (extrem1,xunit))
              (List.rev(line1)) in
let lineCD = transform_points
             (handle_transform (ptC2, ptD2) 
                               (extrem1,extrem2))
              line2 in
let lineDE = transform_points
             (handle_transform (ptD2, ptE2) 
                               (extrem1,extrem2))
              line2 in
let lineEF =  transform_points
                  (handle_transform (ptF2, ptE2)
                               (extrem1,xunit))
                 line1 in
let lineFA = transform_points
             (handle_transform (ptF2, ptA2) 
                               (extrem1,extrem2))
              line2 in

let sk = transform_sketch t
 (make_sketch [Seg (lineAB@lineBC@lineCD@lineDE@lineEF@lineFA)]) in

  let lsty = {linewidth= 1.0;linecap=Buttcap;
            linejoin=Beveljoin;dashpattern=[]} in

  group_pictures
     [make_fill_picture (Nzfill, ColorMap3.map (col.(0))) sk;
       make_draw_picture (lsty,black) sk]
 ;;

   let visible p =  p.xc < 600. && p.xc > 100.
                 &&  p.yc < 600. && p.yc > 100.;;

   let select (t,col) = visible (transform_point t ptA2)
                   ||   visible (transform_point t ptB2)
                   ||   visible (transform_point t ptC2)
                   ||   visible (transform_point t ptD2)
                   ||   visible (transform_point t ptE2)
                   ||   visible (transform_point t ptF2);;
end;;


module P2_hex2_tagged_pattern =
  Add_tags_to_colored_euclidean_pattern (P2_hex2_pattern);;


module P2_hex2_mapping  =
struct
  open Mlgraph;;
  open Util;;
  type source = Group_euclid_p2.element;;
  type dest = Euclidean_geometry.transformation;;
  open Group_euclid_p2;;
  open Euclidean_geometry;;
  open P2_hex2_pattern;;

  let map = 
      let rot1= rotation (middle ptD2 ptE2) 180.0
      and rot2= rotation (middle ptA2 ptF2) 180.0
      and rot3= rotation (middle ptC2 ptD2) 180.0
      in  function
                   TA  ->  compose rot1 rot2
             |     TB  ->  compose rot3 rot2
             |     TC  ->  rot3
             |     Ta  ->  inverse_transformation (compose rot1 rot2)
             |     Tb  ->  inverse_transformation (compose rot3 rot2)
             |     Tc  ->  rot3
end;;


module P2_hex2_color_mapping =
struct
  type source = Group_euclid_p2.element;;
  type dest =  Color_Group.element;;
  open Group_euclid_p2;;

  let map = function   
    (TA|Ta) -> [|0;1;2;3|]
  |   TB -> [|2;0;1;3|]
  |   Tb -> [|1;2;0;3|]
  | (TC|Tc) -> [|1;2;0;3|];;
end;;


module P2_hex2_tag_mapping =
struct
  type source = Group_euclid_p2.element;;
  type dest =  string;;
  open Group_euclid_p2;;

  let map = function   
      TA -> "A"
  |   Ta -> "a"
  |   TB -> "B"
  |   Tb -> "b"
  |   TC -> "C"
  |   Tc -> "c";;
end;;





module P2_hex2_tiling  =
  Construct_tiling (Group_euclid_p2)  (Euclidean_geometry)
                   (P2_hex2_mapping) (P2_hex2_color_mapping) 
                   (P2_hex2_pattern);;



module P2_hex2_tagged_tiling  =
  Construct_tagged_tiling (Group_euclid_p2)  (Euclidean_geometry)
                   (P2_hex2_mapping) (P2_hex2_color_mapping) (P2_hex2_tag_mapping)
                   (P2_hex2_tagged_pattern);;






(* Fichier p4.ml *)

module P4_pattern =
struct
  open Mlgraph;;
  open Util;;
  type transformation = Euclidean_geometry.transformation *  
                        Color_Group.element;;
  let ptA4 = {xc=300.0; yc=300.0} and ptB4={xc=325.0; yc=325.0};;
  let ptC4 = {xc=300.0; yc=350.0} and ptD4={xc=350.0; yc=300.0};;
  let rot1 = rotation ptA4 90.0
  and rot2 = rotation ptB4 180.0
  and rot3 = rotation ptC4 90.0;;


let visualize (t,col) =
 let xunit={xc=1.0;yc=0.0} in
  let alpha = 40.0 in
  let line1 = 
     points_of_arc (Arc (origin, 1.0, 0.0, alpha)) 4 in
  let line2 = List.rev line1 @ (transform_points (rotation xunit 180.0) line1) in
  let extrem1 = {xc=cosinus alpha; yc=sinus alpha} in
  let extrem2 = transform_point (rotation xunit 180.0)
                 extrem1 in
  let lineAC = transform_points
             (handle_transform (ptA4, ptC4) 
                               (xunit, extrem1))
              line1 in
  let lineAD = transform_points
             (handle_transform (ptA4, ptD4) 
                               (xunit, extrem1))
              line1 in
  let lineCD = transform_points
             (handle_transform (ptC4, ptD4) 
                               (extrem1, extrem2))
              line2 in

  let sk =  transform_sketch t
     (make_sketch [Seg (lineAC@lineCD@(List.rev lineAD))]) in
  let lsty = {linewidth= 1.0;linecap=Buttcap;
            linejoin=Beveljoin;dashpattern=[]} in

  group_pictures
     [make_fill_picture (Nzfill, ColorMap3.map (col.(0))) sk;
       make_draw_picture (lsty,black) sk]
 ;;

   let visible p =  p.xc < 800. && p.xc > 0.
                 &&  p.yc < 800. && p.yc >  0.;;

   let select (t,col) = visible (transform_point t ptA4)
                   ||   visible (transform_point t ptB4)
                   ||   visible (transform_point t ptC4)
                   ||   visible (transform_point t ptD4);;
end;;


module P4_tagged_pattern =
struct
  type transformation = Euclidean_geometry.transformation *
                       (Color_Group.element * string);;
  open Mlgraph;;
  open Util;;
  open P4_pattern;;
  let visualize (t,(c,tag)) = 
     let p = P4_pattern.visualize (t,c)
     and c = transform_point  t (barycentre [ptA4;ptC4;ptD4])
     in group_pictures [p; text tag (picture_height p /. 4.) black   c ];;
  let select (t,(c,tag)) = P4_pattern.select (t,c);;
end;;


module P4_mapping  =
struct
  open Mlgraph;;
  type source = Group_euclid_p4.element;;
  type dest = Euclidean_geometry.transformation;;
  open Group_euclid_p4;;
  open Euclidean_geometry;;
  open P4_pattern;;

  let map =
      function
                   TA  ->  rot1
             |     TB  ->  rot3
             |     Ta  ->  inverse_transformation rot1
             |     Tb  ->  inverse_transformation rot3
end;;


module P4_color_mapping =
struct
  type source = Group_euclid_p4.element;;
  type dest =  Color_Group.element;;
  open Group_euclid_p4;;

  let map = function   
      TA -> [|3;0;1;2|]
  |   Ta -> [|1;2;3;0|]
  |   TB -> [|2;1;0;3|]
  |   Tb -> [|2;1;0;3|]
;;
end;;


module P4_tag_mapping =
struct
  type source = Group_euclid_p4.element;;
  type dest =  string;;
  open Group_euclid_p4;;

  let map = function   
      TA -> "A"
  |   Ta -> "a"
  |   TB -> "B"
  |   Tb -> "b"
;;
end;;



module P4_tiling  =
  Construct_tiling (Group_euclid_p4)  (Euclidean_geometry)
                   (P4_mapping) (P4_color_mapping) (P4_pattern);;

module P4_tagged_tiling  =
  Construct_tagged_tiling (Group_euclid_p4)  (Euclidean_geometry)
                   (P4_mapping) (P4_color_mapping) (P4_tag_mapping)
                   (P4_tagged_pattern);;



(* Fichier pg.ml *)


(* Motif en forme de Kangourou dessine par Raoul Raba pour un *)
(* pavage de symetrie PG *)

module Pg_kang_mapping =
struct
  type source = Group_euclid_pg.element;;
  type dest = Euclidean_geometry.transformation;;
  open Mlgraph;;
  open Group_euclid_pg;;
  open Euclidean_geometry;;
  let tr_horiz = translation (69.,0.);;
  let tr_horiz_back =  translation (-.69.,0.);;
  let tr_vertic = translation (0.,68.);;
  let tr_vertic_back = translation (0.,-68.);;
  let tr_screw =
    compose  tr_vertic (vsymmetry 252.5);;
  let tr_screw_back =
    compose (vsymmetry 252.5) tr_vertic_back;;

  let map = function   
    TG -> tr_screw
  | Tg -> tr_screw_back
  | TX -> tr_horiz
  | Tx -> tr_horiz_back;;
end;;


module Pg_kang_color_mapping =
struct
  type source = Group_euclid_pg.element;;
  type dest =  Color_Group.element;;
  open Group_euclid_pg;;

  let map = function   
    TG -> [|2;1;0;3|]
  | Tg -> [|2;1;0;3|]
  | TX -> [|1;2;3;0|]
  | Tx -> [|3;0;1;2|];;
end;;


module Pg_kang_tag_mapping =
struct
  type source = Group_euclid_pg.element;;
  type dest =  string;;
  open Group_euclid_pg;;

  let map = function   
    TG -> "G"
  | Tg -> "g"
  | TX -> "X"
  | Tx -> "x";;
end;;



module Kangourou_pg_pattern =
struct
  open Mlgraph;;
  open Euclidean_geometry;;
  type transformation = Euclidean_geometry.transformation *  
                        Color_Group.element;;
  let tr_horiz = translation (69.,0.);;
  let tr_horiz_back =  translation (-.69.,0.);;
  let tr_vertic = translation (0.,68.);;
  let tr_vertic_back = translation (0.,-68.);;
  let tr_screw =
    compose  tr_vertic (vsymmetry 252.5);;
  let tr_screw_back =
    compose (vsymmetry 252.5) tr_vertic_back;;
 let mk x y = {xc=x; yc=y};;

let ptA = mk 252.5 268.;;
let a1 = mk 249.5 262.5   ;;
let a2 = mk 247. 258.   ;;
let a3 = mk 243.5 253.  ;;
let a4 = mk 240. 248.5   ;;
let a5 = mk 236.5 245.  ;;
let a6 = mk 233. 242.  ;;
let a7 = mk 230. 239.5  ;;
let a8 = mk 225. 237.  ;;
let a9 = mk 220. 235.  ;;
let a10 = mk 218. 234.   ;;
let a11 = mk 215.5 234.5  ;;
let a12 = mk 213.5 236.  ;;
let a13 = mk 212.5 238.   ;;
let a14 = mk 212.5 240.5   ;;
let a15 = mk 214. 241.5   ;;
let a155 = mk 215.5 241.   ;;
let a16 = mk 217.5 241.  ;;
let a17 = mk 218.5 243.  ;;
let a18 = mk 218.5 244.5   ;;
let a19 = mk 218. 247.  ;;
let a20 = mk 217.5 247.2  ;;
let a21 = mk 212.5 247.  ;;
let a22 = mk 210.5 246.5  ;;
let a23 = mk 209. 244.7  ;;
let a24 = mk 207. 242.3  ;;
let a25 = mk 206. 239.5   ;;
let a26 = mk 205.5 236.3  ;;
let a27 = mk 205.5 233.   ;;
let a28 = mk 205.9 229.5  ;;
let a29 = mk 207.5 226.  ;;
let a30 = mk 211. 223.6  ;;
let a31 = mk 215. 222.2  ;;
let a32 = mk 218.3 222.  ;;
let a33 = mk 221.5 222.  ;;
let a34 = mk 224.5 222.3  ;;

let ptB = mk 228. 223.5;;

let b1 = mk 232.2 225.8;;
let b2 = mk 236. 228.;;
let b3 = mk 240.5 231.;;
let b4 = mk 245.6 235.;;
let b5 = mk 248.8 238.3;;
let b6 = mk 252. 242.5;;
let b7 = mk 255.5 247.5;;
let b8 = mk 257.8 251.9;;
let b9 = mk 259.3 258.;;
let b10 = mk 261.8 255.;;
let b11 = mk 263.5 253.;;
let b12 = mk 267.5 257.;;
let b13 = mk 268. 254.5;;
let b14 = mk 268.2 252.;;
let b15 = mk 268. 249.;;
let b16 = mk 267.4 245.5;;
let b17 = mk 266.4 242.;;
let b18 = mk 265. 238.3;;
let b19 = mk 263. 234.3;;
let b20 = mk 261.5 231.5;;
let b21 = mk 260. 229.;;
let b22 = mk 258.8 226.;;
let b23 = mk 258.2 223.7;;
let b24 = mk 257.8 220.;;
let b25 = mk 258. 215.5;;
let b26 = mk 258.7 212.8;;
let b27 = mk 259.7 210.5;;
let b28 = mk 262.5 208.5;;
let b29 = mk 265.5 207.8;;

let ptC = mk 269. 207.9;;
let ptD = mk 200. 207.9;;

let d1 = mk 203. 207.9;;
let d2 = mk 207. 208.2;;
let d3 = mk 210. 209.;;
let d4 = mk 213.7 209.5;;
let d5 = mk 218. 210.4;;
let d6 = mk 223.2 211.3;;
let d7 = mk 227.4 212.;;
let d8 = mk 230.5 212.3;;
let d9 = mk 233. 212.5;;
let d10 = mk 236. 212.;;
let d11 = mk 241.2 210.5;;
let d12 = mk 245. 208.3;;
let d13 = mk 247.2 206.8;;
let d14 = mk 249. 204.5;;

let ptE = mk 252.5 200.;;

let bb1 = mk 229. 222.3;;
let bb2 = mk 230.3 220.5;;
let bb3 = mk 232. 220.;;
let bb4 = mk 233.5 219.3;;
let bb5 = mk 235. 219.;;
let bb6 = mk 237.2 219.3;;
let bb7 = mk 239. 219.5;;
let bb8 = mk 241. 219.;;
let bb9 = mk 243.5 218.5;;
let bb10 = mk 245. 217.8;;
let bb11 = mk 246.8 215.;;
let bb12 = mk 248.2 211.8;;
let bb13 = mk 249.2 208.5;;
let bb14 = mk 250. 204.5;;

let bou1 = bb3;;
let bou2 = mk 233.5 221.3;;
let bou3 = mk 235. 222.3;;
let bou4 = mk 236.5 223.9;;

let centre_oeil = mk 246.8 231.3;;

let orc1 = b11 ;;
let orc2 = mk 261. 250.;;
let orc3 = mk 259. 247.3;;
let orc4 = mk 257.5 244.7;;
let orc5 = mk 256. 242.;;
let orc6 = mk 254.2 238.5;;
let orc7 = mk 253.5 235.7;;
let orc8 = mk 253. 232.5;;

let ori1 = mk 264.8 247.8;;
let ori2 = mk 262.8 246.7;;
let ori3 = mk 261. 244.5;;
let ori4 = mk 259. 241.8;;
let ori5 = mk 258. 239.;;
let ori6 = mk 256.5 236.2;;
let ori7 = mk 255.5 233.6;;
let ori8 = mk 255. 230.5;;
let ori9 = mk 258.3 232.6;;
let ori10 = mk 260. 234.2;;
let ori11 = mk 261.2 236.;;
let ori12 = mk 262.5 238.;;
let ori13 = mk 263.5 240.;;
let ori14 = mk 264.3 242.5;;
let ori15 = mk 265. 245.2;;





let queue_kang
= map (transform_point tr_horiz_back)
    [ptA;a1;a2;a3;a4;a5;a6;a7;a8;a9;a10;a11;a12;a13;a14;
     a15;a155;a16;a17;a18;a19;a20;a21;a22;a23;a24;a25;a26;
     a27;a28;a29;a30;a31;a32;a33;a34;ptB;
     b1;b2;b3;b4;b5;b6;b7;b8;b9;b10;b11;b12;
     b13;b14;b15;b16;b17;b18;b19;b20;b21;b22;
     b23;b24;b25;b26;b27;b28;b29];;



let dos_kang
= map (transform_point (compose_transformations [tr_horiz_back;tr_screw]))
     [ptD;d1;d2;d3;d4;d5;d6;d7;d8;d9;d10;d11;d12;d13;d14;ptE];;

let tete_kang = 
 map (transform_point tr_screw)
    [ptE;bb14;bb13;bb12;bb11;bb10;bb9;bb8;bb7;bb6;bb5;bb4;bb3;bb2;bb1;ptB;
     b1;b2;b3;b4;b5;b6;b7;b8;b9;b10;b11;b12;
     b13;b14;b15;b16;b17;b18;b19;b20;b21;b22;
     b23;b24;b25;b26;b27;b28;b29 ];;


let patte_kang = 
   [ptD;d1;d2;d3;d4;d5;d6;d7;d8;d9;d10;d11;d12;d13;d14;ptE;
    bb14;bb13;bb12;bb11;bb10;bb9;bb8;bb7;bb6;bb5;bb4;bb3;bb2;bb1;ptB];;

let ventre_kang =
  [ptB;a34;a33;a32;a31;a30;a29;a28;a27;a26;a25;a24;a23;a22;a21;a20;
   a19;a18;a17;a16;a155;a15;a14;a13;a12;a11;a10;a9;a8;a7;a6;a5;a4;a3;a2;a1;ptA];;


let oreille_kang =
transform_picture tr_screw
 (make_default_draw_picture
  (group_sketches
     [make_sketch [Seg  [orc1;orc2;orc3;orc4;orc5;orc6;orc7;orc8]];
      make_sketch [Seg [ori1;ori2;ori3;ori4;ori5;ori6;ori7;ori8;
                   ori9;ori10;ori11;ori12;ori13;ori14;ori15;ori1]]]));;

let bouche_kang =
transform_picture tr_screw
 (make_default_draw_picture
  (make_sketch [Seg [bou1;bou2;bou3;bou4]]));;

let oeil_kang =
center_picture
 (group_pictures
    [make_fill_picture
       (Nzfill, white)
       (make_sketch[Arc(origin, 2., 0.0, 360.0)]);
     make_fill_picture
       (Nzfill, black)
       (make_sketch[Arc({xc=1.;yc= -.1.}, 1., 0.0, 360.0)])])
 (transform_point tr_screw centre_oeil);;

let kang_contour  = make_sketch [Seg
                    (tete_kang@dos_kang@queue_kang@patte_kang@ventre_kang)];;

let visualize (t,col) = 
 transform_picture t
     (group_pictures
         [make_fill_picture (Nzfill, ColorMap3.map (col.(0))) kang_contour;
          make_default_draw_picture kang_contour;
          oreille_kang;
          bouche_kang;
          oeil_kang]);;

(*let select (t,col) = true;;*)
   let visible p =  p.xc < 500. && p.xc > 0.
                 &&  p.yc < 600. && p.yc > 100.;;

   let select (t,col) = visible (transform_point t ptA)
                   ||   visible (transform_point t(transform_point tr_horiz_back ptA))
;;

end;;

module Kangourou_pg_tagged_pattern =
  Add_tags_to_colored_euclidean_pattern (Kangourou_pg_pattern);;


module  Pg_kang_tiling =
  Construct_tiling (Group_euclid_pg)  (Euclidean_geometry)
                   (Pg_kang_mapping) (Pg_kang_color_mapping) 
                   (Kangourou_pg_pattern);;

module  Pg_kang_tagged_tiling =
  Construct_tagged_tiling (Group_euclid_pg)  (Euclidean_geometry)
                   (Pg_kang_mapping) (Pg_kang_color_mapping)
                   (Pg_kang_tag_mapping) 
                   (Kangourou_pg_tagged_pattern);;




