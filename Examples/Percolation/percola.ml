(* Invasion Percolation (Wilson - Cowichan problems) *)
open Graphics;;

let n = 100;;
let ppp = 5;;

type rock = int;;

let new_board () = 
  let maxval = 100 in
  let board = Array.create_matrix n n 0 in
  Random.init (Unix.getpid());
  for i = 0 to (n-1) do
    for j = 0 to (n-1) do
      Array.set board.(i) j ((Random.int maxval) : rock)
    done
  done;
  Array.set board.(n/2) (n/2) (-1);
  board;;

let display_pixel board i j = 
  let n = Array.length board in
  if((board.(i).(j) : rock) = -1) 
  then begin
    set_color blue;
    fill_circle (i*ppp+ppp/2) (j*ppp+ppp/2) (ppp/2);
  end
  else begin
    set_color white;
    fill_circle (i*ppp+ppp/2) (j*ppp+ppp/2) (ppp/2);
  end
;;

let display board =
  let n = Array.length board in
  begin
    for i = 0 to (n-1) do
      for j = 0 to (n-1) do
	if((board.(i).(j) : rock) = -1) 
	then begin
	  set_color blue;
	  fill_circle (i*ppp+ppp/2) (j*ppp+ppp/2) (ppp/2);
	end
	else begin
	  set_color white;
	  fill_circle (i*ppp+ppp/2) (j*ppp+ppp/2) (ppp/2);
	end
      done
    done
  end;;

let filled_points board = 
  let points = ref [] in
  for i = 0 to (n-1) do
    for j = 0 to (n-1) do
      if (board.(i).(j) = (-1)) then points := ((i,j)::!points)
    done
  done;
  !points;;

let neighbours board (i,j) =
  [(board.((i-1+n) mod n).(j),((i-1+n) mod n),j);
    (board.((i+1) mod n).(j),((i+1) mod n),j);
    (board.(i).((j-1+n) mod n), i,((j-1+n) mod n));
    (board.(i).((j+1+n) mod n), i,((j+1+n) mod n))];;

let rec max_t l m = 
  let (wm,im,jm) = m in
  match l with 
    [] -> m 
  | (w,i,j)::r -> 
      if(w > wm) 
      then (max_t r (w,i,j))
      else (max_t r m);;

let max_t_list l = 
  max_t (List.tl l) (List.hd l);;

let filled = ref [];;

let percole_step board = 
  let points = if(!filled = []) then filled_points board else !filled in
  let nears = (List.flatten (List.map (neighbours board) points)) in
  let (w,i,j) = max_t (List.tl nears) (List.hd nears) in

  filled:= (i,j)::!filled; 
  Array.set board.(i) j (-1); 
  display_pixel board i j;
  board;;


let percole steps =
  let board = new_board() in
  begin
    display board;
    for i = 0 to steps do
      percole_step board
    done
  end;;

let play steps =
  open_graph (" "^(string_of_int(n*ppp))^"x"^(string_of_int(n*ppp)));
  percole steps;
  let _ = read_line() in
  close_graph();;

play 1000;;
