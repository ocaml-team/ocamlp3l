#!/bin/tcsh
#
# clean up files left by ocamlp3l
#

echo Removing editor files ...
rm -f *~
echo Removing temp source files
rm -f *.par.ml *.seq.ml *.gra.ml
echo Removing object code ...
rm -f *.cm?
echo Removing executable files ...
rm -f *.par *.seq *.gra
echo Removing log files ...
rm -f *.log
echo "Done!"

